#!/bin/bash
if [ $# -eq 0 ]
	then
		echo 'building app ... '
		cd angular
		npm run-script build
		cd ..
		echo 'building finished ... '
elif [ $# -eq 1 ]
	then
		# echo 'building app ... '
		# cd angular
		# npm run-script build
		# cd ..
		# echo 'building finished ... '
		if [ $1 == '-o' ]
			then
				echo 'deploying to ohio env ... '
				cp ./eb-config/config-o.yml ./.elasticbeanstalk/config.yml
				eb deploy MyFirstElasticBeanstalkApplication-env
		fi
		if [ $1 == '-i' ]
			then
				echo 'deploying to mumbai env ... '
				cp ./eb-config/config-i.yml ./.elasticbeanstalk/config.yml
				eb deploy confirmu-india-test
		fi
elif [ $# -eq 2 ]
	then
		# echo 'building app ... '
		# cd angular
		# npm run-script build
		# cd ..
		# echo 'building finished ... '
		if [ $1 == '-o' ]
			then
				echo 'deploying to ohio env ... '
				cp ./eb-config/config-o.yml ./.elasticbeanstalk/config.yml
				eb deploy MyFirstElasticBeanstalkApplication-env
		fi
		if [ $2 == '-i' ]
			then
				echo 'deploying to mumbai env ... '
				cp ./eb-config/config-i.yml ./.elasticbeanstalk/config.yml
				eb deploy confirmu-india-test
		fi
fi

