import os

from pymongo import MongoClient

import conf

necessary_collections_data = ['questions', 'settings', 'rules', 'sms', 'users', 'customers', 'nrc_emotion_lexicon']


def get_src_database():
    client = MongoClient(conf.MongoDB.uri_india)
    db = client[conf.MongoDB.db_india]
    return db


def get_dest_database():
    prod_uri = os.environ.get('PROD_MONGO_URI_INDIA')
    splitted_db = prod_uri.split('/')[-1] if prod_uri is not None else ""
    client = MongoClient(prod_uri)
    db = client[splitted_db]
    return db


def copy_all_collections():
    try:
        src_db = get_src_database()
        dst_db = get_dest_database()
        col_list = src_db.list_collection_names()
        for col in col_list:
            cursor = src_db[col].find()
            result = dst_db[col].insert_one({'test': 1})
            dst_db[col].delete_one({'_id': result.inserted_id})
            # copy data if necessary
            if col in necessary_collections_data:
                for data in cursor:
                    dst_db[col].insert(data)
        print("collections copied successfully")
    except Exception as e:
        print("An Error Occurred while copying collections")


def drop_all_production_collections():
    try:
        dst_db = get_dest_database()
        col_list = dst_db.list_collection_names()
        for col in col_list:
            dst_db[col].drop()
        print("collections dropped successfully")
    except Exception as e:
        print("An Error Occurred while dropping collections")


if __name__ == '__main__':
    copy_all_collections()
    pass
