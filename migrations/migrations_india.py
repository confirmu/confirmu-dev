from pymongo import MongoClient

import conf

myfugo_new_scoring_quesitions = {
    "301": {
        "brown": 15,
        "white": 10,
        "black": 5,
        "grey": 1
    },
    "302": {
        "land": 10,
        "bike": 4,
        "car": 1,
        "house": 5,
        "cattle": 15
    },
    "303": {
        "nofarm": 15,
        "twocows": 10,
        "vegetable": 8,
        "poultry": 2,
        "manycows": 8
    },
    "304": {
        "bull": 10,
        "specialist": 5
    }
}

psychology_types = ['Extrovert', 'Agreeableness', 'Concientiousness', 'Openness', 'Thoroughness', 'Control',
                    'Hesitancy', 'Social resistance', 'Optimising', 'Principled', 'Instinctiveness']

new_dizzy_questions = {
    "Decision": [
        {
            "QuestionType": "Decision",
            "QuestionId": "Decision1",
            "QuestionText": "You're in a sinking ship with 10 other people. There is one small lifeboat, with room for only one person or else the lifeboat would sink. Who would you save (besides yourself)? ",
            "QuestionCode": "100",
            "Options": [
                {
                    "Value": "A 35 year old sailor",
                    "Score": 30
                },
                {
                    "Value": "A heroic fighter",
                    "Score": 25
                },
                {
                    "Value": "A 26 year old college",
                    "Score": 18
                },
                {
                    "Value": "A rich charity worker",
                    "Score": 15
                },
                {
                    "Value": "blind ten years old",
                    "Score": 13
                },
                {
                    "Value": "your favourite celebrity",
                    "Score": 11
                },
                {
                    "Value": "pregnant women",
                    "Score": 7
                },
                {
                    "Value": "2 year old",
                    "Score": 2
                }
            ],
            "QuestionPsychology": psychology_types
        },
        {
            "QuestionType": "Decision",
            "QuestionId": "Decision2",
            "QuestionText": "What songs would you take to a desert island?",
            "QuestionCode": "101",
            "Options": [
                {
                    "Value": "Schemin' Up - Drake",
                    "Score": 0
                },
                {
                    "Value": "Boston - Kenny Chesney",
                    "Score": 0
                },
                {
                    "Value": "Music is All We Got - Chance the Rapper",
                    "Score": 0
                },
                {
                    "Value": "Raised by a Good Time - Steven Lee",
                    "Score": 0
                },
                {
                    "Value": "American Pie - don Mclean",
                    "Score": 0
                },
                {
                    "Value": "Unfaithful\" - Rihanna",
                    "Score": 0
                },
                {
                    "Value": "Fly Me to the Moon - Frank Sinatra",
                    "Score": 0
                },
                {
                    "Value": "Sweet Caroline - Neil Diamond",
                    "Score": 0
                }
            ],
            "QuestionPsychology": psychology_types
        }
    ]
}


def get_database():
    client = MongoClient(conf.MongoDB.uri_india)
    db = client[conf.MongoDB.db_india]
    return db


def create_loan_status_collection():
    """
    adding new collection 'loan_status' to database
    """
    # Create Collection 'loan_status' if not exists
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "loan_status" in col_list:
            print("collection loan_status already exists.")
        else:
            loan_status = db['loan_status']
            # insert for first time only to create collection
            result = loan_status.insert_one({'test': 1})
            loan_status.delete_one({'_id': result.inserted_id})
            print("collection loan_status has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection loan_status {}".format(e))


def update_myfugo_image_scoring():
    """update scoring for myfugo image questions"""
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': "myfugo"}
        myfugo_questions = questions.find_one(query)
        myfugo_image_questions = myfugo_questions['questions'].get("Image")
        for q in myfugo_image_questions:
            q_id = q['QuestionId']
            images = q['Images']
            new_scores = myfugo_new_scoring_quesitions.get(q_id)
            for img in images:
                image_id = img['image_id']
                img['score'] = new_scores[image_id]
        questions.save(myfugo_questions)
    except Exception as e:
        print("an error occurred while updating myfugo image scores {}".format(e))


def add_new_dizzy_questions():
    """adding new dizzy island songs and sinking boat questions"""
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': 'dizzy'}
        dizzy_questions_collection = questions.find_one(query)
        dizzy_questions = dizzy_questions_collection['questions']
        dizzy_questions['Decision'] = new_dizzy_questions['Decision']
        questions.save(dizzy_questions_collection)
        print('new dizzy questions (sinking boat and desert island songs has been added successfully)')
    except Exception as e:
        print("an error occurred while updating new dizzy questions {}".format(e))


def update_myfugo_loan_common_info():
    """updating myfugo common info cap loan"""
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': 'myfugo'}
        myfugo_questions_collection = questions.find_one(query)
        myfugo_common_info = myfugo_questions_collection['common_info']
        loan_info = myfugo_common_info['loan']
        loan_info['total_amount']['max'] = 5000
        loan_info['total_amount']['step'] = 100
        loan_info['period'] = 'day'
        questions.save(myfugo_questions_collection)
        print('myfugo common info cap loan has been updated')
    except Exception as e:
        print("an error occurred while updating myfugo common info {}".format(e))


def create_new_partner_hedgini():
    """
    adding new collection 'hedgini' to database
    """
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "hedgini" in col_list:
            print("collection hedgini already exists.")
        else:
            get_typeform_scores = db['hedgini']
            # insert for first time only to create collection
            result = get_typeform_scores.insert_one({'test': 1})
            get_typeform_scores.delete_one({'_id': result.inserted_id})
            print("collection hedgini has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection hedgini")


def insert_hedgini_question_document():
    '''
    create questions collection for patner hedgini
    '''
    # Create document 'dizzy' if it does not exist
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': "hedgini"}
        result = questions.find_one(query)
        if result is None:  # partner hedgini isn't inserted yet
            document_data = {
                'partner': 'hedgini',
                'common_info': {},
                'questions': {},
                'partner_info': {},
                'email_notifications': {},
            }
            questions.insert_one(document_data)
            print("partner hedgini questions document created successfully.")
        else:
            print("partner hedgini questions already exist.")

    except Exception as e:
        print("an error occurred while inserting document hedgini in questions collection")


def create_new_partner_creditAI():
    """
    adding new collection 'creditAI' to database
    """
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "creditAI" in col_list:
            print("collection creditAI already exists.")
        else:
            get_typeform_scores = db['creditAI']
            # insert for first time only to create collection
            result = get_typeform_scores.insert_one({'test': 1})
            get_typeform_scores.delete_one({'_id': result.inserted_id})
            print("collection creditAI has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection creditAI")


def insert_creditAI_question_document():
    '''
    create questions collection for patner creditAI
    '''
    # Create document 'creditAI' if it does not exist
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': "creditAI"}
        result = questions.find_one(query)
        if result is None:  # partner creditAI isn't inserted yet
            document_data = {
                'partner': 'creditAI',
                'common_info': {},
                'questions': {},
                'partner_info': {},
                'email_notifications': {},
            }
            questions.insert_one(document_data)
            print("partner creditAI questions document created successfully.")
        else:
            print("partner creditAI questions already exist.")

    except Exception as e:
        print("an error occurred while inserting document creditAI in questions collection")


def copy_questions_between_partners():
    '''
    copy questions between partner
    dizzy to creditAI
    LM_financials to Hedgini
    '''
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': {"$in": ["dizzy", "LM_financials"]}}
        src = list(questions.find(query))
        query = {'partner': {"$in": ["creditAI", "hedgini"]}}
        dst = list(questions.find(query))
        for partner in src:
            # copy from dizzy to creditAI
            if partner.get('partner') == 'dizzy':
                creditAI = [p for p in dst if p.get('partner') == 'creditAI'][0]
                creditAI['questions'] = partner.get('questions')
            # copy from LM_financials to Hedgini
            if partner.get('partner') == 'LM_financials':
                hedgini = [p for p in dst if p.get('partner') == 'hedgini'][0]
                hedgini['questions'] = partner.get('questions')
        for paratner in dst:
            questions.save(paratner)
        print('questions have been copied from dizzy and LM to credit and hedgini')
    except Exception as e:
        print("an error occurred while copying date between partner")


def add_collection_scenario_scoring():
    """
    adding new collection 'scenario_scoring' to database
    """
    # Create Collection 'scenario_scoring' if not exists
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "scenario_scoring" in col_list:
            print("collection scenario_scoring already exists.")
        else:
            get_typeform_scores = db['scenario_scoring']
            # insert for first time only to create collection
            result = get_typeform_scores.insert_one({'test': 1})
            get_typeform_scores.delete_one({'_id': result.inserted_id})
            print("collection scenario_scoring has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection scenario_scoring")


if __name__ == '__main__':
    pass
