import os

from pymongo import MongoClient
import random
import conf

# region Migration Constants
# Email Notifications Partners
email_notification_partners = ['bridge2capital', 'b2cap_hindi']
email_notification_data = {'to': 'riaz@xtracapindia.com', 'cc': 'admin@confirmu.com,assaf@confirmu.com'}

dice_questions_text = ["Dice 1 - Here is a true dice. You can throw is one time, if it falls on 1 or 6 you "
                       "will be granted with 5000$. How much would you pay to throw it once? ",

                       "Dice 2 - Here is a true dice. You can throw is one time, if it falls on 1 or 6 you "
                       "will be granted with 5000$. How much would you pay to throw it twice? ",

                       "Dice 3 - Here is a true dice. You can throw is one time, if it falls on 1 or 6 you "
                       "will be granted with 10000$. How much would you pay to throw it once? ",

                       "Dice 4 - Here is a true dice. You can throw is one time, if it falls on 2 or 4 you "
                       "will be granted with 2000$. How much would you pay to throw it once? ",

                       "Dice 5 - Here is a true dice. You can throw is one time, if it falls on 2 or 4 you "
                       "will be granted with 2000$. How much would you pay to throw it twice? ",

                       "Dice 5 - Here is a true dice. You can throw is one time, if it falls on 2 or 4 you "
                       "will be granted with 2000$. How much would you pay to throw it 3 times? "]
cart_questions_text = ["Please select 5 items and Fill in this cart"]

dice_questions_answers = ['50$', '100$', '150$', '200$']
cart_questions_answers = ['Eggs', 'Coffee', 'Coco pops', 'Milk', 'Nature Vally', 'Yugurt', 'Cheese', 'Bananas',
                          'Choclate fudge', 'Oak mill', 'Crackers', 'Red pepers', 'Bread', 'Cookies', 'Salmon']

psychology_types = ['Extrovert', 'Agreeableness', 'Concientiousness', 'Openness', 'Thoroughness', 'Control',
                    'Hesitancy', 'Social resistance', 'Optimising', 'Principled', 'Instinctiveness']

dizzy_questions_scores = {"Extrovert": {
    "high": {"image": {"1": 0, "4": 0, "8": 1, "12": 0, "13": 1, "15": 1, "28": 1, "38": 0},
             "dice": {"Dice2": "50$", "Dice4": "150$", "Dice5": "150$"},
             "cart": {"Choclate fudge": 0, "Oak mill": 0, "Crackers": 0}},
    "low": {"image": {"2": 0, "5": 1, "22": 1, "39": 1}, "dice": {"Dice1": "200$", "Dice2": "200$"},
            "cart": {"Nature Vally": 0, "Crackers": 0}}}, "Agreeableness": {
    "high": {"image": {"9": 0, "14": 1, "18": 1, "28": 0, "34": 0, "36": 1},
             "dice": {"Dice1": "50$", "Dice2": "50$", "Dice3": "50$", "Dice4": "150$", "Dice5": "50$"},
             "cart": {"Milk": 1, "Yugurt": 0, "Oak mill": 0, "Crackers": 0, "coocikes": 0, "Salmon": 0}},
    "low": {"image": {"2": 0}, "dice": {"Dice1": "200$", "Dice5": "200$"}, "cart": {}}}, "Concientiousness": {
    "high": {"image": {"18": 1, "21": 1, "35": 1},
             "dice": {"Dice1": "50$", "Dice2": "50$", "Dice3": "50$", "Dice6": "100$"},
             "cart": {"Oak mill": 0, "Crackers": 0}},
    "low": {"image": {}, "dice": {"Dice1": "150$", "Dice4": "100$"}, "cart": {"Salmon": 0}}},
    "Neurotisicm": {"high": {"image": {}, "dice": {}, "cart": {}},
                    "low": {"image": {"17": 0, "18": 1, "26": 0},
                            "dice": {"Dice2": "150$", "Dice4": "200$"},
                            "cart": {"Eggs": 1, "Coco pops": 0, "Yugurt": 0, "Cheese": 0}}},
    "Openness": {"high": {"image": {"18": 1, "30": 0, "38": 0}, "dice": {},
                          "cart": {"Yugurt": 0, "Bananas": 1, "Crackers": 0, "Salmon": 0}},
                 "low": {"image": {"5": 1}, "dice": {"Dice1": "150$", "Dice5": "150$"},
                         "cart": {}}}, "Thoroughness": {
        "high": {"image": {"6": 1, "14": 1, "25": 1, "27": 0, "35": 1},
                 "dice": {"Dice1": "50$", "Dice2": "50$", "Dice3": "50$", "Dice4": "50$", "Dice5": "50$",
                          "Dice6": "100$"}, "cart": {"Coffee": 0, "Yugurt": 0, "Oak mill": 0, "Salmon": 0}},
        "low": {"image": {}, "dice": {"Dice1": "200$", "Dice4": "200$"}, "cart": {"Crackers": 0}}}, "Control": {
        "high": {"image": {"12": 0, "15": 1, "18": 1, "35": 1},
                 "dice": {"Dice1": "50$", "Dice2": "50$", "Dice3": "50$", "Dice4": "50$", "Dice5": "50$",
                          "Dice6": "50$"}, "cart": {"Coffee": 0, "Coco pops": 1, "Yugurt": 0, "Salmon": 0}},
        "low": {"image": {"2": 0}, "dice": {}, "cart": {"Coco pops": 0, "Milk": 1, "Crackers": 0}}}, "Hesitancy": {
        "high": {"image": {"1": 1, "3": 0, "4": 1, "6": 0, "7": 1, "10": 1, "11": 0, "19": 0, "24": 0, "26": 0, "32": 0,
                           "37": 1, "38": 0},
                 "dice": {"Dice1": "200$", "Dice2": "200$", "Dice3": "50$", "Dice4": "200$", "Dice6": "200$"},
                 "cart": {"Cheese": 0, "Salmon": 0}},
        "low": {"image": {"5": 1, "6": 1, "17": 0, "18": 1, "19": 0, "21": 1, "24": 1},
                "dice": {"Dice2": "200$", "Dice4": "200$", "Dice5": "200$", "Dice6": "100$"},
                "cart": {"Nature Vally": 0, "Crackers": 0}}}, "Social resistance": {
        "high": {"image": {"14": 1}, "dice": {"Dice1": "200$"}, "cart": {"Nature Vally": 0}},
        "low": {"image": {"25": 0}, "dice": {"Dice1": "150$", "Dice2": "200$", "Dice3": "200$", "Dice5": "200$"},
                "cart": {"Bananas": 0}}}, "Optimising": {"high": {"image": {"6": 1, "8": 1, "24": 0, "28": 1},
                                                                  "dice": {"Dice1": "50$", "Dice2": "50$",
                                                                           "Dice3": "50$", "Dice4": "50$",
                                                                           "Dice5": "50$"},
                                                                  "cart": {"Milk": 1, "Cheese": 1, "Oak mill": 0,
                                                                           "Salmon": 0}},
                                                         "low": {"image": {"2": 0, "12": 0, "19": 0, "20": 1, "27": 1},
                                                                 "dice": {"Dice1": "200$", "Dice4": "200$",
                                                                          "Dice6": "200$"},
                                                                 "cart": {"Coco pops": 0, "Nature Vally": 0,
                                                                          "Choclate fudge": 0}}}, "Principled": {
        "high": {"image": {"2": 0, "17": 0, "21": 1, "23": 1, "37": 1, "39": 1},
                 "dice": {"Dice3": "150$", "Dice4": "150$", "Dice5": "150$"}, "cart": {"Coco pops": 0}},
        "low": {"image": {"6": 1, "12": 0, "19": 0, "24": 1, "34": 0},
                "dice": {"Dice1": "200$", "Dice2": "200$", "Dice5": "200$"}, "cart": {}}}, "Instinctiveness": {
        "high": {"image": {"6": 0, "8": 1, "10": 1, "14": 1, "17": 1, "32": 0, "35": 1},
                 "dice": {"Dice2": "50$", "Dice3": "50$", "Dice4": "50$", "Dice5": "200$", "Dice6": "50$"},
                 "cart": {"Milk": 1, "Yugurt": 0, "Bananas": 1, "Choclate fudge": 0}},
        "low": {"image": {}, "dice": {"Dice2": "200$", "Dice4": "200$", "Dice6": "200$"},
                "cart": {"Coco pops": 0, "Nature Vally": 0}}}}

new_dizzy_questions = {
    "Decision": [
        {
            "QuestionType": "Island",
            "QuestionId": "Decision1",
            "QuestionText": "You're in a sinking ship with 10 other people. There is one small lifeboat, with room for only one person or else the lifeboat would sink. Who would you save (besides yourself)? ",
            "QuestionCode": "101",
            "Options": [
                {
                    "Value": "A 35 year old sailor",
                    "Score": 30
                },
                {
                    "Value": "A heroic fighter",
                    "Score": 25
                },
                {
                    "Value": "A 26 year old college",
                    "Score": 18
                },
                {
                    "Value": "A rich charity worker",
                    "Score": 15
                },
                {
                    "Value": "blind ten years old",
                    "Score": 13
                },
                {
                    "Value": "your favourite celebrity",
                    "Score": 11
                },
                {
                    "Value": "pregnant women",
                    "Score": 7
                },
                {
                    "Value": "2 year old",
                    "Score": 2
                }
            ],
            "QuestionPsychology": psychology_types
        },
        {
            "QuestionType": "Island",
            "QuestionId": "Decision2",
            "QuestionText": "What songs would you take to a desert island?",
            "QuestionCode": "100",
            "Options": [
                {
                    "Value": "Schemin' Up - Drake",
                    "Score": 0
                },
                {
                    "Value": "Boston - Kenny Chesney",
                    "Score": 0
                },
                {
                    "Value": "Music is All We Got - Chance the Rapper",
                    "Score": 0
                },
                {
                    "Value": "Raised by a Good Time - Steven Lee",
                    "Score": 0
                },
                {
                    "Value": "American Pie - don Mclean",
                    "Score": 0
                },
                {
                    "Value": "Unfaithful\" - Rihanna",
                    "Score": 0
                },
                {
                    "Value": "Fly Me to the Moon - Frank Sinatra",
                    "Score": 0
                },
                {
                    "Value": "Sweet Caroline - Neil Diamond",
                    "Score": 0
                }
            ],
            "QuestionPsychology": psychology_types
        }
    ]
}

myfugo_new_scoring_quesitions = {
    "301": {
        "brown": 15,
        "white": 10,
        "black": 5,
        "grey": 1
    },
    "302": {
        "land": 10,
        "bike": 4,
        "car": 1,
        "house": 5,
        "cattle": 15
    },
    "303": {
        "nofarm": 15,
        "twocows": 10,
        "vegetable": 8,
        "poultry": 2,
        "manycows": 8
    },
    "304": {
        "bull": 10,
        "specialist": 5
    }
}

bridge2capital_new_scoring_quesitions = {
    "301": {
        "shopping": 5,
        "reading": -5,
        "running": -10,
        "walking": 10,
        "lying": 15
    },
    "302": {
        "one": -10,
        "group": -15,
        "champion": 10
    },
    "303": {
        "game1": 5,
        "game2": -5,
        "game3": -10,
        "game4": 10,
        "game5": 15
    },
    "304": {
        "person1": 5,
        "person2": 10,
        "person3": -10,
        "person4": -15,
        "person5": 15
    },
    "305": {
        "thing1": 15,
        "thing2": 10,
        "thing3": 5,
        "thing4": -15
    }
}


# endregion

def get_database():
    client = MongoClient(conf.MongoDB.uri)
    db = client[conf.MongoDB.db]
    return db


def partner_scoring():
    """
    update question collection by adding Partner Scoring Factors into all common info questions
    """
    db = get_database()
    questions = db['questions']
    scoring_factors = {
        'Text_Analyzer': 0.5,
        'Sentiment': 0.2,
        'Images': 0.3
    }
    try:
        for question_document in questions.find():
            common_info = dict()
            common_info['scoring_factors'] = scoring_factors
            question_document['partner_info'] = common_info
            questions.save(question_document)
        print("Partner scoring has been added into all partners")
    except Exception as e:
        print(e)


def add_email_notification(email_notification_data, partners=[]):
    """
    adding email notification attributes (to & cc) into questions.email_notifications for partners: Bridge2capital
    and b2c_hindi
    """
    db = get_database()
    questions = db['questions']
    try:
        # Get Questions For Given Partners
        query = {'partner': {'$in': partners}}
        partner_questions = questions.find(query)
        for partner_question in partner_questions:
            email_notifications = dict()
            email_notifications['to'] = email_notification_data['to']
            email_notifications['cc'] = email_notification_data['cc']
            partner_question['email_notifications'] = email_notifications
            questions.save(partner_question)
        print("Email Notifications Added into partners", partners)
    except Exception as e:
        print(e)


def add_collection_get_typeform_scores():
    """
    adding new collection 'get_typeform_scores' to database
    """
    # Create Collection 'get_typeform_scores' if not exists
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "get_typeform_scores" in col_list:
            print("collection get_typeform_scores already exists.")
        else:
            get_typeform_scores = db['get_typeform_scores']
            # insert for first time only to create collection
            result = get_typeform_scores.insert_one({'test': 1})
            get_typeform_scores.delete_one({'_id': result.inserted_id})
            print("collection get_typeform_scores has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection get_typeform_scores")


def add_dizzy_psychology_questions_types():
    '''
    add dizzy personality question options to dizzy collection
    '''
    try:
        client = MongoClient(conf.MongoDB.uri)
        db = client[conf.MongoDB.db]
        questions = db['questions']
        query = {'partner': "dizzy"}
        partner_questions = questions.find_one(query)
        for question_type in partner_questions['questions']:
            q_types = partner_questions.get('questions').get(question_type)
            for question in q_types:
                question['QuestionPsychology'] = "Others"
            # partner_questions[f'questions.{question_type}'] = q_types
            questions.save(partner_questions)
        print("psychology types added to question successfully")
    except Exception as e:
        print("An Error Occurred While adding questions psychology types" + str(e))


def add_dizzy_correct_answer(question_types):
    '''
    adding a flag that indicates the correct answer for each option
    '''
    try:
        client = MongoClient(conf.MongoDB.uri)
        db = client[conf.MongoDB.db]
        questions = db['questions']
        query = {'partner': "dizzy"}
        partner_questions = questions.find_one(query)
        for q_type in question_types:
            q = partner_questions.get('questions').get(q_type)
            for question in q:
                options = question.get("Options") if not (q_type == "Image") else question.get("Images")
                for o in options:
                    o['isCorrect'] = random.randint(0, 1)
        questions.save(partner_questions)
        print("psychology is correct answers added to question successfully")
    except Exception as e:
        print("An Error Occurred While adding questions psychology types" + str(e))


def add_dizzy_psychology_questions_types_v2():
    try:
        client = MongoClient(conf.MongoDB.uri)
        db = client[conf.MongoDB.db]
        questions = db['questions']
        query = {'partner': "dizzy"}
        partner_questions = questions.find_one(query)
        matched_psychology_types = []
        for question_type in partner_questions['questions']:
            q_types = partner_questions['questions'].get(question_type)
            for question in q_types:
                if question_type == "Select" or question_type == "dice":
                    question_id = question['QuestionId']
                elif question_type == "Cart":
                    question['QuestionPsychology'] = psychology_types
                    continue
                else:
                    question_id = question['QuestionId'].replace('Image', '')
                matched_psychology_types.clear()
                for psych in dizzy_questions_scores:
                    question_type = "dice" if question_type == "Select" else question_type
                    high = dizzy_questions_scores[psych].get('high').get(question_type.lower())
                    high_keys = [k for k in high]
                    low = dizzy_questions_scores[psych].get('low').get(question_type.lower())
                    low_keys = [k for k in low]
                    all_keys = list(set(high_keys + low_keys))
                    if question_id in all_keys:
                        matched_psychology_types.append(psych)
                question['QuestionPsychology'] = list(set(matched_psychology_types))
            # partner_questions[f'questions.{question_type}'] = q_types
            questions.save(partner_questions)
    except Exception as e:
        print("An Error Occurred While adding questions psychology types" + str(e))


def insert_dizzy_question_document():
    '''
    create questions collection for patner dizzy
    '''
    # Create document 'dizzy' if it does not exist
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': "dizzy"}
        result = questions.find_one(query)
        if result is None:  # partner dizzy isn't inserted yet
            document_data = {
                'partner': 'dizzy',
                'common_info': {},
                'questions': {},
                'partner_info': {},
                'email_notifications': {},
            }
            questions.insert_one(document_data)
            print("partner dizzy questions document created successfully.")
        else:
            print("partner dizzy questions already exist.")

    except Exception as e:
        print("an error occurred while inserting document dizzy in questions collection")


def insert_dizzy_dice_cart_questions(questions_text, questions_answers, question_type):
    try:
        # create select object filed in questions of it doesn't exist
        cart_folder_path = '/static/images/cart/'
        db = get_database()
        questions = db['questions']
        query = {'partner': "dizzy"}
        dizzy_questions = questions.find_one(query)
        if dizzy_questions is None:
            raise Exception("partner dizzy has no document in questions collection")
        new_questions = []

        for q in questions_text:
            select_question = {
                'QuestionType': question_type,
                'QuestionId': 'Cart1',
                'QuestionText': q,
                'QuestionCode': None,
                'Options': []
            }
            options = []
            cart_images_path = next(os.walk('..' + cart_folder_path))[2]
            for opt in questions_answers:
                cart_img = [x for x in cart_images_path if opt.lower() in x.lower()]
                cart_img = cart_img[0] if len(cart_img) > 0 else ""
                options.append({'value': opt, 'score': 0, 'url': cart_folder_path + cart_img})
            select_question['Options'] = options
            new_questions.append(select_question)
        all_questions = dizzy_questions['questions']
        all_questions[question_type] = new_questions
        dizzy_questions['questions'] = all_questions
        questions.save(dizzy_questions)
        print("dizzy {0} questions added successfully".format(question_type))
    except Exception as e:
        print("An Exception Occurred while inserting {0} questions".format(question_type) + str(e))


def insert_dizzy_image_questions(questions_text, folder_path, question_type):
    try:
        # create select object filed in questions of it doesn't exist
        db = get_database()
        questions = db['questions']
        query = {'partner': "dizzy"}
        dizzy_questions = questions.find_one(query)
        if dizzy_questions is None:
            raise Exception("partner dizzy has no document in questions collection")
        new_questions = []
        image_questions_folders = next(os.walk(folder_path))[1]

        for i in range(1, len(image_questions_folders) + 1):
            select_question = {
                'QuestionType': question_type,
                'QuestionId': "Image" + str(i),
                'QuestionText': questions_text,
                'QuestionCode': None,
                'Images': []
            }

            question_images_path = next(os.walk(folder_path + "Question " + str(i)))[2]
            for image_name in (question_images_path):
                image_data = dict()
                image_data['image_id'] = image_name
                image_data['score'] = 0
                image_data['url'] = folder_path.replace('..', '') + "Question {0}/".format(str(i)) + image_name
                select_question['Images'].append(image_data)

            new_questions.append(select_question)
        all_questions = dizzy_questions['questions']
        all_questions[question_type] = new_questions
        dizzy_questions['questions'] = all_questions
        questions.save(dizzy_questions)
        print("dizzy {0} questions added successfully".format(question_type))
    except Exception as e:
        print("An Exception Occurred while inserting {0} questions".format(question_type) + str(e))


def create_dizzy_collection():
    """
    adding new collection 'dizzy' to database
    """
    # Create Collection 'dizzy' if not exists
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "dizzy" in col_list:
            print("collection dizzy already exists.")
        else:
            get_typeform_scores = db['dizzy']
            # insert for first time only to create collection
            result = get_typeform_scores.insert_one({'test': 1})
            get_typeform_scores.delete_one({'_id': result.inserted_id})
            print("collection dizzy has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection dizzy")


def replacing_epaysa_questions_with_dizzy():
    """
    replacing epaysa question with dizzy question's
    """
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "dizzy" not in col_list or "epaysa" not in col_list:
            print("collection dizzy or epaysa does not  exists.")
        else:
            questions = db['questions']
            query = {'partner': "dizzy"}
            dizzy_questions = questions.find_one(query)
            query = {'partner': "epaysa"}
            epaysa_questions = questions.find_one(query)
            epaysa_questions['questions'] = dizzy_questions['questions']
            questions.save(epaysa_questions)
            print("epaysa questions has been replaced with dizzy questions successfully.")
    except Exception as e:
        print("an error occurred while replacing epaysa questions with dizzy questions")


def create_LM_Financials_collection():
    """
    adding new collection 'LM_financials' to database
    """
    # Create Collection 'LM_financials' if not exists
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "LM_financials" in col_list:
            print("collection LM_financials already exists.")
        else:
            get_typeform_scores = db['LM_financials']
            # insert for first time only to create collection
            result = get_typeform_scores.insert_one({'test': 1})
            get_typeform_scores.delete_one({'_id': result.inserted_id})
            print("collection LM_financials has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection LM_financials")


def insert_LM_financials_question_document():
    '''
    inserting LM_financials new document to questions collection
    '''
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': "LM_financials"}
        result = questions.find_one(query)
        if result is None:  # partner LM_financials isn't inserted yet
            document_data = {
                'partner': 'LM_financials',
                'common_info': {},
                'questions': {},
                'partner_info': {},
                'email_notifications': {},
            }
            questions.insert_one(document_data)
            print("partner LM_financials questions document created successfully.")
        else:
            print("partner LM_financials questions already exist.")

    except Exception as e:
        print("an error occurred while inserting document LM_financials in questions collection")


def replacing_LM_financials_questions_with_dizzy():
    """
    replacing LM_financials question with dizzy question's
    """
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "dizzy" not in col_list or "LM_financials" not in col_list:
            print("collection dizzy or LM_financials does not  exists.")
        else:
            questions = db['questions']
            query = {'partner': "dizzy"}
            dizzy_questions = questions.find_one(query)
            query = {'partner': "LM_financials"}
            LM_financials_questions = questions.find_one(query)
            LM_financials_questions['questions'] = dizzy_questions['questions']
            questions.save(LM_financials_questions)
            print("LM_financials questions has been replaced with dizzy questions successfully.")
    except Exception as e:
        print("an error occurred while replacing LM_financials questions with dizzy questions")


def create_loan_status_collection():
    """
    adding new collection 'loan_status' to database
    """
    # Create Collection 'loan_status' if not exists
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "loan_status" in col_list:
            print("collection loan_status already exists.")
        else:
            loan_status = db['loan_status']
            # insert for first time only to create collection
            result = loan_status.insert_one({'test': 1})
            loan_status.delete_one({'_id': result.inserted_id})
            print("collection loan_status has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection loan_status {}".format(e))


def update_image_scoring(partner, new_image_scores):
    """update scoring for image questions"""
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': partner}
        myfugo_questions = questions.find_one(query)
        myfugo_image_questions = myfugo_questions['questions'].get("Image")
        for q in myfugo_image_questions:
            q_id = q['QuestionId']
            images = q['Images']
            new_scores = new_image_scores.get(q_id)
            for img in images:
                image_id = img['image_id']
                img['score'] = new_scores[image_id]
        questions.save(myfugo_questions)
    except Exception as e:
        print("an error occurred while updating {} image scores {}".format(partner, e))


def add_new_dizzy_questions():
    """adding new dizzy island songs and sinking boat questions"""
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': 'dizzy'}
        dizzy_questions_collection = questions.find_one(query)
        dizzy_questions = dizzy_questions_collection['questions']
        dizzy_questions['Decision'] = new_dizzy_questions['Decision']
        questions.save(dizzy_questions_collection)
        print('new dizzy questions (sinking boat and desert island songs has been added successfully)')
    except Exception as e:
        print("an error occurred while updating new dizzy questions {}".format(e))


def update_myfugo_loan_common_info():
    """updating myfugo common info cap loan"""
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': 'myfugo'}
        myfugo_questions_collection = questions.find_one(query)
        myfugo_common_info = myfugo_questions_collection['common_info']
        loan_info = myfugo_common_info['loan']
        loan_info['total_amount']['max'] = 5000
        loan_info['total_amount']['step'] = 100
        loan_info['period'] = 'day'
        questions.save(myfugo_questions_collection)
        print('myfugo common info cap loan has been updated')
    except Exception as e:
        print("an error occurred while updating myfugo common info {}".format(e))


def create_new_partner_hedgini():
    """
    adding new collection 'hedgini' to database
    """
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "hedgini" in col_list:
            print("collection hedgini already exists.")
        else:
            get_typeform_scores = db['hedgini']
            # insert for first time only to create collection
            result = get_typeform_scores.insert_one({'test': 1})
            get_typeform_scores.delete_one({'_id': result.inserted_id})
            print("collection hedgini has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection hedgini")


def insert_hedgini_question_document():
    '''
    create questions collection for patner hedgini
    '''
    # Create document 'dizzy' if it does not exist
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': "hedgini"}
        result = questions.find_one(query)
        if result is None:  # partner hedgini isn't inserted yet
            document_data = {
                'partner': 'hedgini',
                'common_info': {},
                'questions': {},
                'partner_info': {},
                'email_notifications': {},
            }
            questions.insert_one(document_data)
            print("partner hedgini questions document created successfully.")
        else:
            print("partner hedgini questions already exist.")

    except Exception as e:
        print("an error occurred while inserting document hedgini in questions collection")


def create_new_partner_creditAI():
    """
    adding new collection 'creditAI' to database
    """
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "creditAI" in col_list:
            print("collection creditAI already exists.")
        else:
            get_typeform_scores = db['creditAI']
            # insert for first time only to create collection
            result = get_typeform_scores.insert_one({'test': 1})
            get_typeform_scores.delete_one({'_id': result.inserted_id})
            print("collection creditAI has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection creditAI")


def insert_creditAI_question_document():
    '''
    create questions collection for patner creditAI
    '''
    # Create document 'creditAI' if it does not exist
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': "creditAI"}
        result = questions.find_one(query)
        if result is None:  # partner creditAI isn't inserted yet
            document_data = {
                'partner': 'creditAI',
                'common_info': {},
                'questions': {},
                'partner_info': {},
                'email_notifications': {},
            }
            questions.insert_one(document_data)
            print("partner creditAI questions document created successfully.")
        else:
            print("partner creditAI questions already exist.")

    except Exception as e:
        print("an error occurred while inserting document creditAI in questions collection")


def copy_questions_between_partners():
    '''
    copy questions between partner
    dizzy to creditAI
    LM_financials to Hedgini
    '''
    try:
        db = get_database()
        questions = db['questions']
        query = {'partner': {"$in": ["dizzy", "LM_financials"]}}
        src = list(questions.find(query))
        query = {'partner': {"$in": ["creditAI", "hedgini"]}}
        dst = list(questions.find(query))
        for partner in src:
            # copy from dizzy to creditAI
            if partner.get('partner') == 'dizzy':
                creditAI = [p for p in dst if p.get('partner') == 'creditAI'][0]
                creditAI['questions'] = partner.get('questions')
            # copy from LM_financials to Hedgini
            if partner.get('partner') == 'LM_financials':
                hedgini = [p for p in dst if p.get('partner') == 'hedgini'][0]
                hedgini['questions'] = partner.get('questions')
        for paratner in dst:
            questions.save(paratner)
        print('questions have been copied from dizzy and LM to credit and hedgini')
    except Exception as e:
        print("an error occurred while copying date between partner")


def add_collection_scenario_scoring():
    """
    adding new collection 'scenario_scoring' to database
    """
    # Create Collection 'scenario_scoring' if not exists
    try:
        db = get_database()
        col_list = db.list_collection_names()
        if "scenario_scoring" in col_list:
            print("collection scenario_scoring already exists.")
        else:
            get_typeform_scores = db['scenario_scoring']
            # insert for first time only to create collection
            result = get_typeform_scores.insert_one({'test': 1})
            get_typeform_scores.delete_one({'_id': result.inserted_id})
            print("collection scenario_scoring has been created successfully.")
    except Exception as e:
        print("an error occurred while creating collection scenario_scoring")


def all_dizzy_image_qiestions():
    db = get_database()
    questions = db['questions']
    query = {'partner': 'dizzy'}
    myfugo_questions = questions.find_one(query)
    questions = myfugo_questions['questions']
    image_q = questions['Image']
    res = dict()
    for q in image_q:
        if q.get('QuestionId') not in ['Image21', 'Image35', 'Image18']:
            res[q.get('QuestionId')] = q.get('QuestionPsychology')

    return print(res)


def rename_collection_smartcredit():
    try:
        db = get_database()
        smartcredit_collection = db['smartcredit']
        smartcredit_collection.rename("VOua8PmXQC")
        print("collection smartcredit has been renamed")
    except Exception as e:
        print("an error occurred while renaming collection smartcredit")


if __name__ == '__main__':
    rename_collection_smartcredit()
    pass
