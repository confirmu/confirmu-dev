from wtforms import Form, validators, StringField, BooleanField, SubmitField, PasswordField


class ItraveleoForm(Form):
    email = StringField('Email', validators=[validators.DataRequired('Input email')])
    customer_name = StringField('Name', validators=[validators.DataRequired('Input name')])
    question_1 = StringField('Are you traveling alone?', validators=[validators.DataRequired('Answer first question')])
    question_2 = StringField('Is this your first vacation this year?')
    question_3 = StringField('Where do you plan to stay? Any favorite hotels?')
    question_4 = StringField('Are you looking forward to going to a bar or a restaurant every night? ')
    question_5 = StringField(
        'Your destination has so many cool things to do! What kind of activities do you plan to enjoy?')
    question_6 = StringField('How do you plan to make the most of all the sites and activities in your destination?')
    question_7 = StringField('Do you like to hangout with locals or prefer to chill with your travel companions?')
    question_8 = StringField(
        'Imagine you’ve lost your baggage on your way from the airport to hotel. What would you do?')
    question_9 = StringField(
        'As you are walking on the street at night two locals approach you and invite you to hangout with them at a local pub. Will you join them?')
    question_10 = StringField('What do you like to do most while on vacation?')
    question_11 = StringField('What do you think is the best thing about traveling?')
    question_12 = StringField('How often do you travel? Is it mostly for work or leisure?')
    question_13 = StringField('Do you have a travel goal? Or a destination you would most like to visit?')
    question_14 = StringField('What is the top criteria that helps you select your vacation destination?')
    question_15 = StringField('Do you feel disheartened to about going back home as your trip comes to an end?')
    question_16 = StringField('Have you ever taken a loan and repaid it completely?')
    question_17 = StringField(
        'Imagine you are in formal gathering and are required to network with other people. How would you start the conversation? ')
    question_18 = StringField('Do you find yourself refraining from arguments or confronting them?')
    question_19 = StringField('How would you best describe your work?')
    question_20 = StringField('How do you think your closest friends would describe you?')
    question_21 = StringField(
        'When someone is raising a point on a Whatsapp group or any other instant messaging platform, do you find yourself eager to reply immediately?')
    question_22 = StringField(
        'Your boss asks you to stay up all night to respond to an email although you are not getting paid for it. What will you do?')
    question_23 = StringField(
        'You are promoted and will be your best friend’s senior at work. How will you interact with him/her now?')
    question_24 = StringField(
        'Imagine you have missed work deadlines due to negligence. What explanation will you give your boss? ')
    question_25 = StringField(
        'Just before you are about to go up on stage for a presentation you find a stain on your shirt, with no spare shirt around. What will you do?')
    question_26 = StringField(
        'Your partner got promoted at work and is now required to work late. You’ll hardly see him / her at home. What will you do?')
    question_27 = StringField(
        'When the cashier at the supermarket hands you your bill, you realize that the prices are higher than you expected. How will you react?')


class IlendCarForm(Form):
    email = StringField('Email')
    customer_name = StringField('Name')
    question_1 = StringField('Who\'s the car loan for ?')
    question_2 = StringField('How long do you have a drivers license?')
    question_3 = StringField('What do you like about driving?')
    question_4 = StringField('Can you describe what is your favourite kind of car?')
    question_5 = StringField('What made you buy a car and why borrow money for it?')
    question_6 = StringField('How do you navigate on roads to your destination?')
    question_7 = StringField('Do you go to the garage sometimes or you handle it on your own?')
    question_8 = StringField('If you were to choose between a company leased car and private owned car what would you choose?')
    question_9 = StringField('What\'s your typical reaction when another driver passes you on your queue when you are about to make a turn?')
    question_10 = StringField('Did you ever had a car which you were really attached to?why?')
    question_11 = StringField('As you scheduled an appointment you find your self stuck on a traffic jam but with no possibility to estimate whether you gonna be late or not,what will you then?')
    question_12 = StringField('Ther\'es now a signal on your car showing that within 3000 kilometers you would need to have the car get into the a regular checkup at the garage and next week annual license expires and car has to be tested what will you do?')
    question_13 = StringField('Auto trades have a new deal on car trade in deal where if you sell your car to them with additional fee you can get one new but you find out that if you sell your car and take a loan for 3 years this maybe cheaper now but expensive later,which option will you pick')


class IlendHomeForm(Form):
    email = StringField('Email')
    customer_name = StringField('Name')
    question_1 = StringField('So...is this your first house?')
    question_2 = StringField('Why did you choose this house and this current location?')
    question_3 = StringField('What  do you like most to do at home?')
    question_4 = StringField('There\'s a small leak from one of the pipes in the kitchen , it is not something significant will you fix now?')
    question_5 = StringField('Did you do a maintainance due diligence process before taking the decision on buying the house?')
    question_6 = StringField('There\'s a new family who recently moved in to the floor where you live in and they are making awful noise and are bringing friends who stay up late and make dirt in the public corridor,are you gonna talk to them or try something else, if so what will you?')
    question_7 = StringField('The house committee decides to raise the monthly fees being collects from residents by 50% for on building a stairway for one of the old cripple neighbors in the house, will you comply with that?')
    question_8 = StringField('As prices of real estate are on the rise you find your self getting an offer from a real estate broker to sell your house in a 20% gain after only 1 year of stay , will you accept the offer?')
    question_9 = StringField('Do you keep all doors locked at night?')
    question_10 = StringField('Do you like to entertain guests at home?')
    question_11 = StringField('You find out that your aircondition is broken and the technician wants to charge for the fix a huge amount of money which is a heavy burden on your current cash flow, how do you plan to carry the payments?')
    question_12 = StringField('Do you check once in a while if you can refinance your long term debt mortgage?')


class IlendWeddingForm(Form):
    email = StringField('Email')
    customer_name = StringField('Name')
    question_1 = StringField('Whose getting married?')
    question_2 = StringField('Wedding loan is for you or other family relative?')
    question_3 = StringField(
        'Where are you planning on holding the marriage ceremony, a wedding hall or private with family?')
    question_4 = StringField(
        'Can you please share any previous commitment arrangement where you made repayed entire debt?')
    question_5 = StringField('Do you plan on a honeymoon after the wedding?')
    question_6 = StringField('If so where are you planning on travelling?')
    question_7 = StringField(
        'As you arrive to select the wedding hall with your partner the manager of the hall decides he now wants to charge you 25%  for the use of the hall,what will you do?')
    question_8 = StringField(
        'You enter into a meetup and and theres a mingling session and you are actually required to network with other people how will you start the conversation?')
    question_9 = StringField(
        'Your boss have gave you 5 targets to commit by end of day yet there are many distrubances how will you get the job done by end of day?')
    question_10 = StringField('Do you find yourself refrain from arguments?')
    question_11 = StringField('Some people are happy with the way things are in their lifes how about you?')
    question_12 = StringField('Whats best describes you in your work?')
    question_13 = StringField('Can you share a challenge in your life where you failed to achieve goals you set out ?')
    question_14 = StringField('How do you think your closest friends perceive you ?')
    question_15 = StringField(
        'When someone is raising a point whether on a whatsapp group or any other discussion platform do you find yourself  eager to answer him right now?')
    question_16 = StringField(
        'Assuming you have a zero risk investment carrying annual interest of 5% vs a volatile but guaranateed return after 5 years of 50% what will you choose and why?')
    question_17 = StringField(
        'Your boss asks you to stay all night and wait to respond to an email although you are not getting paid for that what will you do?')
    question_18 = StringField(
        'You are now appointed at work to be a project manager, that means you will need to run someone who’se your best friend at work how will you interact with him now?')
    question_19 = StringField(
        'Your\'e now being asked by your boss why you have missed the current targets and deadlins, you know its cause you ignored problems which you didnt look at, what kind of an explanation will you provide ?')
    question_20 = StringField(
        'Your boss has quit is job and you have a new manager which barely talks to you how can you handle that?')
    question_21 = StringField(
        'Your\'e about to go up on the stage and pitch however a minute before you find a stain on your shirt with no spare shirt what will you do?')
    question_22 = StringField(
        'your partner has just got promoted at work and is now being required to work late, you hardly see him,what will you do?')
    question_23 = StringField(
        'After you finished collecting shopping at the supermarket you turn to the cashier and as the bill is handed to you you realize the prices are very high than you expected,what will you?')


class TutuForm(Form):
    email = StringField('Email')
    customer_name = StringField('Name')
    question_1 = StringField('Para quem é o empréstimo do carro?')
    question_2 = StringField('Quanto tempo você possui uma carteira de motorista?')
    question_3 = StringField('O que você gosta de dirigir?')
    question_4 = StringField('Você pode descrever qual é o seu tipo favorito de carro?')
    question_5 = StringField('O que fez você comprar um carro e por que emprestar dinheiro para isso?')
    question_6 = StringField('Como você navega nas estradas para o seu destino?')
    question_7 = StringField('Você vai à garagem às vezes ou você lida com isso sozinho?')
    question_8 = StringField(
        'Se você fosse escolher entre uma empresa de carro alugado e um carro de propriedade privada, o que você escolheria?')
    question_9 = StringField(
        'Qual é a sua reação típica quando outro motorista o passa na sua fila quando está prestes a dar uma volta?')
    question_10 = StringField('você já teve um carro ao qual você estava realmente ligado? Por quê?')
    question_11 = StringField(
        'À medida que agendou um compromisso, você encontra o seu auto preso em um engarrafamento, mas sem possibilidade de estimar se você vai atrasar ou não, o que você vai fazer?')
    question_12 = StringField(
        'Agora, um sinal no seu carro mostra que dentro de 3000 quilômetros você precisaria ter o carro entrar no check-up regular na garagem e na próxima semana expira a licença anual e o carro deve ser testado o que você fará?')
    question_13 = StringField(
        'Os negócios automotivos têm um novo acordo sobre o comércio de carros no negócio onde se você vender seu carro para eles com uma taxa adicional, você pode obter um novo, mas descobre que se você vende seu carro e pegue um empréstimo por 3 anos, isso talvez seja mais barato agora, mas caro mais tarde, qual opção você escolherá?')


class NineFieldsForm(Form):
    email = StringField()
    customer_name = StringField()
    question_1 = StringField()
    question_2 = StringField()
    question_3 = StringField()
    question_4 = StringField()
    question_5 = StringField()
    question_6 = StringField()
    question_7 = StringField()
    question_8 = StringField()
    question_9 = StringField()


class FiveFieldForm(Form):
    email = StringField()
    customer_name = StringField()
    question_1 = StringField()
    question_2 = StringField()
    question_3 = StringField()
    question_4 = StringField()
    question_5 = StringField()


class OneFieldForm(Form):
    text = StringField('Text to analyze separated with ";" or every single word')


class AuthenticationForm(Form):
    password = StringField('Enter password:')


class Credit24ChangeQuestionsForm(Form):
    question_1h = StringField('question #1, Hebrew')
    question_2h = StringField('question #2, Hebrew')
    question_3h = StringField('question #3, Hebrew')
    question_4h = StringField('question #4, Hebrew')
    question_5h = StringField('question #5, Hebrew')
    question_1e = StringField('question #1, English')
    question_2e = StringField('question #2, English')
    question_3e = StringField('question #3, English')
    question_4e = StringField('question #4, English')
    question_5e = StringField('question #5, English')
    password = PasswordField('Password to apply changes')


class OMLP2PChangeQuestionsForm(Form):
    question_1h = StringField('question #1, Hindi')
    question_2h = StringField('question #2, Hindi')
    question_3h = StringField('question #3, Hindi')
    question_4h = StringField('question #4, Hindi')
    question_5h = StringField('question #5, Hindi')
    question_6h = StringField('question #6, Hindi')
    question_7h = StringField('question #7, Hindi')
    question_8h = StringField('question #8, Hindi')
    question_9h = StringField('question #9, Hindi')
    question_1e = StringField('question #1, English')
    question_2e = StringField('question #2, English')
    question_3e = StringField('question #3, English')
    question_4e = StringField('question #4, English')
    question_5e = StringField('question #5, English')
    question_6e = StringField('question #6, English')
    question_7e = StringField('question #7, English')
    question_8e = StringField('question #8, English')
    question_9e = StringField('question #9, English')
    password = PasswordField('Password to apply changes')


class DynamicForm(Form):
    pass


class LoginForm(Form):
    username = StringField('Username', validators=[validators.DataRequired()])
    password = PasswordField('Password', validators=[validators.DataRequired()])
    submit = SubmitField('Sign In')


class NameEmailForm(Form):
    email = StringField('Email')
    customer_name = StringField('Name')
