import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Step1Component } from './containers/welcome/step1/step1.component';
import { Step2Component } from './containers/welcome/step2/step2.component';
import { SummaryComponent } from './containers/summary/summary.component';
import { ErrorComponent } from './shared/components/error/error.component';
import { ImageComponent } from './containers/questions/image/image.component';
import { CartComponent } from './containers/questions/cart/cart.component';
import { DiceComponent } from './containers/questions/dice/dice.component';
import { ErrorRoutingComponent } from './shared/error-routing/error-routing.component';
import { StatusComponent } from './containers/status/status.component';
const routes: Routes = [
  {
    path: ':partner/:id',
    pathMatch: 'full',
    component: Step1Component
  },
  {
    path: 'step2',
    component: Step2Component
  },
  {
    path: 'image',
    component: ImageComponent
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'dice',
    component: DiceComponent
  },
  {
    path: 'summary',
    component: SummaryComponent
  },
  {
    path: 'status',
    component: StatusComponent
  },
  {
    path: 'error',
    component: ErrorComponent
  },
  {
    path: '**',
    component: ErrorRoutingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
