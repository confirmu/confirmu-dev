import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../../../core/services/rest-api.service';
import { isUndefined } from 'util';
import { TranslateService } from '@ngx-translate/core';
import { spentTimeCategories } from 'src/app/shared/models/app.constants';

@Component({
  selector: 'app-dice',
  templateUrl: './dice.component.html',
  styleUrls: ['./dice.component.scss']
})
export class DiceComponent implements OnInit {
  diceOptions = [];
  diceQuestion;
  diceQuestionAnswers = [];
  monsterMsg: string;
  items = [];
  optionSelected = false;
  startingTime;
  constructor(private router: Router, public restApi: RestApiService, private translateService: TranslateService) {
    this.resetMonsterMsg();
  }

  ngOnInit() {
    this.startingTime = this.restApi.getCurrentTime();
    this.nextQuestion(null, null);
  }

  continue() {
    this.restApi.setSpentTime(spentTimeCategories.dice, { spentTime: this.restApi.getCurrentTime() - this.startingTime });
    this.router.navigateByUrl('summary', { skipLocationChange: true });
  }

  changeMonsterMsg() {
    this.monsterMsg = this.translateService.instant('dice_msg');
  }

  resetMonsterMsg() {
    this.monsterMsg = this.translateService.instant('swipe_left_msg');
  }

  goToNextQuestion(option, questionId) {
    if ( !this.optionSelected ) {
      this.optionSelected = true;
      this.items.push(option);
      this.changeMonsterMsg();
      setTimeout(() => {
      this.resetMonsterMsg();
      this.nextQuestion(questionId, option);
     }, 1000);
    }
  }

  nextQuestion(questionId, selectedItem) {
    this.diceOptions = [];
    this.items = [];
    if (!isUndefined(this.diceQuestion)) {
      this.diceQuestionAnswers.push({
        QuestionId: questionId, QuestionType: this.diceQuestion.QuestionType, QuestionAnswer: selectedItem
      });
    }
    this.diceQuestion = this.restApi.getNextQuestion('Dice');
    if (isUndefined(this.diceQuestion)) { // last dice quesiton reached
      this.restApi.diceAnswers = this.diceQuestionAnswers;
      this.continue(); // route to next type of questions
    } else if (this.diceQuestion === -1) {
      // no dice questions available
    } else {
      this.diceQuestion.Options.forEach(opt => {
        this.diceOptions.push(opt.value);
      });
    }
    this.optionSelected = false;
  }
}
