import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ImageComponent } from './image/image.component';
import { CartComponent } from './cart/cart.component';
import { DiceComponent } from './dice/dice.component';
import { FormsModule } from '@angular/forms';
@NgModule({
    declarations: [
        CartComponent,
        ImageComponent,
        DiceComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule.forRoot()
    ],
    providers: [],
    entryComponents: [],
    exports: [
        ImageComponent,
        CartComponent,
        DiceComponent
    ],
})

export class QuestionsModule { }
