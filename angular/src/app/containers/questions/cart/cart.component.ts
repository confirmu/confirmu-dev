import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../../../core/services/rest-api.service';
import { TranslateService } from '@ngx-translate/core';
import { isUndefined } from 'util';
import Hammer from 'hammerjs';
import { spentTimeCategories } from '../../../shared/models/app.constants';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})

export class CartComponent implements OnInit {
  cartOptions = [];
  items = [];
  monsterMsg: string;
  cartQuestionAnswers = [];
  cartQuestion;
  data = [];
  currentDraggedItem;
  isDragging = false;
  lastPosY = 0;
  lastPosX = 0;
  top = 8;
  left = 8;
  styleOnce = 0;
  itemDropped = false;
  startingTime;
  inCartItems = [];
  registeredBefore = false;
  hasVibrated = false;
  constructor(private router: Router, public restApi: RestApiService, private translateService: TranslateService) {
    this.resetMonsterMsg();
  }

  ngOnInit() {
    this.startingTime = this.restApi.getCurrentTime();
    this.nextQuestion();
    // window.onresize = (ev) => {
    //   this.styleOnce = 0;
    //   this.left = 8;
    //   this.top = 8;
    // };
  }

  initCartItem(i) {
    if (this.styleOnce < this.cartOptions.length) {
      const imagesContainer = document.getElementsByClassName('imges-container')[0] as HTMLElement;
      const windowWidth = window.innerWidth;
      const imgesContainerWidth = windowWidth - (20 * 2);
      // calcualte the needed height to place all items in the container
      imagesContainer.style.height = Math.ceil((this.cartOptions.length * 60) / imgesContainerWidth) * 60 + 8 + 'px';
      this.styleOnce++;
      const styles = `left:${this.left + 'px'}; top:${this.top + 'px'}`;
      this.left += 60;
      if (this.left >= imgesContainerWidth - 40) { //  max container width reached
        this.top += 60;
        this.left = 8;
      }
      const eleItem = document.getElementById(i);
      eleItem.setAttribute('style', styles);
      eleItem.style.backgroundImage = `url(${this.data[i]})`;
      if (!this.registeredBefore ) {
        const mc = new Hammer(eleItem);
        mc.add(new Hammer.Pan({ direction: Hammer.DIRECTION_ALL, threshold: 0 }));
        mc.on('pan', this.handleDrag);
      }
    } else {
      this.registeredBefore = true;
    }
  }

  handleDrag = (ev) => {
    const elem = ev.target;
    if (!this.isDragging) {
      this.isDragging = true;
      this.lastPosX = elem.offsetLeft;
      this.lastPosY = elem.offsetTop;
      this.currentDraggedItem = elem;
    }
    const posX = ev.deltaX + this.lastPosX;
    const posY = ev.deltaY + this.lastPosY;
    elem.style.left = posX + 'px';
    elem.style.top = posY + 'px';
    if (ev.deltaY > 200) {  // shake cart
      document.getElementById('cartImg').classList.add('shaking');
      if (!this.hasVibrated) {
        window.navigator.vibrate(200);
        this.hasVibrated = true;
      }
    } else {
      document.getElementById('cartImg').classList.remove('shaking');
      this.hasVibrated = false;
    }

    if (ev.isFinal) {
      document.getElementById('cartImg').classList.remove('shaking');
      this.hasVibrated = false;
      if (ev.deltaY > 200) {
        const elemToInstert = this.currentDraggedItem.cloneNode(true) as HTMLElement;
        elemToInstert.classList.add('inside-cart');
        const itemToSelect = this.cartOptions[parseInt(this.currentDraggedItem.id, 10)];
        if (!this.isItemAddedToCart(itemToSelect)) {
          this.selectItem(itemToSelect);
          this.inCartItems.push(elemToInstert);
          this.updateCartElements();
        }
      }
      this.isDragging = false;
      this.itemDropped = false;
      elem.style.left = this.lastPosX + 'px';
      elem.style.top = this.lastPosY + 'px';
      this.currentDraggedItem = null;
    }

  }

  continue() {
    this.restApi.setSpentTime(spentTimeCategories.cart, { spentTime: this.restApi.getCurrentTime() - this.startingTime });
    this.router.navigateByUrl('dice', { skipLocationChange: true });
  }

  changeMonsterMsg() {
    this.monsterMsg = this.translateService.instant('interesting_choices');
  }

  resetMonsterMsg() {
    this.monsterMsg = this.translateService.instant('swipe_left_msg');
  }

  selectItem(option) {
    if (this.items.indexOf(option) === -1) {
      this.items.push(option);
      if (!isUndefined(this.cartQuestion)) {
        this.cartQuestionAnswers.push({
          QuestionId: this.cartQuestion.QuestionId, QuestionType: this.cartQuestion.QuestionType, QuestionAnswer: option
        });
      }
    } else {
      this.items.splice(this.items.indexOf(option), 1);
    }
    if (this.items.length === 5) {
      this.changeMonsterMsg();
      setTimeout(() => {
        this.nextQuestion();
        this.resetMonsterMsg();
      }, 1000);
    }
  }

  updateCartElements() {
    this.removeAllCartElements();
    this.inCartItems.forEach(ele => {
      document.getElementById('cartContainer').appendChild(ele);
    });
  }

  isItemAddedToCart(item) {
    if (this.items.includes(item)) {
      return true;
    }
    return false;
  }

  removeAllCartElements() {
    const inCartItems = document.getElementById('cartContainer');
    while (inCartItems.firstChild) {
      inCartItems.removeChild(inCartItems.firstChild);
    }
  }

  removeItem(option) {
    const indexOfCartItem = this.items.indexOf(option);
    if (indexOfCartItem !== -1) {
      this.items.splice(indexOfCartItem, 1);
      this.inCartItems.splice(indexOfCartItem, 1);
      this.updateCartElements();
    }
  }

  nextQuestion() {
    this.cartQuestion = this.restApi.getNextQuestion('Cart');
    if (isUndefined(this.cartQuestion)) { // last dice quesiton reached
      this.restApi.cartAnswers = this.cartQuestionAnswers;
      this.continue(); // route to next type of questions
    } else if (this.cartQuestion === -1) {
      // no dice questions available
    } else {
        this.cartQuestion.Options.forEach(opt => {
        this.cartOptions.push(opt.value);
        this.data.push(opt.preloadedImage.src);
      });
    }
  }
}
