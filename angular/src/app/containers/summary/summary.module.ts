import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { SummaryComponent } from './summary.component';

@NgModule({
    declarations: [
        SummaryComponent
    ],
    imports: [
        CommonModule,
        TranslateModule.forRoot()
    ],
    providers: [],
    entryComponents: [],
    exports: [
        SummaryComponent
    ],
})

export class SummaryModule { }
