import { Component, OnInit } from '@angular/core';
import { RestApiService } from '../../core/services/rest-api.service';
import { spentTimeCategories } from 'src/app/shared/models/app.constants';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  pressedOnSubmit = false;
  finalScore: number;
  loading = false;
  startingTime;
  constructor(public restApi: RestApiService) { }

  ngOnInit() {
    this.startingTime = this.restApi.getCurrentTime();
  }

  submit() {
    this.loading = true;
    this.restApi.setSpentTime(spentTimeCategories.summary, { spentTime: this.restApi.getCurrentTime() - this.startingTime });
    this.restApi.setSpentTime(spentTimeCategories.totalTime, this.restApi.calcualteTotalSpentTime());
    this.restApi.setSpentTime(spentTimeCategories.timeUnit, 'ms');
    this.restApi.sendQuestionsForScoring().subscribe((res) => {
      this.finalScore = res.result && typeof res.result === 'object' &&
        res.result.final_score;
      this.loading = false;
    });
    this.pressedOnSubmit = true;
  }

}
