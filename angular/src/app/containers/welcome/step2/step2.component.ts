import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from '../../../core/services/rest-api.service';
import Hammer from 'hammerjs';
import { spentTimeCategories } from 'src/app/shared/models/app.constants';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {

  startingTime;
  constructor(private router: Router, public restApi: RestApiService) { }

  ngOnInit() {
    this.startingTime = this.restApi.getCurrentTime();
  }

  start() {
    this.restApi.setSpentTime(spentTimeCategories.introduction, { spentTime: this.restApi.getCurrentTime() - this.startingTime });
    this.router.navigateByUrl('image', { skipLocationChange: true });
  }

  swipeToContinue() {
    this.start();
  }

  stopDraggingImage(event) {
    event.preventDefault();
    return false;
  }

}
