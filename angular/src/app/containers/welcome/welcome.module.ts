import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { Step1Component } from './step1/step1.component';
import { Step2Component } from './step2/step2.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
    declarations: [
        Step1Component,
        Step2Component
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        TranslateModule.forRoot()
    ],
    providers: [],
    entryComponents: [
        Step1Component,
        Step2Component],
    exports: [
        Step1Component,
        Step2Component
    ],
})

export class WelcomeModule { }
