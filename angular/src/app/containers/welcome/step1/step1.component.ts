import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { RestApiService } from '../../../core/services/rest-api.service';
import { options, spentTimeCategories } from '../../../shared/models/app.constants';
@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {
  fb: FormBuilder;
  welcomeForm: FormGroup;
  pressedOnStart = false;
  loading = true;
  startingTime;
  constructor(private router: Router, fb: FormBuilder, public restApi: RestApiService, private route: ActivatedRoute) {
    this.fb = fb;
    this.route.params.subscribe(params => {
      this.restApi.partnerName = params.partner;
      this.restApi.externalId = params.id;
    });
    this.loadQuestions();
  }

  ngOnInit() {
    this.createFormValidation();
    this.startingTime = this.restApi.getCurrentTime();
  }

  createFormValidation() {
    this.welcomeForm = this.fb.group({
      userName: new FormControl('', [
        Validators.required,
        Validators.pattern(/(\w.+\s)./i)
      ])
    });
  }

  goToStep2() {
    this.pressedOnStart = true;
    this.restApi.setSpentTime(spentTimeCategories.welcome, { spentTime: this.restApi.getCurrentTime() - this.startingTime });
    this.router.navigateByUrl('step2', { skipLocationChange: true });
  }



  loadQuestions() {
    this.restApi.getQuestions(options).subscribe((data: {}) => {
      this.loading = false;
      this.restApi.updateQuestions(data);
    }, (error) => {
      this.router.navigateByUrl('error', { skipLocationChange: true });
    });
  }
}
