import { Component, OnInit } from '@angular/core';
import {RestApiService} from "../../core/services/rest-api.service";
import {options} from "../../shared/models/app.constants";
import {Router} from "@angular/router";

@Component({
  selector: 'app-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {
  status_bar;
  constructor(private router: Router, public restApi: RestApiService) { }

  ngOnInit() {
    // this.getAnswerResults();
  }

  // getAnswerResults(){
  //     this.restApi.getAnswersResults(['image']).subscribe((data: {}) => {
  //       console.log(data);
  //       const results = data['results']
  //       results.forEach((ele: {}) =>{
  //         const totalNumberOfSelectedItems = ele[0].selected_times + ele[1].selected_times
  //         this.status_bar.barA = {percentage: ele[0].selected_times / totalNumberOfSelectedItems}
  //       })
  //   }, (error) => {
  //       console.log(error)
  //     this.router.navigateByUrl('error', { skipLocationChange: true });
  //   });
  // }
}
