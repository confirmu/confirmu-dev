import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { monsterImageUrl } from '../../shared/models/app.constants';

@Injectable({
    providedIn: 'root'
})
export class RestApiService {
    imageQuestions = [];
    cartQuestions = [];
    diceQuestions = [];
    imageCurrentQuestionIndex = 0;
    cartCurrentQuestionIndex = 0;
    diceCurrentQuestionIndex = 0;
    imageAnswers;
    cartAnswers;
    diceAnswers;
    response;
    mainURL: string;
    spentTime = {};
    partnerName: string;
    externalId: string;
    // monsterImage initated just because missing property in production mode
    monsterImage = {
        welcome_monster: { src: '' }, introduction_monster: { src: '' }, reserve_path: { src: '' },
        dice_monster: { src: '' }, image_monster: { src: '' }, path: { src: '' }, cart: { src: '' }
    };
    monsterImagesLoaded = false;
    constructor(private http: HttpClient, private router: Router) {
        this.mainURL = `${environment.baseApiURL}`;
    }

    generateHttpOptions(partner, partnerAccessToken, externalId, sendFullResponse) {
        let FullUrlPath = partner == 'epaysa'? "false" : "true";
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Partner: partner,
                'Access-Token': partnerAccessToken,
                ExternalId: externalId,
                SendFullResponse: sendFullResponse,
                App: 'dizzy',
              'FullUrlPath': FullUrlPath
            })
        };
        return httpOptions;
    }

    calcualteTotalSpentTime() {
        let totalTime = 0;
        Object.keys(this.spentTime).forEach(key => {
            totalTime += this.spentTime[key].spentTime;
        });
        return totalTime;
    }

    getCurrentTime() {
        return new Date().getTime();
    }

    // HttpClient API post() method => Fetch Questions
    getQuestions(options): Observable<any> {
        const accessToken = environment.access_tokens[this.partnerName.toUpperCase() + '_TOKEN'];
        const httpOptions = this.generateHttpOptions(this.partnerName, accessToken, this.externalId, false);
        return this.http.post<any>(`${this.mainURL}getQuestionsData/`, JSON.stringify(options), httpOptions)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    getAnswersResults(answers_types = []){
        const accessToken = environment.access_tokens['DIZZY' + '_TOKEN'];
        const httpOptions = this.generateHttpOptions('dizzy', accessToken, 111, false);
        return this.http.post<any>(`${this.mainURL}getAnswersResults/`, JSON.stringify({answers_types}), httpOptions)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    updateQuestions(question) {
        this.response = question;
        this.splitQuestionsByCategory();
        this.preloadImages();
        this.preloadDiceImages();
        this.preloadMonsterImages();
        this.preloadCartImages();
    }

    // Error handling
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        return throwError(errorMessage);
    }

    preloadImages() {
        this.imageQuestions.forEach(img => {
            img.Images.forEach(ele => {
                const image = new Image();
                image.src = ele.url;
                image.onload = () => {
                    ele.preloadedImage = image;
                };
            });

        });
    }

    preloadDiceImages() {
        this.diceQuestions.forEach(dice => {
            const image = new Image();
            image.src = '/static/dizzy_app/assets/images/dice.svg';
            image.onload = () => {
                dice.preloadedImage = image;
            };
        });
    }

    preloadMonsterImages() {
        Object.keys(monsterImageUrl).forEach(key => {
            const image = new Image();
            image.src = monsterImageUrl[key];
            image.onload = () => {
                this.monsterImage[key] = image;
                if (Object.keys(this.monsterImage).length === Object.keys(monsterImageUrl).length) {
                    this.monsterImagesLoaded = true;
                }
            };
        });
    }

    preloadCartImages() {
        this.cartQuestions.forEach(cart => {
            cart.Options.forEach(opt => {
                const image = new Image();
                image.src = opt.url;
                image.onload = () => {
                    opt.preloadedImage = image;
                };
            });

        });
    }

    splitQuestionsByCategory() {
        const questions = this.response.Questions;
        questions.forEach(question => {
            switch (question.QuestionType) {
                case 'Image':
                    this.imageQuestions.push(question);
                    break;
                case 'Select':
                    this.diceQuestions.push(question);
                    break;
                case 'Cart':
                    this.cartQuestions.push(question);
                    break;
                default:
                    break;
            }
        });
    }

    getNextQuestion(questionCategory: string): any {
        switch (questionCategory) {
            case 'Image':
                this.imageCurrentQuestionIndex++;
                return this.imageQuestions[this.imageCurrentQuestionIndex - 1];
            case 'Dice':
                this.diceCurrentQuestionIndex++;
                return this.diceQuestions[this.diceCurrentQuestionIndex - 1];
            case 'Cart':
                this.cartCurrentQuestionIndex++;
                return this.cartQuestions[this.cartCurrentQuestionIndex - 1];
            default:
                break;
        }
    }

    // TODO : return an observable
    sendQuestionsForScoring() {
        const dizzyRequest = {
            Answers: {
                image: this.imageAnswers,
                dice: this.diceAnswers,
                cart: this.cartAnswers
            },
            spentTime: this.spentTime
        };
        const accessToken = environment.access_tokens[this.partnerName.toUpperCase() + '_TOKEN'];
        const httpOptions = this.generateHttpOptions(this.partnerName, accessToken, this.externalId, false);
        return this.http.post<any>(`${this.mainURL}calculatePsychologyScore/`, JSON.stringify(dizzyRequest), httpOptions)
            .pipe(
                retry(1),
                catchError(this.handleError)
            );
    }

    setSpentTime(category: string, value: any) {
        this.spentTime[category] = value;
    }

}
