import { Component } from '@angular/core';
import { LOCALE_EN } from '../local/en';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dizzy-app';

  constructor(private translate: TranslateService) {
    this.translate.addLangs(['en']);
    this.translate.setTranslation('en', LOCALE_EN);
    this.translate.setDefaultLang('en');
    const browserLang = translate.getBrowserLang();
    this.translate.use(browserLang.match(/en/) ? browserLang : 'en');
  }
}
