import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TranslateModule } from '@ngx-translate/core';
import { WelcomeModule } from './containers/welcome/welcome.module';
import { QuestionsModule } from './containers/questions/questions.module';
import { SharedModule } from './shared/shared.module';
import { SummaryModule } from './containers/summary/summary.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StatusComponent } from './containers/status/status.component';
import {ProgressBarModule} from "angular-progress-bar"

@NgModule({
  declarations: [
    AppComponent,
    StatusComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    WelcomeModule,
    HttpClientModule,
    QuestionsModule,
    SharedModule,
    BrowserAnimationsModule,
    SummaryModule,
    ProgressBarModule,
    TranslateModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
