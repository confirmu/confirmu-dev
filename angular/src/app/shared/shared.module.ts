import { NgModule } from '@angular/core';
import { ErrorComponent } from './components/error/error.component';
import { TranslateModule } from '@ngx-translate/core';
import { ErrorRoutingComponent } from './error-routing/error-routing.component';

@NgModule({
    declarations: [
        ErrorComponent,
        ErrorRoutingComponent
    ],
    imports: [
        TranslateModule.forRoot()
    ],
    exports: [
        ErrorComponent,
        ErrorRoutingComponent
    ],
    providers: [
    ]
})

export class SharedModule { }
