export const options = {
  psychology: {
    // All: true,
    Concientiousness: {
      Image: 8,
      Decision: 1,
      Cart: 1,
      Select: 6
    },
    Extrovert: {
      Image:4,
      Select: 6
    }
  }
};

export const monsterImageUrl = {
  welcome_monster : '/static/dizzy_app/assets/images/welcome-monster.svg',
  introduction_monster : '/static/dizzy_app/assets/images/introduction-monster.svg',
  reserve_path : '/static/dizzy_app/assets/images/reverse-path.svg',
  dice_monster: '/static/dizzy_app/assets/images/dice-monster.svg',
  image_monster : '/static/dizzy_app/assets/images/image-monster.svg',
  path : '/static/dizzy_app/assets/images/path.svg',
  cart : '/static/dizzy_app/assets/images/cart.svg'
};

export const spentTimeCategories = {
  cart : 'cart',
  dice : 'dice',
  image : 'image',
  summary : 'summary',
  welcome : 'welcome',
  introduction : 'introduction',
  totalTime : 'totalTime',
  timeUnit : 'timeUnit'
};
