export const environment = {
  production: true,
  baseApiURL: '/api/',
  access_tokens : {
    BRIDGE2CAPITAL_TOKEN: 'd466608f-cc36-47af-a492-cbc406beaa65',
    CORIUNDER_TOKEN: '056f16c3-6f7f-4d1d-b604-e63a9456e1ed',
    ENTERID_TOKEN: '8c1f170e-a756-4de3-9e7e-bbbf02d47731',
    ILEND_TOKEN: '876c86f4-8234-48cf-a030-413d32f1ae3e',
    ILOAN_TOKEN: '00c43271-78b5-43be-803b-3a6671d54924',
    INCLUSIVITY_TOKEN: '8b088fc7-ee70-4f4b-a054-35cb4a2061c4',
    INDEPAY_TOKEN: 'b4b1253b-1885-47a0-9438-dc165a34a7dc',
    ITRAVELEO_TOKEN: '4c50b14c-380e-4e3b-938f-2f82906866f0',
    LEO_TOKEN: '051ddce7-ef21-42f7-ada5-3809a61c310e',
    MOBILE_APP_TOKEN: '0662aba4-555a-41ea-b6c2-d2644d7616df',
    mobile_token: '168624df-ab13-475c-a4ac-dd6a04bc8a43',
    MYFUGO_TOKEN: '41b148c5-5780-459b-bd5f-0031775355fd',
    SMARTCREDIT_TOKEN: 'd46e610a-c549-4041-9174-677cfcdbb1ed',
    TUTU_TOKEN: '18a4e70d-1ddb-414b-ad5e-fcebc4e54b01',
    UBAPESA_TOKEN: '515c6272-02b0-42c6-a4e9-fcf8ae4c6ec1',
    WHATSLOAN_TOKEN: 'efe3df88-3076-4ab2-be3d-c80ca9677394',
    DIZZY_TOKEN: '2affd204-3f3a-497c-9e38-154e264be04b',
    BACREDITOSIMPLE_TOKEN: '4e0d7115-a463-4aba-819c-7205683634b5',
    EPAYSA_TOKEN: 'e57a8278-57bc-48a1-ac05-d9f1af48fe75'
  }
};
