from statistics import mean
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer


def get_sentiment_estimation(text):
    analyser = SentimentIntensityAnalyzer()
    score = analyser.polarity_scores(text)
    return score


def get_sentiment_score(list_of_sentences):
    sentiment_estimation = [float(get_sentiment_estimation(x).get('compound')) for x in list_of_sentences]
    if sentiment_estimation:
        return (mean(sentiment_estimation) + 1) * 50
    else:
        return 0


if __name__ == '__main__':
    sentences = [
        'Dutch shooting: Letter suggests motive in Utrecht attack',
        'Three people were killed and three others seriously wounded in the attack in the central city of Utrecht.',
        'Artificial meat: UK scientists growing \'bacon\' in labs',
        'The idea was to essentially, rather than feeding a cow grass and then us eating the meat - why don\'t we, in quotation marks, \'feed our cells grass'
    ]
    for x in sentences:
        print(x, ' >>> ', get_sentiment_estimation(x))

    print('All together >>> ', get_sentiment_estimation(' '.join(sentences)))
    print(get_sentiment_score(sentences))
