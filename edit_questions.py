from utils.google_services import translate

languages = {
    'en-US': 'English (US)',
    #   'he-IL': 'Hebrew',
    #'id-ID': 'Bahasa',
    'hi-IN': 'Hindi',
    #'pt-BR': 'Portuguese (Brazil)',
    #'pt-PT': 'Portuguese (Portugal)',
    #'ar-JO': 'Arabic (Jordan)',
    #'es-ES': 'Spanish (Spain)',
    #'zh-CN': 'Chinese (S)',
    #'zh-TW': 'Chinese (T)',
    #'vi-VN': 'Tiếng Việt',
    #'th-TH': 'ไทย',
    #'fil-PH': 'Filipino ',
    #'ru-RU': 'Русский',
    #'jv-ID': 'Javanese',
    #'kn-IN': 'Kannada (India)',
    #'ta-IN': 'Tamil (India)',
    #'te-IN': 'Telugu (India)',
    #'gu-IN': 'Gujarati (India)',
    'yue-Hant-HK': 'Chinese, Cantonese (Traditional, Hong Kong)'

}

original = {

    "display_name": 'English (US)',
    "email": "Email",
    "customer_name": "Name",
    "question_1": 'Where do you like to waste your money on?',
    "question_2": 'How do you plan to save money?',
    "question_3": 'You receive mobile account which is too high, what would you about it?',
    'question_4': 'All of a sudden you realize you spent too much in the past month and you have a problem, how do you recover yourself from this situation?',
    "question_5": 'You now  wish to upgrade your mobile but this comes in favor of the expense of violating an existing obligation, will you go for it?',
    "question_6": 'How do you react to someone who bypass you on the queue to the cashier or the ATM',
    "question_7": 'Yesterday a public announcement was made  on television that from now on no more use of cash, how does that put you in?',
    "question_8": 'What is your opinion on who ever uses cash only?',
    "question_9": 'Where do you mostly invest your money?',
    "question_10": 'Do you prefer using the Internet or banking applications or simply prefer scheduling an appointment at the bank branch?',
}

translation = {}

translation['en-US'] = original.copy()

for x in languages:

    if x == 'yue-Hant-HK':
        x = 'zh-TW'
    lang = x.split('-')[0]

    if x == 'en-US':
        continue

    translation[x] = {'display_name': languages[x]}

    for y in original:

        if y == 'display_name':
            continue

        translation[x][y] = translate(original[y], to_lang=lang).get('data').get('translations')[0].get(
            'translatedText')

    print('%s done' % x)

print(translation)

final = """var translations = %s 
function translate_questions(toLanguage) { 
    var elements = document.getElementsByClassName('questions')[0].getElementsByTagName('li'); 
    for (i=0; i<elements.length; i++) { 
        var term = elements[i].getElementsByTagName('input')[0].id; 
        elements[i].getElementsByTagName('label')[0].textContent = translations[toLanguage][term]; 
    } 
} 

function set_available_langs() { 
    var available_langs = Object.keys(translations); 
    var dropdown = document.getElementById('dropdown'); 

    for (i=0; i<available_langs.length; i++) { 
        var new_option = document.createElement('option'); 
        new_option.value = available_langs[i]; 
        new_option.innerHTML = translations[available_langs[i]]['display_name']; 
        dropdown.appendChild(new_option); 
    } 
} 

""" % translation

#with open('questions.js', 'w', encoding='utf-8') as f:
#    f.write(str(final))