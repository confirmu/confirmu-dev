from forms import NameEmailForm, NineFieldsForm
from flask import render_template, request

from utils.calculations import get_scores, get_patterns, define_message
from utils.mongo_operations import save_to_confifrmu, get_partner_questions
from utils.flash_errors import flash_errors
from utils.calculations import calculate_final_score

import logging

from conf import ConstValues
from utils.google_services import translate


def chat_with_routes(request, partner):
    if request.method == 'POST':
        form = NameEmailForm(request.form)
        patterns = get_patterns(partner)
        if form.validate():
            ignore_fields = ['email', 'customer_name', 'questions_type']
            text_to_analyze = ' '.join([request.form[x] for x in request.form if x not in ignore_fields])
            scores = get_scores(text_to_analyze).get('raw_scores')
            final_score = calculate_final_score(scores, patterns)
            save_to_confifrmu(
                partner,
                dict(
                    name=request.form['customer_name'],
                    email=request.form['email'],
                    questions_type=request.form['questions_type'],
                    form=request.form,
                    text_to_analyze=text_to_analyze,
                    final_score=final_score
                )
            )

            return render_template('default_result_template.html')
        else:
            flash_errors(form)
    if request.method == 'GET':
        questions = get_partner_questions(partner)
        return render_template('ilend_template.html', form=NameEmailForm(), questions=questions)


def nine_field_stt(partner, collection, template='minimal_form.html', result_template='raw_results.html'):
    form = NineFieldsForm(request.form)
    if request.method == 'GET':
        questions = get_partner_questions(partner)
        questions_keys = [x for x in questions.get(list(questions.keys())[0]).keys() if x not in ['display_name']]
        questions_keys.sort()
        return render_template(template, questions_keys=questions_keys,  questions=questions, form=form)
    if request.method == 'POST':
        if form.validate():
            patterns = get_patterns(partner)
            raw_text = '. '.join([request.form[x] for x in request.form if x not in ['email', 'customer_name', 'dropdown']])
            text_to_analyze = translate(raw_text).get('data').get('translations')[0].get('translatedText')
            scores = get_scores(text_to_analyze).get('raw_scores')
            logging.debug(patterns)
            if len(patterns) > 0:
                final_score = calculate_final_score(scores, patterns)
            else:
                logging.error("No patterns, final score defined as 0")
                final_score = 0
            decision = define_message(final_score)
            result = dict(
                    name=request.form['customer_name'],
                    email=request.form.get('email'),
                    form=request.form,
                    raw_text=raw_text,
                    text_to_analyze=text_to_analyze,
                    scores=scores,
                    final_score=final_score,
                    decision=decision
                )
            save_to_confifrmu(collection, result)

            return render_template(result_template, result=result)
        else:
            flash_errors(form)

