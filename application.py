import base64
import json
import logging
import os
import random
from functools import wraps
from math import exp
from statistics import mean

import bcrypt
import flask
from flask import Flask, request, jsonify, render_template, flash, redirect, make_response, abort, send_file, session
from flask_cors import CORS
from flask_login import LoginManager

from chat_factory import chat_with_routes, nine_field_stt
from conf import ConstValues, Emoxicon, PartnerConfigurations as Config, PartnerQuestionsConf, GibberishContent
from forms import FiveFieldForm, AuthenticationForm, Credit24ChangeQuestionsForm, LoginForm, ItraveleoForm, TutuForm, \
    OneFieldForm
from mobile.getcustomer import get_customer_info_from_coriunder
from sentiment_analyzer import get_sentiment_score
from utils.amutils import make_shuffled_list, get_all_partner_questions, get_default_partner_questions, \
    get_scenario_questions
from utils.analyze_expenses import get_net_income, calculate_decision
from utils.calculations import get_scores, define_message, define_message_us_old, get_sms_income_ratio, \
    get_images_scores, \
    calculate_mobile_app_score, calculate_form_scores, calculate_weighted_final_score, calculate_psychology_score, \
    calculate_scenario_score
from utils.get_resuls_from_mongo import create_report
from utils.google_services import translate
from utils.mongo_operations import save_to_confifrmu, query_scores_by_search_criteria, set_questions, \
    get_partner_questions, get_user_by_name, update_questions, get_answers, get_customer_by_phone, \
    delete_customer_by_phone, get_partner_info, get_customer_by_email
from utils.email_tools import send_partner_notification, send_notification
from utils.image_randomization_algorithm import image_randomization_algorithm
from utils.gibberishclassifier import english_gibberish_classifier, hindi_gibberish_classifier, dect_an_conv

# region App Initialization
application = Flask(__name__)
application.config['SECRET_KEY'] = ConstValues.app_secret_key
cors = CORS(application, resources={r"/api/*": {"origins": "*"}})
# login = LoginManager(application)

logging.basicConfig(level=logging.getLevelName(ConstValues.confirmu_log_level) or logging.INFO,
                    format='%(asctime)s %(name)s %(levelname)-8s %(message)s',
                    datefmt='%y-%m-%d %H:%M:%S')


# endregion

# region Error Handlers
@application.errorhandler(400)
def bad_request(error):
    application.logger.error(str(error))
    return make_response(jsonify(dict(error_code=400, error_text='Bad request {}'.format(error))), 400)


@application.errorhandler(404)
def not_found(error):
    application.logger.error(str(error))
    return make_response(jsonify(dict(error_code=404, error_text='Resource not found')), 404)


@application.errorhandler(405)
def not_found(error):
    application.logger.error(str(error))
    return make_response(
        jsonify(dict(error_code=405, error_text='The method is not allowed for the requested URL')),
        405
    )


@application.errorhandler(500)
def not_found(error):
    application.logger.error(str(error))
    return make_response(jsonify(dict(error_code=500, error_text='Internal server error')), 500)


# endregion

# region Helpers
def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(error)


def calculate_final_score(scores, patterns):
    if scores is None:
        return 0
    return int(mean([exp(float(scores[x])) for x in scores if x in patterns]) / exp(1) * 100)


def auth_required(f):
    @wraps(f)
    def decorated_func(*args, **kwargs):
        if 'partner' in session:
            return f(*args, **kwargs)
        if request.headers.get('Partner'):
            partner = request.headers.get('Partner')
            token = request.headers.get('Access-Token')
            if Config.config.get(partner) is None or Config.config.get(partner).get('access_token') != token:
                return make_response(jsonify(dict(error_code=401, error_text='Unauthorized')), 401)
            session['partner'] = partner
            return f(*args, **kwargs)
        elif request.method == 'GET':
            return redirect('/partner/login')

    return decorated_func


def get_patterns(partner=None):
    if Config.config.get(partner) is not None and Config.config.get(partner).get('patterns') is not None:
        patterns = Config.config.get(partner).get('patterns')
    else:
        patterns = ConstValues.patterns
    return patterns


def raise_exception(message):
    raise Exception(message)


def get_number_of_questions_to_shuffle(r):
    free_text = r.get("FreeText") or 0
    slider = r.get("Slider") or 0
    image = r.get("Image") or 0
    drawing = r.get("Drawing") or 0
    voice = r.get("Voice") or 0
    select = r.get("Select") or 0
    return free_text, slider, image, drawing, voice, select


# endregion

# region Routes
@application.route("/", methods=['GET'])
def home():
    return render_template('openapidesc.html', filename='confirmu')


@application.route("/dizzy_app", methods=['GET'])
@application.route("/dizzy_app/", methods=['GET'])
@application.route("/dizzy_app/<partner>", methods=['GET'])
@application.route("/dizzy_app/<partner>/", methods=['GET'])
@application.route("/dizzy_app/<partner>/<external_id>", methods=['GET'])
@application.route("/dizzy_app/<partner>/<external_id>/", methods=['GET'])
def dizzy_app(partner=None, external_id=None):
    return render_template('dizzy_app.html', partner=partner)


@application.route("/anna", methods=['GET', 'POST'])
def anna():
    if request.method == 'POST':
        result = 'Your answres:<br>'
        for x in request.form:
            result += '<b>{}</b>: {}<br>'.format(x, request.form[x])
        return result
    if request.method == 'GET':
        return render_template('dynamic_form.html')


@application.route("/mobbisurance", methods=['GET', 'POST'])
def mobbisurance():
    partner = 'mobbisurance'
    if request.method == 'POST':
        try:
            r = request.json
            questions = r['Questionaire'].get('Questions')
            email = r['Customer'].get('EmailAddress')
            text_answers = [
                x['FreeTextQuestion'].get('FreeTextAnswer') for x in questions if
                x['General'].get('QuestionType') == 'FreeText'
            ]
            joined_text_answers = '.'.join(text_answers)
            images_score = get_images_scores(questions, 'mobbisurance')
            ta = get_scores(joined_text_answers, False)
            ta_score = min(calculate_final_score(ta.get('raw_scores'), ConstValues.patterns) * 1.25, 100)
            ta_error = ta.get('error_message')
            sent_score = get_sentiment_score(text_answers)
            final_score = calculate_weighted_final_score(partner, ta_score, sent_score, images_score)
            result = {
                "email": email,
                "final_score": final_score,
                "ta_score": ta_score,
                "ta_error": ta_error,
                "sent_score": sent_score,
                "images_score": images_score,
                "request": r
            }
            save_to_confifrmu("mobbisurance", result)

            logging.debug("Email: {} >> Psycholinguistic: {}; Sentiment : {};  Images: {}; Final: {}".format(
                email, ta_score, sent_score, images_score, final_score
            ))
            return "Your score is <b>{:d}</b><br>We are going to contact you soon<br>Have a nice day!".format(
                int(final_score)
            )

        except Exception as e:
            logging.error('ERROR: {}'.format(e))
            return "Something went wrong, sorry :("
    if request.method == 'GET':
        return render_template('mobbisurance.html')


@application.route("/concordia", methods=['GET', 'POST'])
def concordia():
    partner = 'concordia'
    if request.method == 'POST':
        try:
            r = request.json
            questions = r['Questionaire'].get('Questions')
            email = r['Customer'].get('EmailAddress')
            text_answers = [
                x['FreeTextQuestion'].get('FreeTextAnswer') for x in questions if
                x['General'].get('QuestionType') == 'FreeText'
            ]
            joined_text_answers = '.'.join(text_answers)
            images_score = get_images_scores(questions, 'concordia')
            ta = get_scores(joined_text_answers, False)
            ta_score = min(calculate_final_score(ta.get('raw_scores'), ConstValues.patterns) * 1.25, 100)
            ta_error = ta.get('error_message')
            sent_score = get_sentiment_score(text_answers)
            final_score = calculate_weighted_final_score(partner, ta_score, sent_score, images_score)
            result = {
                "email": email,
                "final_score": final_score,
                "ta_score": ta_score,
                "ta_error": ta_error,
                "sent_score": sent_score,
                "images_score": images_score,
                "request": r
            }
            save_to_confifrmu("concordia", result)

            logging.debug("Email: {} >> Psycholinguistic: {}; Sentiment : {};  Images: {}; Final: {}".format(
                email, ta_score, sent_score, images_score, final_score
            ))
            return "We are going to contact you soon<br>Have a nice day!"

        except Exception as e:
            logging.error('ERROR: {}'.format(e))
            return "Something went wrong, sorry :("
    if request.method == 'GET':
        partner_info = get_partner_info('concordia')
        questions = make_shuffled_list('concordia', free_text=5, slider=0, image=3, drawing=0, voice=0, select=0)
        return render_template('shuffle.html', partner_info=partner_info, questions=questions)


@application.route("/iloan", methods=['GET', 'POST'])
def iloan():
    partner = 'iloan'
    if request.method == 'POST':
        try:
            r = request.json
            questions = r['Questionaire'].get('Questions')
            email = r['Customer'].get('EmailAddress')
            text_answers = [
                x['FreeTextQuestion'].get('FreeTextAnswer') for x in questions if
                x['General'].get('QuestionType') == 'FreeText'
            ]
            joined_text_answers = '.'.join(text_answers)
            images_score = get_images_scores(questions, 'iloan')
            ta = get_scores(joined_text_answers)
            ta_score = min(calculate_final_score(ta.get('raw_scores'), ConstValues.patterns) * 1.25, 100)
            ta_error = ta.get('error_message')
            sent_score = get_sentiment_score(text_answers)
            final_score = calculate_weighted_final_score(partner, ta_score, sent_score, images_score)
            result = {
                "email": email,
                "final_score": final_score,
                "ta_score": ta_score,
                "ta_error": ta_error,
                "sent_score": sent_score,
                "images_score": images_score,
                "request": r
            }
            save_to_confifrmu("iloan", result)

            logging.debug("Email: {} >> Psycholinguistic: {}; Sentiment : {};  Images: {}; Final: {}".format(
                email, ta_score, sent_score, images_score, final_score
            ))
            return "We are going to contact you soon<br>Have a nice day!"

        except Exception as e:
            logging.error('ERROR: {}'.format(e))
            return "Something went wrong, sorry :("
    if request.method == 'GET':
        partner_info = get_partner_info('iloan')
        questions = make_shuffled_list('iloan', free_text=5, slider=2, image=3, drawing=1, voice=2, select=2)
        return render_template('shuffle.html', partner_info=partner_info, questions=questions)


@application.route("/test", methods=['GET', 'POST'])
def test():
    partner = 'test'
    if request.method == 'POST':
        try:
            r = request.json
            questions = r['Questionaire'].get('Questions')
            email = r['Customer'].get('EmailAddress')
            text_answers = [
                x['FreeTextQuestion'].get('FreeTextAnswer') for x   in questions if
                x['General'].get('QuestionType') == 'FreeText'
            ]
            joined_text_answers = '.'.join(text_answers)
            images_score = get_images_scores(questions, "mobileapp")
            ta = get_scores(joined_text_answers)
            ta_score = min(calculate_final_score(ta.get('raw_scores'), ConstValues.patterns) * 1.25, 100)
            ta_error = ta.get('error_message')
            sent_score = get_sentiment_score(text_answers)
            final_score = calculate_weighted_final_score(partner, ta_score, sent_score, images_score)
            result = {
                "email": email,
                "final_score": final_score,
                "ta_score": ta_score,
                "ta_error": ta_error,
                "sent_score": sent_score,
                "images_score": images_score,
                "request": r
            }
            save_to_confifrmu("test", result)

            logging.debug("Email: {} >> Psycholinguistic: {}; Sentiment : {};  Images: {}; Final: {}".format(
                email, ta_score, sent_score, images_score, final_score
            ))
            return "We are going to contact you soon<br>Have a nice day!"

        except Exception as e:
            logging.error('ERROR: {}'.format(e))
            return "Something went wrong, sorry :("
    if request.method == 'GET':
        partner_info = get_partner_info('mobileapp')
        questions = make_shuffled_list('mobileapp', free_text=2, image=2)
        return render_template('shuffle.html', partner_info=partner_info, questions=questions)


@application.route("/bridge2capital", methods=['GET', 'POST'])
def bridge2capital():
    partner = 'bridge2capital'
    if request.method == 'POST':
        try:
            r = request.json
            questions = r['Questionaire'].get('Questions')
            email = r['Customer'].get('EmailAddress')
            text_answers = [
                x['FreeTextQuestion'].get('FreeTextAnswer') for x in questions if
                x['General'].get('QuestionType') == 'FreeText'
            ]
            text_validate = english_gibberish_classifier(text_answers)
            if text_validate:
                return GibberishContent.english_gibberish_reason
            joined_text_answers = '.'.join(text_answers)
            images_score = get_images_scores(questions, 'bridge2capital')
            ta = get_scores(joined_text_answers)
            ta_score = min(calculate_final_score(ta.get('raw_scores'), ConstValues.patterns) * 1.25, 100)
            ta_error = ta.get('error_message')
            sent_score = get_sentiment_score(text_answers)
            final_score = calculate_weighted_final_score(partner, ta_score, sent_score, images_score)
            result = {
                "email": email,
                "final_score": final_score,
                "ta_score": ta_score,
                "ta_error": ta_error,
                "sent_score": sent_score,
                "images_score": images_score,
                "request": r
            }
            save_to_confifrmu("bridge2capital", result)

            try:
                send_partner_notification(
                    partner,
                    "Scoring report from confirmU to customer {}".format(email),
                    "Customer email: {email}<br>Final score: {final_score}".format(
                        final_score=int(final_score),
                        email=email
                    )
                )
            except Exception as e:
                logging.error('Error while sending an email: {}'.format(e))

            logging.debug("Email: {} >> Psycholinguistic: {}; Sentiment : {};  Images: {}; Final: {}".format(
                email, ta_score, sent_score, images_score, final_score
            ))
            return "We are going to contact you soon<br>Have a nice day!"

        except Exception as e:
            logging.error('ERROR: {}'.format(e))
            return "Something went wrong, sorry :("
    if request.method == 'GET':
        partner_info = get_partner_info('bridge2capital')
        questions_conf = PartnerQuestionsConf.bridge2capital_questions
        questions = make_shuffled_list('bridge2capital', free_text=questions_conf.get("free_text"),
                                       slider=questions_conf.get("slider"),
                                       image=questions_conf.get("image"), drawing=questions_conf.get("drawing"),
                                       voice=questions_conf.get("voice"), select=questions_conf.get("select"))
        return render_template('shuffle.html', partner_info=partner_info, questions=questions)


@application.route("/bacreditosimple", methods=['POST'])
@application.route("/bacreditosimple/<external_id>", methods=['GET'])
def bacreditosimple(external_id=None):
    partner = 'bacreditosimple'
    if request.method == 'POST':
        try:
            r = request.json
            external_id = request.headers['ExternalId']
            questions = r['Questionaire'].get('Questions')
            email = r['Customer'].get('EmailAddress')
            text_answers_spanish = [
                x['FreeTextQuestion'].get('FreeTextAnswer') for x in questions if
                x['General'].get('QuestionType') == 'FreeText'
            ]
            try:
                translated = translate(text_answers_spanish)
                logging.debug(translated)
                text_answers = [x.get('translatedText') for x in translated['data']['translations']]
            except Exception as e:
                logging.error('Error while translating to english: {}'.format(e))
                text_answers = text_answers_spanish

            joined_text_answers = '.'.join(text_answers)
            images_score = get_images_scores(questions, 'bacreditosimple')
            ta = get_scores(joined_text_answers)
            ta_score = min(calculate_final_score(ta.get('raw_scores'), ConstValues.patterns) * 1.25, 100)
            ta_error = ta.get('error_message')
            sent_score = get_sentiment_score(text_answers)
            final_score = calculate_weighted_final_score(partner, ta_score, sent_score, images_score)
            result = {
                "email": email,
                "external_id": external_id,
                "final_score": final_score,
                "ta_score": ta_score,
                "ta_error": ta_error,
                "sent_score": sent_score,
                "images_score": images_score,
                "request": r,
                "text_answers": text_answers
            }
            save_to_confifrmu("bacreditosimple", result)
            try:
                send_partner_notification(
                    partner,
                    "Scoring report from confirmU to customer = {}".format(external_id),
                    "ExternalID: {ext_id}<br>Final score: {final_score}<br>Customer email: {email}".format(
                        ext_id=external_id,
                        final_score=int(final_score),
                        email=email
                    )
                )
            except Exception as e:
                logging.error('Error while sending an email: {}'.format(e))

            logging.debug("Email: {} >> Psycholinguistic: {}; Sentiment : {};  Images: {}; Final: {}".format(
                email, ta_score, sent_score, images_score, final_score
            ))
            return "We are going to contact you soon<br>Have a nice day!"

        except Exception as e:
            logging.error('ERROR: {}'.format(e))
            return "Something went wrong, sorry :("
    if request.method == 'GET':
        partner_info = get_partner_info('bacreditosimple')
        questions = make_shuffled_list('bacreditosimple', free_text=5, image=3)
        return render_template('shuffle.html', partner_info=partner_info, questions=questions, external_id=external_id)


@application.route("/epaysa", methods=['POST'])
@application.route("/epaysa/<external_id>", methods=['GET'])
def epaysa(external_id=None):
    partner = 'epaysa'
    if request.method == 'POST':
        try:
            r = request.json
            external_id = request.headers['ExternalId']
            questions = r['Questionaire'].get('Questions')
            email = r['Customer'].get('EmailAddress')
            text_answers_spanish = [
                x['FreeTextQuestion'].get('FreeTextAnswer') for x in questions if
                x['General'].get('QuestionType') == 'FreeText'
            ]
            try:
                translated = translate(text_answers_spanish)
                logging.debug(translated)
                text_answers = [x.get('translatedText') for x in translated['data']['translations']]
            except Exception as e:
                logging.error('Error while translating to english: {}'.format(e))
                text_answers = text_answers_spanish

            joined_text_answers = '.'.join(text_answers)
            images_score = get_images_scores(questions, partner)
            ta = get_scores(joined_text_answers)
            ta_score = min(calculate_final_score(ta.get('raw_scores'), ConstValues.patterns) * 1.25, 100)
            ta_error = ta.get('error_message')
            sent_score = get_sentiment_score(text_answers)
            final_score = calculate_weighted_final_score(partner, ta_score, sent_score, images_score)
            result = {
                "email": email,
                "external_id": external_id,
                "final_score": final_score,
                "ta_score": ta_score,
                "ta_error": ta_error,
                "sent_score": sent_score,
                "images_score": images_score,
                "request": r,
                "text_answers": text_answers
            }
            save_to_confifrmu(partner, result)
            try:
                send_partner_notification(
                    partner,
                    "Scoring report from confirmU to customer = {}".format(external_id),
                    "ExternalID: {ext_id}<br>Final score: {final_score}<br>Customer email: {email}".format(
                        ext_id=external_id,
                        final_score=int(final_score),
                        email=email
                    )
                )
            except Exception as e:
                logging.error('Error while sending an    email: {}'.format(e))

            logging.debug("Email: {} >> Psycholinguistic: {}; Sentiment : {};  Images: {}; Final: {}".format(
                email, ta_score, sent_score, images_score, final_score
            ))
            return "We are going to contact you soon<br>Have a nice day!"

        except Exception as e:
            logging.error('ERROR: {}'.format(e))
            return "Something went wrong, sorry :("
    if request.method == 'GET':
        partner_info = get_partner_info(partner)
        questions = make_shuffled_list(partner, free_text=5, image=3)
        return render_template('shuffle.html', partner_info=partner_info, questions=questions, external_id=external_id)


@application.route("/b2cap_hindi", methods=['GET', 'POST'])
def b2cap_hindi():
    partner = 'b2cap_hindi'
    if request.method == 'POST':
        try:
            r = request.json
            questions = r['Questionaire'].get('Questions')
            email = r['Customer'].get('EmailAddress')
            text_answers_hindi = [
                x['FreeTextQuestion'].get('FreeTextAnswer') for x in questions if
                x['General'].get('QuestionType') == 'FreeText'
            ]
            try:
                text_answers = dect_an_conv(text_answers_hindi)
                if text_answers:
                    pass
                else:
                    text_answers = text_answers_hindi
                logging.debug(text_answers)
            except Exception as e:
                logging.error('Error while translating to english: {}'.format(e))
                text_answers = text_answers_hindi

            text_validate = english_gibberish_classifier(text_answers)
            if text_validate:
                return GibberishContent.hindi_gibberish_reason
            joined_text_answers = '.'.join(text_answers)
            images_score = get_images_scores(questions, 'b2cap_hindi')
            ta = get_scores(joined_text_answers)
            ta_score = min(calculate_final_score(ta.get('raw_scores'), ConstValues.patterns) * 1.25, 100)
            ta_error = ta.get('error_message')
            sent_score = get_sentiment_score(text_answers)
            final_score = calculate_weighted_final_score(partner, ta_score, sent_score, images_score)
            result = {
                "email": email,
                "final_score": final_score,
                "ta_score": ta_score,
                "ta_error": ta_error,
                "sent_score": sent_score,
                "images_score": images_score,
                'language': 'hindi',
                "request": r
            }
            save_to_confifrmu("b2cap_hindi", result)

            try:
                send_partner_notification(
                    partner,
                    "Scoring report from confirmU to customer {}".format(email),
                    "Customer email: {email}<br>Final score: {final_score}".format(
                        final_score=int(final_score),
                        email=email
                    )
                )
            except Exception as e:
                logging.error('Error while sending an email: {}'.format(e))

            logging.debug("Email: {} >> Psycholinguistic: {}; Sentiment : {};  Images: {}; Final: {}".format(
                email, ta_score, sent_score, images_score, final_score
            ))
            return "We are going to contact you soon<br>Have a nice day!"

        except Exception as e:
            logging.error('ERROR: {}'.format(e))
            return "Something went wrong, sorry :("
    if request.method == 'GET':
        partner_info = get_partner_info('b2cap_hindi')
        questions = make_shuffled_list('b2cap_hindi', free_text=5, slider=2, image=3, drawing=1, voice=2, select=2)
        return render_template('shuffle.html', partner_info=partner_info, questions=questions)


@application.route("/dizzy", methods=['POST'])
def dizzy():
    partner = 'dizzy'
    if request.method == 'POST':
        try:
            form_step = request.headers.get(
                "step") or 0  # indicates the current step of the forms question to specify what operation to do ( 0 for the first time )
            if form_step == '0':  # images step
                if request.data:
                    r = request.json
                    psychology = r.get("psychology")

                    result = make_shuffled_list(partner, 0, 0, 0, 0, 0, 0,
                                                excluded_image_attrs=['score'], psychology=psychology)
                    session['shuffled_questions'] = result
                    images_questions_only = list(filter(lambda q: q.get("QuestionType") == "Image", result))
                    # TODO : remove answers from questions
                    # IMPORTANT : Remove this line
                    return jsonify(result)
                else:
                    result = get_all_partner_questions(partner, excluded_image_attrs=['score'])
            elif form_step == '1':  # filter dice questions based on answers
                if request.data:  # Questions Answers
                    r = request.json
                    question_answers = r['Answers']
                    shuffled_questions = session['shuffled_questions']
                    questions_data = []
                    for ans in question_answers:
                        question_id = ans.get('QuestionId')
                        original_question = \
                            list(filter(lambda q: q.get("QuestionId") == question_id, shuffled_questions))[0]
                        final_answer = ans.get('QuestionAnswer')
                        psychology = original_question.get('QuestionPsychology')
                        expected_answers = [
                            opt['image_id'] for opt in original_question['Images'] if
                            opt['isCorrect'] == 1
                        ]
                        questions_data.append({'QuestionAnswer': final_answer, 'ExpectedAnswers': expected_answers,
                                               'QuestionPsychology': psychology})
                    pics_ptc_results = image_randomization_algorithm(questions_data)
                    session['pics_ptc_results'] = pics_ptc_results
                    dice_questions_only = list(
                        filter(lambda q: q.get("QuestionType") == "Select", session['shuffled_questions']))
                    dice_questions_to_ask = []
                    results = {}
                    for psych, ptc in pics_ptc_results.items():  # psych: stands for psychology
                        results[psych] = "low"
                        if ptc == 1:
                            dice_questions_to_ask.append(
                                list(filter(lambda q: q.get("QuestionPsychology") == psych, dice_questions_only)))
                    session['results'] = results
                    return jsonify(dice_questions_to_ask)
                    # IMPORTANT : remove this line
                    # TODO : remove answers from questions
            elif form_step == '2':
                if request.data:  # Questions Answers
                    r = request.json
                    question_answers = r['Answers']
                    shuffled_questions = session['shuffled_questions']
                    results = session['results']
                    pics_ptc_results = session['pics_ptc_results']
                    for ans in question_answers:
                        question_id = ans.get('QuestionId')
                        original_question = \
                            list(filter(lambda q: q.get("QuestionId") == question_id, shuffled_questions))[0]
                        final_answer = ans.get('QuestionAnswer')
                        psychology = original_question.get('QuestionPsychology')
                        expected_answers = [
                            opt['value'] for opt in original_question['Options'] if
                            opt['isCorrect'] == 1
                        ]
                        if pics_ptc_results[psychology] == 1:
                            results[psychology] = "high" if final_answer in expected_answers else "low"
                return jsonify(results)
        except Exception as e:
            logging.error("An error occurred while handling dizzy questions" + str(e))
    else:
        return r'POST method needed'


@application.route("/myfugo", methods=['GET', 'POST'])
def myfugo():
    partner = 'myfugo'
    if request.method == 'POST':
        try:
            email = request.headers.get('ExternalId')
            return calculate_form_scores(partner, request, email)
        except Exception as e:
            logging.error('ERROR: {}'.format(e))
            return "Something went wrong, sorry :("
    if request.method == 'GET':
        partner_info = get_partner_info(partner)
        external_id = request.args.get('email')
        if external_id:
            partner_info.pop('name_question')
            partner_info.pop('email_question')
        questions = make_shuffled_list(partner, free_text=5, slider=2, image=3, drawing=1, voice=2, select=2)
        return render_template('shuffle.html', partner_info=partner_info, questions=questions, external_id=external_id)


@application.route("/ubapesa", methods=['GET', 'POST'])
def ubapesa():
    partner = 'ubapesa'
    if request.method == 'POST':
        try:
            r = request.json
            return calculate_form_scores(partner, r)
        except Exception as e:
            logging.error('ERROR: {}'.format(e))
            return "Something went wrong, sorry :("
    if request.method == 'GET':
        partner_info = get_partner_info(partner)
        questions = make_shuffled_list(partner, free_text=5, slider=2, image=3, drawing=1, voice=2, select=2)
        return render_template('shuffle.html', partner_info=partner_info, questions=questions)


@application.route("/itraveleo", methods=['GET', 'POST'])
@application.route("/itraveleo/", methods=['GET', 'POST'])
@application.route("/itraveleo/<personal_id>", methods=['GET', 'POST'])
def itraveleo(personal_id=None):
    form = ItraveleoForm(request.form)
    if request.method == 'POST':
        patterns = get_patterns('itraveleo')
        if form.validate():
            text_to_analyze = ' '.join([request.form[x] for x in request.form if x not in ['email', 'customer_name']])
            scores = get_scores(text_to_analyze).get('raw_scores')
            final_score = calculate_final_score(scores, patterns)
            save_to_confifrmu(
                'itraveleo',
                dict(
                    name=request.form['customer_name'],
                    email=request.form['email'],
                    personal_id=personal_id,
                    form=request.form,
                    text_to_analyze=text_to_analyze,
                    final_score=final_score
                )
            )

            return render_template('default_result_template.html')
        else:
            flash_errors(form)
    if request.method == 'GET':
        return render_template('default_form_template.html', form=form)


@application.route("/tutu", methods=['GET', 'POST'])
@application.route("/tutu/", methods=['GET', 'POST'])
@application.route("/tutu/<personal_id>", methods=['GET', 'POST'])
def tutu(personal_id=None):
    form = TutuForm(request.form)
    if request.method == 'POST':
        patterns = get_patterns('tutu')
        if form.validate():
            text_to_analyze = ' '.join([request.form[x] for x in request.form if x not in ['email', 'customer_name']])
            scores = get_scores(text_to_analyze).get('raw_scores')
            final_score = calculate_final_score(scores, patterns)

            save_to_confifrmu(
                'tutu',
                dict(
                    name=request.form['customer_name'],
                    email=request.form['email'],
                    personal_id=personal_id,
                    form=request.form,
                    text_to_analyze=text_to_analyze,
                    final_score=final_score
                )
            )

            return render_template('default_result_template.html')
        else:
            flash_errors(form)
    if request.method == 'GET':
        return render_template('default_form_template.html', form=form)


@application.route("/ilend", methods=['GET', 'POST'])
@application.route("/ilend/", methods=['GET', 'POST'])
def ilend():
    return chat_with_routes(request, 'ilend')


@application.route("/minterest", methods=['GET', 'POST'])
@application.route("/minterest/", methods=['GET', 'POST'])
def minterest():
    return chat_with_routes(request, 'minterest')


@application.route("/api/token", methods=['GET', 'POST'])
@application.route("/api/token/", methods=['GET', 'POST'])
@auth_required
def process_token():
    if request.method == 'POST':
        try:
            try:
                req_data = json.loads(request.data)
            except Exception as e:
                req_data = json.loads(request.data.decode('ascii', 'ignore').encode('utf-8'))
        except Exception as e:
            return 'unable to parse request: %s' % str(e)
        result = dict()
        result['request_data'] = dict(
            user=req_data.get('user'),
            email=req_data.get('email'),
            password=str(req_data.get('password'))
        )
        # get data from Coriunder profile
        try:
            result['customer'] = get_customer_info_from_coriunder(
                req_data.get('user'),
                req_data.get('email'),
                req_data.get('password')
            )
        except Exception as e:
            result['customer'] = {'error_code': '-1', 'error_message': str(e)}
        # get scores from ML algorithm
        try:
            result['scores'] = get_scores(result['customer'].get('profile').get('AboutMe'))
            result['decision'] = dict(
                message='Test message, for now score is just sum of all scores',
                score=result['scores'].get('final_score')
            )
        except Exception as e:
            result['scores'] = {'error_code': '-1', 'error_message': str(e)}
        # save to mongo
        record = str(save_to_confifrmu('loanscore', result))
        result['record'] = str(record)
        return jsonify(result)
    else:
        return r"use POST method to get data"


@application.route("/api/getdecision", methods=['GET', 'POST'])
@application.route("/api/getdecision/", methods=['GET', 'POST'])
@application.route("/api/getdecision/<settings>", methods=['GET', 'POST'])
@auth_required
def get_decision(settings='default'):
    if request.method == 'POST':
        result = dict(error_code=0, error_text='')
        try:
            try:
                req_data = json.loads(request.data)
            except Exception as e:
                req_data = json.loads(request.data.decode('ascii', 'ignore').encode('utf-8'))
        except Exception as e:
            result['error_code'] = -1
            result['error_text'] = 'unable to parse request: %s' % str(e)
            return jsonify(result)
        result = dict()
        result['request_data'] = req_data

        # get scores from ML algorithm
        try:
            result['scores'] = get_scores(req_data.get('message'))
        except Exception as e:
            result['scores'] = {'error_code': '-1', 'error_message': str(e)}

        try:
            result['net_income'] = get_net_income(req_data)
        except Exception as e:
            result['net_income'] = {'error_code': '-1', 'error_message': str(e)}

        try:
            result['decision'] = calculate_decision(
                result['scores'].get('final_score'),
                req_data.get('requstedLoanAmt'),
                req_data.get('durationInMonths'),
                result['net_income'].get('value')
            )
        except Exception as e:
            result['decision'] = {'error_code': '-1', 'error_message': str(e)}

        save_to_confifrmu('get_decision', result)
        return jsonify(result['decision'])
    else:
        return r"use POST method to get data"


@application.route("/api/getscores", methods=['GET', 'POST'])
@application.route("/api/getscores/", methods=['GET', 'POST'])
@auth_required
def get_scores_route():
    if request.method == 'POST':
        response_code = 200
        try:
            try:
                req_data = json.loads(request.data)
            except Exception as e:
                req_data = json.loads(request.data.decode('ascii', 'ignore').encode('utf-8'))
        except Exception as e:
            return make_response(jsonify(dict(error_code=400, error_text='unable to parse request: %s' % str(e))), 400)

        partner = request.headers.get('Partner')

        if partner == 'itraveleo':
            result = dict()
            patterns = get_patterns('partner')

            customer_key = req_data.get('customer').get('CustomerNumber')
            result['partner'] = partner
            result['customer_key'] = str(customer_key)
            result['request_data'] = req_data

            # get scores from ML algorithm
            try:
                text_to_analyze = req_data.get('customer').get('AboutMe')
                scores_result = get_scores(text_to_analyze, False)

                if scores_result.get('error_code') == 0:
                    scores = scores_result.get('raw_scores')
                    result['scores'] = scores
                    response_code = 201
                    final_score = 0

                    if len(patterns) > 0:
                        ta_score = calculate_final_score(scores, patterns)
                    else:
                        ta_score = calculate_final_score(scores, ConstValues.patterns)
                    if partner in ['ubapesa', 'inclusivity']:
                        sms_data = req_data.get('customer').get('sms')
                        final_score = ta_score * 8
                        if sms_data is not None:
                            logging.debug('Information about sms received')
                            sms_balance_ratio = get_sms_income_ratio(sms_data)
                            logging.debug('sms balance ratio: {}'.format(sms_balance_ratio))
                            if sms_balance_ratio < 1.:
                                final_score = final_score - 50
                            elif sms_balance_ratio > 1.1:
                                final_score = final_score + 50
                        msg = define_message_us_old(final_score)
                    else:
                        final_score = ta_score
                        msg = define_message(final_score)
                    result['response'] = {
                        "Code": 0,
                        "IsSuccess": True,
                        "Key": customer_key,  # customer number
                        "Message": msg.get('message'),
                        "Number": final_score
                    }
                    if msg.get('decision'):
                        result['response']['Decision'] = msg.get('decision')

                else:
                    result['response'] = {
                        "Code": scores_result.get('error_code'),
                        "IsSuccess": False,
                        "Key": customer_key,  # customer number
                        "Message": scores_result.get('error_message'),
                        "Number": -1
                    }
                    response_code = 400
            except Exception as e:
                result['response'] = {
                    "Code": -1,
                    "IsSuccess": False,
                    "Key": customer_key,  # customer number
                    "Message": "Score calculation failed: %s" % str(e),
                    "Number": -1
                }
                response_code = 400
        else:
            result, response_code = calculate_mobile_app_score(partner, req_data)

        save_to_confifrmu('get_scores', result)
        return jsonify(result['response']), response_code
    else:
        return r"use POST method to get data"


@application.route("/api/getscores/<customer_key>", methods=['GET'])
@auth_required
def get_scores_by_key(customer_key):
    try:
        result, code = query_scores_by_search_criteria(customer_key, request.headers.get('Partner'))
        return jsonify(result), code
    except Exception as e:
        return jsonify({
            "Code": -1,
            "IsSuccess": False,
            "Key": str(customer_key),
            "Message": "Couldn't find score: %s" % str(e),
            "Number": -1
        }), 404


@application.route("/api/customers", methods=['POST'])
@application.route("/api/customers/<phone>", methods=['GET'])
@application.route("/api/customers/<phone>", methods=['DELETE'])
@auth_required
def customers(phone=None):
    partner = request.headers.get('Partner')
    if request.method == 'POST':
        try:
            r = json.loads(request.data)
            if r.get('phone') and r.get('password'):
                if get_customer_by_phone(r['phone'], partner):
                    return make_response(jsonify(dict(error_text='User exists already')), 400)
                r['partner'] = partner
                save_to_confifrmu('customers', r)
                return make_response(jsonify(r), 201)
        except Exception as e:
            logging.error('Unable to create user.\nRequest:\n {}\nError: {}'.format(
                request.data,
                e
            ))
            return abort(400)
    elif request.method == 'GET':
        try:
            customer_info = get_customer_by_phone(phone, partner)
            if customer_info:
                return make_response(jsonify(customer_info), 200)
            else:
                return make_response(jsonify(dict(error_text='Unable to find user')), 400)
        except Exception as e:
            logging.error('Unable to find user.\nRequest:\n {}\nError: {}'.format(
                request.data,
                e
            ))
            return abort(400)
    elif request.method == 'DELETE':
        try:
            num_of_deleted = delete_customer_by_phone(phone, partner)
            if num_of_deleted:
                return make_response(jsonify({"Number of deleted users": num_of_deleted}), 200)
            else:
                return make_response(jsonify(dict(error_text='Unable to delete user')), 400)
        except Exception as e:
            logging.error('Unable to delete user.\nRequest:\n {}\nError: {}'.format(
                request.data,
                e
            ))
            return abort(400)

# @application.route("/api/customersbyemail/", methods=['GET', 'POST'])
# @auth_required
# def customers_by_email(email=None):
#     partner = request.headers.get('Partner')
#     email=request.headers.get('email')
#     query_limit=request.headers.get('limit')
#
#     if request.method == 'POST':
#         try:
#             customer_info = get_customer_by_email(partner, email, int(query_limit))
#             if customer_info and email:
#                 return make_response(jsonify(customer_info), 200)
#             else:
#                 return make_response(jsonify(dict(error_text='Unable to find user')), 400)
#         except Exception as e:
#             logging.error('Unable to find user.\nRequest:\n {}\nError: {}'.format(
#                 request.data,
#                 e
#             ))
#             return abort(400)
#     elif request.method == 'DELETE':
#         try:
#             num_of_deleted = delete_customer_by_phone(phone, partner)
#             if num_of_deleted:
#                 return make_response(jsonify({"Number of deleted users": num_of_deleted}), 200)
#             else:
#                 return make_response(jsonify(dict(error_text='Unable to delete user')), 400)
#         except Exception as e:
#             logging.error('Unable to delete user.\nRequest:\n {}\nError: {}'.format(
#                 request.data,
#                 e
#             ))
#             return abort(400)

@application.route("/api/getquestions/", methods=['POST'])
@application.route("/api/getquestions/<num_of_questions>", methods=['POST'])
@auth_required
def get_questions_route(num_of_questions=0):
    if request.method == 'POST':
        response_code = 200
        partner = request.headers.get('Partner')
        if partner == 'mobileapp':
            try:
                language = request.headers.get('Language')
                return jsonify({
                    "Code": 0,
                    "IsSuccess": True,
                    "Questions": get_partner_questions('mobileapp', language)
                })
            except Exception as e:
                logging.error('Can not get questions for language {}. Return English'.format(language))

            return jsonify({
                "Code": 0,
                "IsSuccess": True,
                "Questions": Config.config.get(partner).get('questions')})

        try:
            num_of_questions = int(num_of_questions)
        except Exception as e:
            return make_response(jsonify(dict(error_code=400, error_text='wrong number of questions: %s' % str(e))),
                                 400)
        result = dict()

        if Config.config.get(partner).get('questions') is not None:
            q_list = list(Config.config.get(partner).get('questions'))
            q_list = random.sample(q_list, int(num_of_questions))
            questions = {x: Config.config.get(partner).get('questions').get(x) for x in q_list}
            if questions is not None:
                return jsonify({
                    "Code": 0,
                    "IsSuccess": True,
                    "Questions": questions})
            else:
                return jsonify(dict(error_code=1, error_text='No questions found for %s' % partner)), 400
        else:
            return jsonify(dict(error_code=2, error_text='No settings for: %s' % partner)), 400


@application.route("/api/getQuestionsData", methods=['POST'])
@application.route("/api/getQuestionsData/", methods=['POST'])
@application.route("/api/schuffle/", methods=['POST'])
@application.route("/api/schuffle", methods=['POST'])
@auth_required
def get_questions_data():
    partners_scenario_questions = ['hedgini', 'creditAI', 'LM_financials', 'epaysa', 'VOua8PmXQC']
    response = {
        "Code": 0,
        "IsSuccess": True,
        "Questions": []
    }
    if request.method == 'POST':
        has_pratner = request.headers.has_key('Partner')
        if has_pratner is False:
            return jsonify(dict(error_code=-1, error_text='Partner Not Parsed')), 500
        partner = request.headers.get('Partner', None)
        app = request.headers.get('App', None)
        full_url_path = str(request.headers.get("FullUrlPath", default=True)).lower() == "true"
        if partner in ['bridge2capital', 'b2cap_hindi']:
            query_lang = request.headers.get('Language', None)
            if query_lang:
                if query_lang == 'hi':
                    partner = 'b2cap_hindi'
                elif query_lang == 'en':
                    partner = partner
                else:
                    return jsonify(dict(error_code=2, error_text='Check the language'), 400)
            else:
                return jsonify(dict(error_code=2, error_text='You missed one more parameter?'), 400)
        try:

            if request.data:
                r = request.json
                psychology = r.get("psychology")
                free_text, slider, image, drawing, voice, select = get_number_of_questions_to_shuffle(r)
                result = make_shuffled_list(partner, free_text, slider, image, drawing, voice, select,
                                            excluded_image_attrs=['score'], psychology=psychology,
                                            full_url_path=full_url_path)
            else:
                if partner in partners_scenario_questions:
                    result, scenario_id = get_scenario_questions(excluded_image_attrs=['score'],
                                                                 full_url_path=full_url_path)
                    response['scenario_id'] = scenario_id
                else:
                    result = get_default_partner_questions(partner, excluded_image_attrs=['score'],
                                                           full_url_path=full_url_path)

            random.shuffle(result)
            if len(result) > 0:
                response['Questions'] = result
                # response.headers['X-Content-Type-Options'] = 'nosniff'
                return jsonify(response), 200, {'Access-Control-Allow-Origin': '*',
                                                'Access-Control-Allow-Methods': 'POST',
                                                'Access-Control-Allow-Credentials': 'true'}
            else:
                return jsonify(dict(error_code=2, error_text='No questions for: %s' % partner)), 400

        except Exception as e:
            logging.error('Error while fetching questions {}'.format(e))
            return jsonify(dict(error_code=-1, error_text='Error wile fetching questions')), 500


@application.route("/api/getTypeformScores", methods=['POST'])
@application.route("/api/getTypeformScores/", methods=['POST'])
def get_typeform_scores():
    if request.method == 'POST':
        try:
            result = dict()
            score_results = dict()
            text_only_answers = []
            if request.data:  # Get Questions And Answers From TypeForm Payload
                r = request.json
                form_response = r.get("form_response") or raise_exception('form response is not found')
                hidden_fields = form_response.get("hidden") or raise_exception('hidden fields are not found')
                email = hidden_fields.get("email") or 'Email not found'
                customer_name = hidden_fields.get("name") or 'name not found'
                partner = hidden_fields.get("partner") or raise_exception('partner name not found')
                external_id = hidden_fields.get("externalid") or raise_exception('external id not found')
                definition = form_response.get("definition") or raise_exception(
                    'definition array not found in form response')
                questions = definition.get("fields")
                answers = form_response.get("answers") or raise_exception('no answers found for this form')

            # Get Text Only Answers
            for A in answers:
                text_only_answers.append(A.get("text")) if A.get("type") == 'text' else ''

            text_to_analyze = '.'.join(text_only_answers)
            text_analysis_score = get_scores(text_to_analyze, False)

            if text_analysis_score.get('error_code') == 0:
                scores = text_analysis_score.get('raw_scores')
                score_results['score'] = text_analysis_score.get('final_score')
                score_results['Message'] = define_message(score_results['score'])
            else:
                score_results['score'] = text_analysis_score.get('raw_scores')
                score_results['Message'] = 'error_code 0'

            result['typeform_response'] = request.json
            score_results['customerName'] = customer_name
            result['results'] = score_results

            save_to_confifrmu('get_typeform_scores', result)

            send_partner_notification(
                partner,
                "Scoring report from confirmU to customer = {}".format(customer_name),
                "customer_name: {customer_name}<br>Final score: {final_score}<br>Customer email;: {email}".format(
                    customer_name=customer_name,
                    final_score=score_results['score'],
                    email=email
                )
            )
            return jsonify({
                "Code": 0,
                "IsSuccess": True
            }), 200
        except Exception as e:
            logging.error('An Error Occurred while processing your request at /api/getTypeformScores/'.format(e))
            return jsonify({
                "Code": -1,
                "IsSuccess": False,
                "error_message": e
            }), 500


@application.route("/api/calculatePsychologyScore", methods=['POST'])
@application.route("/api/calculatePsychologyScore/", methods=['POST'])
@application.route("/api/getScenarioScore", methods=['POST'])
@application.route("/api/getScenarioScore/", methods=['POST'])
@auth_required
def get_psychology_score():
    partners_scenario_questions = ['hedgini', 'creditAI', 'LM_financials', 'epaysa', 'VOua8PmXQC']
    if request.method == 'POST':
        if request.data:
            try:
                r = request.json
                partner = request.headers.get("Partner") or raise_exception(
                    "partner {} not found".format(request.headers.get("Partner")))
                external_id = request.headers.get("ExternalId") or raise_exception(
                    "External Id not found")
                answers = r.get('Answers') or raise_exception("No Answers Passed To Calculate Score")
                psychology_scoring = str(request.headers.get("Psychology", default=False)).lower() == "true"

                if psychology_scoring or partner not in partners_scenario_questions:
                    send_full_response = str(request.headers.get("SendFullResponse", default=False)).lower() == "true"
                    spent_time = r.get('spentTime')
                    results = calculate_psychology_score(answers, partner)
                    send_partner_notification(
                        partner,
                        "Scoring report from confirmU to customer = {}".format(external_id),
                        "external_id: {external_id}<br>Final score: {final_score}".format(
                            external_id=external_id,
                            final_score=results.get('final_score')
                        )
                    )

                    partner_answers = {
                        'external_id': external_id,
                        'answers': answers,
                        'result': results,
                        'spent_time': spent_time,
                        'dizzy': True
                    }
                    # TODO : [To Be Removed] this code snippet is for testing purposes only
                    partner = 'scenario_scoring' if partner == 'dizzy' else partner
                    save_to_confifrmu(partner, partner_answers)

                    return jsonify({
                        "Code": 0,
                        "IsSuccess": True,
                        "result": results if send_full_response else ""
                    }), 200
                else:
                    scenario_id = request.headers.get("ScenarioId") or 1  # using default scenario as 1
                    email = request.headers.get("Email")
                    results = calculate_scenario_score(answers, scenario_id)
                    spent_time = r.get('spentTime')
                    try:
                        if email is None:
                            partner_emails = {
                                "hedgini": "tenant@hedgini.ca",
                                "LM_financials": "HAMZA@LMFINANCIALSERVICES.CA",
                                "creditAI": "Sabelo@tuesapp.com",
                                "epaysa": "admin@epaysa.co"
                            }
                            email = partner_emails.get(partner)
                        if email is not None:
                            send_notification("Scoring report from confirmU to customer = {customer}".format(
                                customer=partner + " Inc"), email,
                                "external_id: {external_id}<br>Final score: {final_score}<br>Recommendation: {recommendation}"
                                    .format(external_id=external_id, final_score=results.get("final_score"),
                                            recommendation=results.get("recommendation")))
                    except Exception as e:
                        print('An Error Occurred While Sending Email in scenario score : {}'.format(e))
                    partner_answers = {
                        'external_id': external_id,
                        'answers': answers,
                        'result': results,
                        'spent_time': spent_time,
                        'scenarioId': scenario_id
                    }
                    save_to_confifrmu(partner, partner_answers)

                    return jsonify({
                        "Code": 0,
                        "IsSuccess": True,
                        "result": results
                    }), 200

            except Exception as e:
                print('An Error Occurred While Calculating PsychologyScores' + str(e))
                return bad_request('An Error occurred while getting psychology score')
        else:
            return bad_request("request data not found")
    else:
        return r"use POST method to get data"


@application.route("/api/loanStatus", methods=['POST'])
@application.route("/api/loanStatus/", methods=['POST'])
@auth_required
def get_loan_status():
    if request.method == 'POST':
        if request.data:
            try:
                r = request.json
                partner = request.headers.get("Partner") or raise_exception(
                    "partner {} not found".format(request.headers.get("Partner")))
                loan_data = r['Loan Data']
                loan_status_object = dict()
                loan_status = dict()
                loan_status_object['partner'] = partner
                # each value is extracted individually to avoid storing unwanted data in the request
                loan_status['qualified_amount'] = loan_data.get('Qualified Amount')
                loan_status['loan_approved_date'] = loan_data.get('Loan Approved Date')
                loan_status['loan_duration'] = loan_data.get('Loan Duration')
                loan_status['installments'] = loan_data.get('Installments')
                loan_status['installment_amount'] = loan_data.get('Installment Amount')
                loan_status['days_late'] = loan_data.get('Days Late')
                loan_status['remaining_loan_amount'] = loan_data.get('Remaining Loan Amount')
                loan_status_object['loan_data'] = loan_status
                save_to_confifrmu('loan_status', loan_status_object)
                return jsonify({
                    "Code": 0,
                    "IsSuccess": True,
                    "result": "Success"
                }), 200
            except Exception as e:
                print('An Error Occurred While setting loan status : {}'.format(e))
                return bad_request('An Error occurred while setting loan status')
        else:
            return bad_request("Request data not found")
    else:
        return r"only POST is supported for this end point"


@application.route("/api/getAnswersResults", methods=['POST'])
@application.route("/api/getAnswersResults/", methods=['POST'])
@auth_required
def get_answers_results():
    if request.method == 'POST':
        if request.data:
            try:
                r = request.json
                partner = request.headers.get("Partner") or raise_exception(
                    "partner {} not found".format(request.headers.get("Partner")))
                partner_answers_collection = get_answers(partner, limit=0)
                partner_questions = get_partner_questions(partner, questions_types=['image'])
                answers_results_type = r.get("answers_types")
                answers_results = dict()
                for ans_type in answers_results_type:
                    for partner_ans in partner_answers_collection:
                        if partner_ans.get('answers') is not None:
                            answers = partner_ans['answers'].get(ans_type)
                            for ans in answers:
                                ans_id = ans['QuestionId']
                                matched_question = list(filter(lambda question: question['QuestionId'] == ans_id,
                                                               partner_questions[ans_type]))[0]
                                first_opt = matched_question[ans_type.capitalize() + 's'][0]
                                second_opt = matched_question[ans_type.capitalize() + 's'][1]
                                if answers_results.get(ans_id) is None:
                                    answers_results[ans_id] = {
                                        first_opt['image_id']: {
                                            "img_url": first_opt['url'],
                                            "selected_times": 1 if ans['QuestionAnswer'] == 0 else 0
                                        },
                                        second_opt['image_id']: {
                                            "img_url": second_opt['url'],
                                            "selected_times": 1 if ans['QuestionAnswer'] == 1 else 0
                                        }
                                    }
                                else:
                                    answers_results[ans_id][first_opt['image_id']].update(
                                        {
                                            "selected_times": answers_results[ans_id][first_opt['image_id']][
                                                                  'selected_times'] + (
                                                                  1 if ans['QuestionAnswer'] == 0 else 0)
                                        })
                                    answers_results[ans_id][second_opt['image_id']].update(
                                        {
                                            "selected_times": answers_results[ans_id][second_opt['image_id']][
                                                                  'selected_times'] + (
                                                                  1 if ans['QuestionAnswer'] == 1 else 0)
                                        })
                return jsonify({
                    "Code": 0,
                    "IsSuccess": True,
                    "result": answers_results
                }), 200

            except Exception as e:
                print('An Error Occurred While getting image questions results : {}'.format(e))
                return bad_request('An Error Occurred While getting image questions results')


@application.route("/api/getScore", methods=['POST'])
@application.route("/api/getScore/", methods=['POST'])
@auth_required
def get_score():
    if request.method == 'POST':
        if request.data:
            try:
                r = request.json
                partner = request.headers.get("Partner") or raise_exception(
                    "partner {} not found".format(request.headers.get("Partner")))
                send_email = str(request.headers.get("SendEmail", default=False)).lower() == "true"
                external_id = request.headers.get("ExternalId", None)
                questions = r['Questionaire'].get('Questions') or raise_exception("answers not found in request")
                email = r['Customer'].get('EmailAddress')
                trans = str(request.headers.get("Translate", default=False)).lower() == "true"

                text_answers = [
                    x['FreeTextQuestion'].get('FreeTextAnswer') for x in questions if
                    x['General'].get('QuestionType') == 'FreeText'
                ]

                if trans:  # in case answers language is NOT english
                    try:
                        translated = translate(text_answers)
                        logging.debug(translated)
                        text_answers = [x.get('translatedText') for x in translated['data']['translations']]
                    except Exception as e:
                        logging.error('Error while translating to english: {}'.format(e))
                        text_answers = text_answers

                joined_text_answers = '.'.join(text_answers)
                images_score = get_images_scores(questions, partner)
                ta = get_scores(joined_text_answers)
                ta_score = min(calculate_final_score(ta.get('raw_scores'), ConstValues.patterns) * 1.25, 100)
                ta_error = ta.get('error_message')
                sent_score = get_sentiment_score(text_answers)
                final_score = calculate_weighted_final_score(partner, ta_score, sent_score, images_score)
                result = {
                    "email": email,
                    "final_score": final_score,
                    "ta_score": ta_score,
                    "ta_error": ta_error,
                    "sent_score": sent_score,
                    "images_score": images_score,
                    "request": r,
                    "text_answers": text_answers
                }

                if external_id:  # in case user has external id add it to results
                    result['external_id'] = external_id

                save_to_confifrmu(partner, result)

                if send_email:  # in case sending email is required
                    try:
                        send_partner_notification(
                            partner,
                            "Scoring report from confirmU to customer = {}".format(external_id),
                            "ExternalID: {ext_id}<br>Final score: {final_score}<br>Customer email: {email}".format(
                                ext_id=external_id,
                                final_score=int(final_score),
                                email=email
                            )
                        )
                    except Exception as e:
                        logging.error('Error while sending an email: {}'.format(e))

                # Return Response (Success)
                response = dict()
                response['message'] = "We are going to contact you soon<br>Have a nice day!"
                response_result = dict(result)  # create a shallow copy
                del response_result['request']  # remove request from response
                response['result'] = response_result
                return make_response(jsonify(response), 200)

            except Exception as e:
                logging.error('ERROR: {}'.format(e))
                return bad_request(e)
        else:
            return bad_request("request data not found")
    else:
        return r"use POST method to get data"


@application.route("/api/getPartnerMetadata", methods=['POST'])
@application.route("/api/getPartnerMetadata/", methods=['POST'])
def get_partner_metadata():
    if request.method == 'POST':
        try:
            response = {
                "Code": 0,
                "IsSuccess": True,
                "result": False,
                "message": "Partner Doesn't Exist"
            }
            partner = request.headers.get("Partner") or raise_exception(
                "partner {} not found".format(request.headers.get("Partner")))
            access_token = request.headers.get("Access-Token")
            if os.environ.get("{partner_name}_TOKEN".format(partner_name=partner.upper())) == access_token:
                response['result'] = True
                response['message'] = "Partner Exists"

            return jsonify(response), 200, {'Access-Control-Allow-Origin': '*',
                                            'Access-Control-Allow-Methods': 'POST'}
        except Exception as e:
            print('An Error Occurred While getting partner info : {}'.format(e))
            return bad_request('An Error Occurred While getting partner info {}'.format(e))


@application.route("/record_full/", methods=['GET', 'POST'])
@application.route("/start/", methods=['GET', 'POST'])
def record_audio_for_form():
    return nine_field_stt('default', 'speech_to_text')


@application.route("/VOua8PmXQC", methods=['GET', 'POST'])
def VOua8PmXQC():
    """this endpoint belongs to smartcredit"""
    return nine_field_stt('VOua8PmXQC', 'VOua8PmXQC')


@application.route("/enterid", methods=['GET', 'POST'])
def enterid():
    return nine_field_stt('enterid', 'enterid')


@application.route("/enterid_dropdown", methods=['GET', 'POST'])
def enterid_dropdown():
    return nine_field_stt('enterid_dropdown', 'enterid')


@application.route("/leo", methods=['GET', 'POST'])
def leo():
    return nine_field_stt('leo', 'leo', 'leo_form.html', 'result.html')


@application.route("/process_wav", methods=['POST'])
def process_wav():
    from utils import google_services
    data = request.form
    audio = data.get('audio')[22:]
    session = str(data.get('session'))
    lang = str(data.get('language'))
    if not os.path.exists(os.path.join('temp_audio', session)):
        os.makedirs(os.path.join('temp_audio', session))
    file_name = str(os.path.join('temp_audio', session, str(data.get('fname')))).replace(':', '-')
    with open(file_name, 'wb') as f:
        f.write(base64.decodebytes(str.encode(audio)))
    try:
        transcription = google_services.transcribe_file(file_name, lang)
        text = transcription.get('results')[0].get('alternatives')[0].get('transcript')
    except Exception as e:
        text = '=( I can\'t recognize your speech'

    return jsonify(
        {
            'result': 'success',
            'transcription': text
        }
    )


@application.route("/save_wav", methods=['POST'])
def save_wav():
    data = request.form
    audio = data.get('audio')[22:]
    file_name = str('{}-{}'.format(
        str(data.get('session')),
        str(data.get('fname'))
    ).replace(':', '-'))
    if not os.path.exists('temp_audio'):
        os.makedirs('temp_audio')
    with open(os.path.join('temp_audio', file_name), 'wb') as f:
        f.write(base64.decodebytes(str.encode(audio)))

    return jsonify(
        {
            'result': 'success',
            'file': file_name
        }
    )


@application.route("/getaudio/<session>/", methods=['GET'])
def return_audio(session):
    filename = str(request.args.get('file'))
    if not os.path.exists(os.path.join('temp_audio', session, filename)):
        abort(404)
    return send_file(
        os.path.join('temp_audio', session, filename),
        mimetype="audio/wav",
        as_attachment=True,
        attachment_filename=filename)


@application.route("/detailed_analysis/", methods=['GET', 'POST'])
def detailed_analysis():
    form = OneFieldForm(request.form)
    if request.method == 'GET':
        return render_template('default_form_template.html', form=form)
    if request.method == 'POST':
        scores = {}
        parts = request.form['text'].split(';')
        if len(parts) == 1:
            parts = request.form['text'].split()
        for x in parts:
            word_score = get_scores(x, gibber_check=False)
            if word_score.get('raw_scores') is not None:
                pre_score = word_score.get('raw_scores')
                scores[x] = {y: round(exp(float(pre_score[y])) / exp(1), 3) for y in ConstValues.patterns}
        result = dict(
            form=request.form,
            text_to_analyze=request.form['text'],
            scores=scores,
            patterns=ConstValues.patterns,
        )
        # save_to_confifrmu('speech_to_text', result)

        return render_template('results_by_word.html', result=result)


@application.route("/credit24/", methods=['GET', 'POST'])
def credit24():
    form = FiveFieldForm(request.form)
    if request.method == 'GET':
        return render_template('5_field_form.html', form=form, questions=get_partner_questions('credit24'))
    if request.method == 'POST':
        if form.validate():
            raw_text = '. '.join([request.form[x] for x in request.form if x not in ['email', 'customer_name']])
            text_to_analyze = translate(raw_text).get('data').get('translations')[0].get('translatedText')
            scores = get_scores(text_to_analyze).get('raw_scores')
            logging.debug(str(ConstValues.patterns))
            if len(ConstValues.patterns) > 0:
                final_score = calculate_final_score(scores, ConstValues.patterns)
            else:
                logging.error("No patterns, final score defined as 0")
                final_score = 0
            decision = define_message(final_score)
            result = dict(
                name=request.form['customer_name'],
                email=request.form['email'],
                form=request.form,
                raw_text=raw_text,
                text_to_analyze=text_to_analyze,
                scores=scores,
                final_score=final_score,
                decision=decision
            )
            save_to_confifrmu('crefit24', result)

            return render_template('raw_results.html', result=result)
        else:
            flash_errors(form)


@application.route("/report/<collection>/", methods=['GET', 'POST'])
@auth_required
def report_download(collection):
    form = AuthenticationForm(request.form)
    if request.method == 'GET':
        return render_template('default_form_template.html', form=form)
    if request.method == 'POST':
        password = request.form['password'].encode()
        hashed = b'$2b$12$JbEAMuM2EeOAkABAhDyNBe7.TyVqHCtm7LJfFsWXZpyu/CMLbsv6y'
        if bcrypt.hashpw(password, hashed) == hashed:
            create_report(collection)
            return send_file(
                '{}.xlsx'.format(collection),
                mimetype='application/vnd.ms-excel',
                as_attachment=True,
                attachment_filename='{}.xlsx'.format(collection)

            )
        else:
            return redirect("/report/", code=302)


@application.route("/credit24_edit/", methods=['GET', 'POST'])
def credit24_edit_questions():
    partner = 'credit24'
    form = Credit24ChangeQuestionsForm(request.form)
    if request.method == 'GET':
        cur_questions = get_partner_questions(partner)
        return render_template('credit24_edit_questions.html', form=form, cur_questions=cur_questions)
    if request.method == 'POST':
        password = request.form['password'].encode()
        hashed = b'$2b$12$JbEAMuM2EeOAkABAhDyNBeYH/CtcK63Ds.ScOscn8P6AoD2Rgh/ta'
        if bcrypt.hashpw(password, hashed) == hashed:
            questions = request.form.to_dict()
            del questions['password']
            for x in questions:
                questions[x] = questions[x].replace('{', '').replace('}', '')
            set_questions(partner, questions)
            cur_questions = get_partner_questions(partner)
            return render_template('credit24_edit_questions.html', form=form, cur_questions=cur_questions)
        else:
            cur_questions = get_partner_questions(partner)
            return render_template(
                'credit24_edit_questions.html',
                form=form,
                cur_questions=cur_questions,
                error_message='Password is incorrect!'
            )


@application.route('/partner', methods=['GET'])
@auth_required
def partner():
    return render_template('partner.html')


@application.route('/partner/questions')
@auth_required
def edit_questions():
    partner = session['partner']
    qs = get_partner_questions(partner)
    langs = [x for x in qs]
    langnames = {x: qs[x]['display_name'] for x in qs}
    return render_template('partner_questions.html', langs=langs, langnames=langnames)


@application.route('/partner/answers')
@auth_required
def show_answers():
    partner = session['partner']
    answers = get_answers(partner)
    return render_template('partner_answers.html', answers=answers)


@application.route('/partner/login', methods=['GET', 'POST'])
def partner_login():
    form = LoginForm(request.form)
    if form.validate():
        user = get_user_by_name(request.form['username'])
        if user is not None and bcrypt.hashpw(request.form['password'].encode(), user['password_hash'].encode()) == \
                user['password_hash'].encode():
            session['partner'] = user['partner']
            return redirect('/partner')

    return render_template('partner_login.html', form=form)


@application.route('/partner/logout', methods=['GET'])
def partner_logout():
    session.pop('partner', None)
    return redirect('/partner/login')


@application.route('/get_translation', methods=['POST'])
def get_translation():
    a = request.form['language']
    qs = get_partner_questions(session['partner']).get(a)
    del qs['display_name']
    return jsonify(qs), 200


@application.route('/set_translation', methods=['POST'])
def set_translation():
    lang = request.form['language']
    result = update_questions(session['partner'], lang, request.form)
    if result is True:
        return jsonify("Records updated successfully"), 200
    else:
        return jsonify(result), 500


@application.route('/api/text_emotions', methods=['POST'])
def text_emotions():
    try:
        try:
            req_data = json.loads(request.data)
        except Exception as e:
            req_data = json.loads(request.data.decode('ascii', 'ignore').encode('utf-8'))
    except Exception as e:
        return make_response(jsonify(dict(error_code=400, error_text='unable to parse request: %s' % str(e))), 400)
    result = dict()
    partner = request.headers.get('Partner')
    locale = request.headers.get('Locale') or 'en'
    try:
        word_emotions = []
        emx = Emoxicon.emoxicon
        for word in req_data.get('text').split(' '):
            word_emotions.append(Emoxicon.emoxicon.get(word))
        for em in ['anger', 'anticipation', 'disgust', 'fear', 'joy', 'negative', 'positive', 'sadness', 'surprise',
                   'trust']:
            result[em] = sum([int(y[em]) for y in word_emotions if y is not None])
        return jsonify({
            "Code": 0,
            "IsSuccess": True,
            "Emotions": result})
    except Exception as e:
        return jsonify({'error': 'error getting emotional scores: {}'.format(e)})


@application.route('/temp_audio')
def get_temp():
    file = request.args.get('file')
    return send_file(
        os.path.join('temp_audio', file),
        mimetype="audio/wav",
        attachment_filename=file
    )


@application.route('/.well-known/acme-challenge/<file>')
def ssl_valid(file):
    return send_file(
        os.path.join('static', file),
        attachment_filename=file,
        as_attachment=True
    )


# endregion

if __name__ == '__main__':
    application.debug = True
    application.run(host='localhost', port=5748)
