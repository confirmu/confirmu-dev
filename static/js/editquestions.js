window.onload = function(){
   document.getElementById("sendquestions-btn").style.display = "none";
   document.getElementById("getquestions-btn").addEventListener("click", function(){
        //getTearSheet();
        var reqdata = {};
        reqdata['language'] = document.getElementById("language").value;
        questions = document.getElementById("questions");
        while (questions.firstChild) {
            questions.removeChild(questions.firstChild);
        };
        document.getElementById("sendquestions-btn").style.display = "none";
		$.ajax({
            url: '/get_translation',
            data: reqdata,
            type: 'POST',
            success: function (data) {
                for (var property in data) {
                    if (data.hasOwnProperty(property)) {
                        var question = document.createElement('div');
                        question.className = 'input-group';
                        var qspan =  document.createElement('span');
                        qspan.className ="input-group-addon";
                        qspan.innerHTML = property;
                        var qinput = document.createElement('input');
                        qinput.className ="form-control";
                        qinput.setAttribute("type", "text");
                        qinput.setAttribute("id", property);
                        qinput.setAttribute("name", property);
                        qinput.value = data[property];
                        question.appendChild(qspan);
                        question.appendChild(qinput);
                        questions.appendChild(question);
                    };
                };
                document.getElementById("sendquestions-btn").style.display = "block";


                },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
            }
        });

    }, false);


    document.getElementById("sendquestions-btn").addEventListener("click", function(){
        //getTearSheet();
        var formValues = {};

		for (var i = 0; i < document.getElementsByClassName("form-control").length; i++) {
		    formValues[document.getElementsByClassName("form-control")[i].id] = document.getElementsByClassName("form-control")[i].value
		};
		$.ajax({
            url: '/set_translation',
            data: formValues,
            type: 'POST',
            success: function (data) {
                alert(data);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });

    }, false);

};


