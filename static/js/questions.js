function translate_questions(toLanguage) {
    var elements = document.getElementsByClassName('questions')[0].getElementsByTagName('li');
    for (i=0; i<elements.length; i++) {
        if (elements[i].getElementsByTagName('input').length > 0) {
            var parElem = elements[i].getElementsByTagName('input')[0];
        } else {
            var parElem = elements[i].getElementsByTagName('select')[0];
        }
        var term = parElem.id;
        if (translations[toLanguage][term] !== null && typeof translations[toLanguage][term] === 'object') {
            var dd = document.createElement('select');
            dd.name = term;
            dd.id = term;
            for (j=0; j<translations[toLanguage][term]['answers'].length; j++) {
                var new_option = document.createElement('option');
                new_option.value = translations[toLanguage][term]['answers'][j];
                new_option.innerHTML = translations[toLanguage][term]['answers'][j];
                dd.appendChild(new_option);
            }
            elements[i].replaceChild(dd, parElem);
            elements[i].getElementsByTagName('label')[0].textContent = translations[toLanguage][term]['label'];
        } else {
            elements[i].getElementsByTagName('label')[0].textContent = translations[toLanguage][term];
        }
    }

}

function set_available_langs() {
    var available_langs = Object.keys(translations);
    var dropdown = document.getElementById('dropdown');

    for (i=0; i<available_langs.length; i++) {
        var new_option = document.createElement('option');
        new_option.value = available_langs[i];
        new_option.innerHTML = translations[available_langs[i]]['display_name'];
        dropdown.appendChild(new_option);
    }
}
