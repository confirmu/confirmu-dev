

function translate_questions(toLanguage) {
    var elements = document.getElementsByClassName('questions')[0].getElementsByTagName('li');
    for (i=0; i<elements.length; i++) {
        var term = elements[i].getElementsByTagName('input')[0].id;
        elements[i].getElementsByTagName('label')[0].textContent = translations[toLanguage][term];
    }

}

function set_available_langs() {
    var available_langs = Object.keys(translations);
    var dropdown = document.getElementById('dropdown');

    for (i=0; i<available_langs.length; i++) {
        var new_option = document.createElement('option');
        new_option.value = available_langs[i];
        new_option.innerHTML = translations[available_langs[i]]['display_name'];
        dropdown.appendChild(new_option);
    }
}
