let currentTab = 0; // Current tab is set to be the first tab (0)
let tabs = document.getElementsByClassName("tab");
let record_buttons = document.getElementById("record_buttons");

for (i = 0; i < tabs.length; i++) {
    var newSpan = document.createElement('span');
      newSpan.classList.add("step");
    document.getElementById("progress-bar").appendChild(newSpan)
}
var errors = document.getElementById('errors');

document.addEventListener( 'keydown', function( ev ) {
    var keyCode = ev.keyCode || ev.which;
    // enter
    if( keyCode === 13 ) {
        ev.preventDefault();
        self.nextPrev(1);
    }
    if( keyCode === 9 ) {
        ev.preventDefault();
    }
} );


showTab(currentTab); // Display the current tab

function showTab(n) {
  tabs[n].style.display = "block";
  let inputs = tabs[n].getElementsByTagName('input');
  if (inputs.length === 1) {
    inputs[0].focus();
  }
  if (tabs[n].classList.contains("voice")) {
      record_buttons.style.display = "block";
  } else {
      record_buttons.style.display = "none";
  }

  if (n === (tabs.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Send answers";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  fixStepIndicator(n);
}

function nextPrev(n) {
  if (n === 1 && !validateForm()) return false;
  tabs[currentTab].style.display = "none";
  currentTab = currentTab + n;
  if (currentTab >= tabs.length) {
    document.getElementById('anna_form').style.display = "none";
    let rqst = transformForm(document.getElementById("anna_form"));
    //console.log(rqst);
    //document.getElementById("anna_form").submit();
    let xhr = new XMLHttpRequest();
    xhr.open("POST", processing_url, true);

    //Send the proper header information along with the request
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("ExternalId", external_id);
    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let result = document.getElementById('result');
            result.innerHTML = this.responseText;
            result.style.display = "block";
        }
    };
    xhr.send(JSON.stringify(rqst));
    return false;
  }
  showTab(currentTab);
}

function validateForm() {
  var x, y, i, valid = true;
  let select_checks = [];
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  for (i = 0; i < y.length; i++) {
    if (y[i].value.length === 0 || (y[i].name === 'email' && !validateEmail(y[i].value))) {
      y[i].className += " invalid";
      valid = false;
    }
    if (y[i].name.indexOf("text") > 0 && y[i].value.length < 10) {
      y[i].className += " invalid";
      valid = false;
    }
    if (y[i].name.indexOf("select") > 0) {
        select_checks.push(y[i].checked)
    }

  }
  if (select_checks.length > 0 && !select_checks.includes(true)) {
      valid = false;
  }
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  x[n].className += " active";
}


function transformForm(form) {
    let frm = serializeArray(form);
    let response = {};
    let Questions = [];
    let LastName = '';
    let EmailAddress = '';
    frm.forEach(function(inpt) {
        if (inpt.name === 'name') {LastName = inpt.value}
        else if (inpt.name === 'email') {EmailAddress = inpt.value}
        else if (inpt.name.split('-')[0] === 'question' && inpt.name.split('-')[2] === 'text') {
            let answer = {};
            answer.FreeTextAnswer = inpt.value;
            answer.FreeTextQuestion = document.getElementById(inpt.name).innerHTML;
            let question = {};
            question.FreeTextQuestion = answer;
            let general = {};
            general.QuestionId = inpt.name.split('-')[1];
            general.QuestionType = "FreeText";
            question.General = general;
            Questions.push(question);
        }
        else if (inpt.name.split('-')[0] === 'question' && inpt.name.split('-')[3] === 'select') {
            let answer = {};
            answer.FinalSelection = inpt.value;
            answer.SelectionQuestionText = document.getElementById(inpt.name).innerHTML;
            answer.SelectionQuestionCode = inpt.name.split('-')[2];
            let question = {};
            question.SelectionQuestion = answer;
            let general = {};
            general.QuestionId = inpt.name.split('-')[1];
            general.QuestionType = "Selection";
            question.General = general;
            Questions.push(question);
        }
        else if (inpt.name.split('-')[0] === 'question' && inpt.name.split('-')[2] === 'voice') {
            let answer = {};
            answer.VoiceQuestionText = document.getElementById(inpt.name).innerHTML;
            answer.VoiceQuestionFile = inpt.value;
            let question = {};
            question.VoiceQuestion = answer;
            let general = {};
            general.QuestionId = inpt.name.split('-')[1];
            general.QuestionType = "Voice";
            question.General = general;
            Questions.push(question);
        }
    });
    response.Customer = {};
    response.Customer.EmailAddress = EmailAddress;
    response.Customer.LastName = LastName;
    response.Questionaire = {};
    response.Questionaire.Questions = Questions;
    if (document.getElementById('total_amount')) {
        response.Loan = {};
        response.Loan.TotalAmount = document.getElementById('total_amount').value;

        if (document.getElementById('loan_length')) {
            response.Loan.Length = document.getElementById('loan_length').value;
            response.Loan.Periods = payment_period;
        }

        if (document.getElementById('payment_value')) {
            response.Loan.Payment = document.getElementById('payment_value').innerHTML
        }
    }
    return response;
}


function serializeArray(form) {
    var field, l, s = [];
    if (typeof form == 'object' && form.nodeName == "FORM") {
        var len = form.elements.length;
        for (var i=0; i<len; i++) {
            field = form.elements[i];
            if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
                if (field.type == 'select-multiple') {
                    l = form.elements[i].options.length;
                    for (j=0; j<l; j++) {
                        if(field.options[j].selected)
                            s[s.length] = { name: field.name, value: field.options[j].value };
                    }
                } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
                    s[s.length] = { name: field.name, value: field.value };
                }
            }
        }
    }
    return s;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

let session_id = uuidv4();
let audio_context;
let recorder;
let audio_stream;

function Initialize() {
    try {
        // Monkeypatch for AudioContext, getUserMedia and URL
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        navigator.getUserMedia = ( navigator.getUserMedia ||
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);

        window.URL = window.URL || window.webkitURL;
        audio_context = new AudioContext;
    } catch (e) {
        alert('No web audio support in this browser!');
    }
}

function startRecording() {

    navigator.getUserMedia({ audio: true }, function (stream) {
        audio_stream = stream;
        let input = audio_context.createMediaStreamSource(stream);
        recorder = new Recorder(input);
        recorder && recorder.record();
        //err_msg.textContent = "Recording...";
        document.getElementById("start-btn").disabled = true;
        document.getElementById("stop-btn").disabled = false;

    }, function (e) {
        console.error('No live audio input: ' + e);
        //err_msg.textContent = "No live audio input: " + e;
    });
}

function stopRecording(callback, AudioFormat) {
    recorder && recorder.stop();
    audio_stream.getAudioTracks()[0].stop();


    if(typeof(callback) == "function"){

        recorder && recorder.exportWAV(function (blob) {
            callback(blob);
            recorder.clear();
        }, (AudioFormat || "audio/wav"));
    }
}

// Handle on start recording button
document.getElementById("start-btn").addEventListener("click", function(){
    Initialize();
    startRecording();
}, false);

// Handle on stop recording button
document.getElementById("stop-btn").addEventListener("click", function(){
    let _AudioFormat = "audio/wav";
    document.getElementById("stop-btn").disabled = true;
    stopRecording(function(AudioBLOB){
        let url = URL.createObjectURL(AudioBLOB);

        let reader = new FileReader();
        let base64data;
        //document.getElementsByClassName('error-message')[0].textContent = "Saving your voice.."
        reader.readAsDataURL(AudioBLOB);
        reader.onloadend = function() {
             base64data = reader.result;
             //document.getElementsByClassName('error-message')[0].textContent = "Trying to transcribe.."
             //console.log(base64data);

             $.post(
                "/save_wav",
                {
                    audio: base64data,
                    fname: new Date().toISOString() + '.wav', session: session_id,
                 },
                function(data, status){
                    console.log(data);
                    let result = tabs[currentTab].getElementsByTagName('input')[0];
                    //document.getElementsByClassName('current')[0].children[1].value = data.transcription
                    //document.getElementsByClassName('error-message')[0].textContent = ""
                    result.value = data.file;
                    console.log(data.file);
                    nextPrev(1);
                    document.getElementById("start-btn").disabled = false;
                }
             );
        }



    }, _AudioFormat);


}, false);

if (document.getElementById('loan_details')) {

    let total_amount = document.getElementById("total_amount");
    let total_amount_value = document.getElementById("total_amount_value");
    total_amount_value.innerHTML = total_amount.value;

    let loan_length = document.getElementById("loan_length");
    let loan_length_value = document.getElementById("loan_length_value");
    loan_length_value.innerHTML = loan_length.value;

    let payment_value = document.getElementById("payment_value");
    calculate(total_amount, loan_length, interest_rate, total_amount_value, loan_length_value, payment_value);


    total_amount.oninput = function () {
        total_amount_value.innerHTML = this.value;
        calculate(total_amount, loan_length, interest_rate, total_amount_value, loan_length_value, payment_value)
    };

    loan_length.oninput = function () {
        loan_length_value.innerHTML = this.value;
        calculate(total_amount, loan_length, interest_rate, total_amount_value, loan_length_value, payment_value)

    };


    function calculate(amount, months, apr, total_amount_value, loan_length_value, payment_value) {
        // to the number of monthly payments.
        let principal = parseFloat(amount.value);
        let interest;
        if (payment_period === 'week') {
            interest = parseFloat(apr) / 100 / 52;
        } else if (payment_period === 'month') {
            interest = parseFloat(apr) / 100 / 12;
        } else if(payment_period === 'day'){
            interest = parseFloat(apr) / 100 / 30;
        } else {
            console.log("no valid period for calculations");
            return 1;
        }

        let payments = parseFloat(months.value);

// compute the monthly payment figure
        let x = Math.pow(1 + interest, payments); //Math.pow computes powers
        let monthly = (principal * x * interest) / (x - 1);

// If the result is a finite number, the user's input was good and
// we have meaningful results to display
        if (isFinite(monthly)) {
            // Fill in the output fields, rounding to 2 decimal places
            payment_value.innerHTML = monthly.toFixed(2);

        }
        else {
            payment_value.innerHTML = ""; // Erase the content of these elements

        }
    }

}