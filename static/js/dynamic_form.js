var currentTab = 0; // Current tab is set to be the first tab (0)
var tabs = document.getElementsByClassName("tab");
for (i = 0; i < tabs.length; i++) {
    var newSpan = document.createElement('span');
      newSpan.classList.add("step");
    document.getElementById("progress-bar").appendChild(newSpan)
}
var errors = document.getElementById('errors')

document.addEventListener( 'keydown', function( ev ) {
    var keyCode = ev.keyCode || ev.which;
    // enter
    if( keyCode === 13 ) {
        ev.preventDefault();
        self.nextPrev(1);
    };
    if( keyCode === 9 ) {
        ev.preventDefault();
    }
} );


showTab(currentTab); // Display the current tab

function showTab(n) {
  var x = document.getElementsByClassName("tab");
  x[n].style.display = "block";
  var inputs = x[n].getElementsByTagName('input')
  if (inputs.length === 1) {
    inputs[0].focus();
  }

  if (n === (x.length - 1)) {
    document.getElementById("nextBtn").innerHTML = "Send answers";
  } else {
    document.getElementById("nextBtn").innerHTML = "Next";
  }
  fixStepIndicator(n);
}

function nextPrev(n) {
  var x = document.getElementsByClassName("tab");
  if (n === 1 && !validateForm()) return false;
  x[currentTab].style.display = "none";
  currentTab = currentTab + n;
  if (currentTab >= x.length) {
    document.getElementById('anna_form').style.display = "none";
    let rqst = transformForm(document.getElementById("anna_form"));
    console.log(rqst);
    //document.getElementById("anna_form").submit();
    let xhr = new XMLHttpRequest();
    xhr.open("POST", '/mobbisurance', true);

    //Send the proper header information along with the request
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.onreadystatechange = function() { // Call a function when the state changes.
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            let result = document.getElementById('result');
            result.innerHTML = this.responseText;
            result.style.display = "block";
        }
    };
    xhr.send(JSON.stringify(rqst));
    return false;
  }
  showTab(currentTab);
}

function validateForm() {
  var x, y, i, valid = true;
  x = document.getElementsByClassName("tab");
  y = x[currentTab].getElementsByTagName("input");
  for (i = 0; i < y.length; i++) {
    if (y[i].value.length === 0 || (y[i].name === 'email' && !validateEmail(y[i].value))) {
      y[i].className += " invalid";
      valid = false;
    }
  }
  if (valid) {
    document.getElementsByClassName("step")[currentTab].className += " finish";
  }
  return valid; // return the valid status
}

function fixStepIndicator(n) {
  var i, x = document.getElementsByClassName("step");
  for (i = 0; i < x.length; i++) {
    x[i].className = x[i].className.replace(" active", "");
  }
  x[n].className += " active";
}


function transformForm(form) {
    let frm = serializeArray(form);
    let response = {};
    let Questions = [];
    let LastName = '';
    let EmailAddress = '';
    frm.forEach(function(inpt) {
        if (inpt.name === 'name') {LastName = inpt.value}
        if (inpt.name === 'email') {EmailAddress = inpt.value}
        if (inpt.name.split('-')[0] === 'question' && inpt.name.split('-')[2] === 'text') {
            let answer = {};
            answer.FreeTextAnswer = inpt.value;
            answer.FreeTextQuestion = document.getElementById(inpt.name).innerHTML;
            let question = {};
            question.FreeTextQuestion = answer;
            let general = {};
            general.QuestionId = inpt.name.split('-')[1];
            general.QuestionType = "FreeText";
            question.General = general;
            Questions.push(question);
        }
        if (inpt.name.split('-')[0] === 'question' && inpt.name.split('-')[3] === 'select') {
            let answer = {};
            answer.FinalSelection = inpt.value;
            answer.SelectionQuestionText = document.getElementById(inpt.name).innerHTML;
            answer.SelectionQuestionCode = inpt.name.split('-')[2];
            let question = {};
            question.SelectionQuestion = answer;
            let general = {};
            general.QuestionId = inpt.name.split('-')[1];
            general.QuestionType = "Selection";
            question.General = general;
            Questions.push(question);
        }
    });
    response.Customer = {};
    response.Customer.EmailAddress = EmailAddress;
    response.Customer.LastName = LastName;
    response.Questionaire = {};
    response.Questionaire.Questions = Questions;
    return response;
}


function serializeArray(form) {
    var field, l, s = [];
    if (typeof form == 'object' && form.nodeName == "FORM") {
        var len = form.elements.length;
        for (var i=0; i<len; i++) {
            field = form.elements[i];
            if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
                if (field.type == 'select-multiple') {
                    l = form.elements[i].options.length;
                    for (j=0; j<l; j++) {
                        if(field.options[j].selected)
                            s[s.length] = { name: field.name, value: field.options[j].value };
                    }
                } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
                    s[s.length] = { name: field.name, value: field.value };
                }
            }
        }
    }
    return s;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
};