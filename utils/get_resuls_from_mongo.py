from pymongo import MongoClient
import os
from conf import MongoDB
import pandas as pd


def create_report(collection='get_scores'):
    client = MongoClient(MongoDB.uri)
    db = client.confirmu

    scores = list(db[collection].find({}))
    results = []
    for x in scores:
        if x.get('request_data') is not None and x.get('response') is not None:
            res = [
                x['request_data'].get('customer').get('AboutMe'),
                x['response'].get('Number'),
                x.get('created'),
                x.get('partner')
            ]

            if x.get('scores') is not None:
                if not x['scores'].get('top') is None:
                    del x['scores']['top']
                res.extend(list(x['scores'].values()))
            else:
                res.extend([0] * 59)
            results.append(res)

    df = pd.DataFrame(
        results,
        columns=[
            'text', 'final_score', 'created', 'partner',
            'active', 'adjusted', 'adventurous', 'aggressive', 'agreeableness', 'ambitious', 'anxious', 'artistic', 'assertive',
            'body', 'cautious', 'cheerful', 'cold', 'conscientiousness', 'cooperative', 'depression', 'disciplined', 'dutiful',
            'emotionally', 'empathetic', 'energetic', 'extraversion', 'family', 'food', 'friendly', 'friendship', 'generous',
            'genuine', 'happiness', 'health', 'humble', 'imaginative', 'impusive', 'independent', 'insecure', 'intellectual',
            'leisure', 'liberal', 'melancholy', 'money', 'nest', 'neuroticism', 'openness', 'organized', 'persuasive', 'power',
            'religion', 'reward', 'self-assured', 'self-conscious', 'sexually', 'sociable', 'social', 'stressed', 'thinking',
            'trusting', 'type-a', 'work', 'workhorse'
        ]
    )

    df.to_excel('{}.xlsx'.format(collection))
