import emojis
import nltk
from nltk.tokenize import word_tokenize, sent_tokenize
import re

from utils.dictionaries import CustomDictionaries


# Count use of emojiis
def count_emojis(text):
    result = dict()
    result['count'] = emojis.count(text)
    result['count_unique'] = emojis.count(text, unique=True)
    return result


# New dictionary – count future tense words
def count_future_tense(text):
    words1 = word_tokenize(text)
    pos_tags1 = nltk.pos_tag(words1)
    return pos_tags1


def count_custom_freq(text):
    result = dict()
    words = list(map(lambda x: x.lower(), re.findall(r'[A-Za-z\-]+', text)))
    for x in CustomDictionaries.dictionaries:
        result[x] = dict()
        result[x]['count'] = len([y for y in words if y in CustomDictionaries.dictionaries[x]])
    return result


def freq_analyzer(text):
    result = dict()
    result['emojis'] = count_emojis(text)
    result.update(count_custom_freq(text))

    return result


if __name__ == '__main__':
    print(count_emojis('Prefix 😄 🐍 😄 🐍 Sufix'))
    print(count_future_tense('I\'m going to have a short vacation. Thats gonna be great. Next stop would be announced'))
    print(count_custom_freq('She is much older then me. I want to run away and drive my car fast, master'))
    print(freq_analyzer('Prefix 😄 🐍 😄 🐍 Sufix I\'m going to have a short vacation. Thats gonna be great. Next stop would be announced She is much older then me. I want to run away and drive my car fast, master' ))
