from pymongo import MongoClient
import conf
import pprint


def get_income(customer):
    """
    Query income per year based on customer details
    :param customer: customer  information
    :return: income per year
    """
    income = dict(
        error_code=0,
        error_text='',
        value=-1
    )
    try:
        client = MongoClient(conf.MongoDB.uri)
        db = client[conf.MongoDB.db]
        salaries = db[conf.MongoDB.salaries]
        query = {"$or": []}
        if customer.get('location'):
            location = customer.get('location')
            query.get("$or").extend([{"city": location, "slice": "Default"}])
        else:
            raise ValueError('No city found')

        if customer.get('seniority'):
            query.get("$or").extend([
                {"city": customer.get('location'), "slice": "Years Experience", "bucket": customer.get('seniority')}
            ])
        if customer.get('degree'):
            query.get("$or").extend([
                {"city": customer.get('location'), "slice": "Degree", "bucket": customer.get('degree')}
            ])

        if len(customer.get('skills')) > 0:
            skills = [
                {"city": customer.get('location'), "slice": "Skill", "bucket": x.get('Value')}
                for x in customer.get('skills')
            ]
            query.get("$or").extend(skills)
        print(query)
        result = list(salaries.find(query).sort([('avg_salary', -1)]).limit(1))
        if len(result) == 0:
            income['error_code'] = 1
            income['error_text'] = 'could not find income for combination: %s' % str(query)
            return income
        income['value'] = result[0].get('avg_salary')
        return income
    except Exception as e:
        income['error_code'] = -1
        income['error_text'] = 'ERROR getting info about salaries: %s' % str(e)
        return income


def get_expenses(customer):
    expenses = dict(
        error_code=0,
        error_text='',
        value=-1
    )
    try:
        client = MongoClient(conf.MongoDB.uri)
        db = client[conf.MongoDB.db]
        col = db[conf.MongoDB.col]
        query = {
            'City': customer.get('location'),
        }
        result = list(col.find(query).sort([('Cost of living', -1)]).limit(1))
        if len(result) == 0:
            expenses['error_code'] = 1
            expenses['error_text'] = 'could not find income: %s' % str(query)
            return expenses
        expenses['value'] = result[0].get('Cost of living')
        return expenses
    except Exception as e:
        expenses['error_code'] = -1
        expenses['error_text'] = 'ERROR getting info about expenses: %s' % str(e)
        return expenses


def get_net_income(customer):
    """
    calculate monthly net income
    :param customer: contains income per year and average expenses in a year for single person
    :return: net income: (income - expenses)/12
    """
    net_income = dict(
        error_code=0,
        error_text='',
        value=-1
    )
    try:
        income = get_income(customer)
        if income['error_code'] != 0:
            return income
        expenses = get_expenses(customer)
        if expenses['error_code'] != 0:
            return expenses
        net_income['value'] = (income['value'] - expenses['value'])/12
        if net_income['value'] < 0:
            net_income['error_code'] = 1
            net_income['error_text'] = 'Estimated expenses are greater than income'
        return net_income
    except Exception as e:
            net_income['error_code'] = 1
            net_income['error_text'] = 'Error calculating net_income: %s' % str(e)
            return net_income


def calculate_decision(score, loan_sum, num_of_month, net_income):
    decision = dict(
        error_code=0,
        error_text=''
    )
    try:
        client = MongoClient(conf.MongoDB.uri)
        db = client[conf.MongoDB.db]
        rules = db[conf.MongoDB.rules]
        mnth_rep = loan_sum / num_of_month
        irr = 'yes' if mnth_rep < net_income * 0.25 else 'no'
        query = {
                '$and': [
                        {'irr': irr},
                        {'min_score': {'$lte': score}},
                        {'max_score': {'$gt': score}},
                    ]
                }
        result = list(rules.find(query))
        if len(result) == 0:
            decision['error_code'] = 1
            decision['error_text'] = 'Could not find decision for parameters: %s' % str(query)
            return decision
        if len(result) > 1:
            decision['error_code'] = 2
            decision['error_text'] = 'More than one decision were found for rule: %s. Decisions: %s' % (
                str(query), str(result)
            )
            return decision
        rule_decision = result[0].get('decision')
        decision['text'] = rule_decision
        decision['approved_sum'] = 0
        if rule_decision == 'approve':
            decision['approved_sum'] = loan_sum
        elif rule_decision == 'partial':
            decision['approved_sum'] = loan_sum * score / 100
        return decision
    except Exception as e:
        decision['error_code'] = -1
        decision['error_text'] = 'ERROR getting decision: %s' % str(e)
        return decision


if __name__ == '__main__':
    customer = dict(
        location='Mumbai',
        degree='Master of Science (MS)',
        seniority='5-9 years',
        skills=[{"Value": "SQL"}, {"Value": "Project Management"}],
        loan_amt=100000,
        duration_mnth=36,
        text='super charging text to get a lot of money'
    )

    pprint.pprint(calculate_decision(65, 100000000, 25, get_net_income(customer)['value']))

    print(customer)
    #pprint.pprint(get_expenses(customer))

