class CustomDictionaries:
    dictionaries = dict(
        motion_words=['drive', 'go', 'run'],
        relative_words=['closer', 'higher', 'older'],
        social_words=['mother', 'father', 'he', 'she', 'we', 'they'],
        ego_words=['i', 'me', 'my', 'mine'],
        religious_body=[
            'mouth', 'rib', 'sweat', 'and', 'naked', 'adults', 'boy', 'and',
            'female', 'drive', 'go', 'run', 'able', 'accomplish', 'master'
        ]
    )

