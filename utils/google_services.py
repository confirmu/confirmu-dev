import os
import requests
import json
import base64
from conf import ConstValues

requests.packages.urllib3.disable_warnings()


def transcribe_file(speech_file, lang='en-US'):
    with open(speech_file, 'rb') as f:
        audio_content = base64.b64encode(f.read()).decode('UTF-8')

    data = {
        "config": {
            #"sample_rate": 48000,
            "encoding": "LINEAR16",
            "language_code": lang
        },
        "audio": {
            "content": audio_content
        }
    }

    headers = {"Content-Type": "application/json"}

    r = requests.post(
        'https://speech.googleapis.com/v1beta1/speech:syncrecognize?key=%s' % ConstValues.transcript_key,
        headers=headers,
        data=json.dumps(data),
        verify=False
    )
    return r.json()


def translate(text, to_lang='en'):
    data = {
        'q': text,
        'target': to_lang
    }

    headers = {"Content-Type": "application/json"}

    r = requests.post(
        'https://translation.googleapis.com/language/translate/v2?key=%s' % ConstValues.translate_key,
        headers=headers,
        data=json.dumps(data),
        verify=False
    )

    return r.json()


if __name__ == '__main__':
    source_text = transcribe_file('temp_audio/brooklyn.wav').get('results')[0].get('alternatives')[0].get('transcript')
    print(source_text)
    print(translate(source_text, to_lang='ru'))