import bcrypt


def check_password_hash(password, hash):
    return bcrypt.hashpw(password.encode(), hash.encode()) == hash.encode()


def check_password_strength(password):
    if len(password) > 5:
        return True
    else:
        return False

