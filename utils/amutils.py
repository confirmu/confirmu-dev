import random
import urllib.parse

import flask

from conf import PartnerQuestionsConf
from utils.mongo_operations import get_all_questions_to_shuffle


def make_shuffled_list(partner, free_text=0, slider=0, image=0, drawing=0, voice=0, select=0, excluded_image_attrs=[],
                       psychology=None, full_url_path=True):
    result = list()
    if psychology:  # questions must be based on given psychology
        result = make_shuffled_psychological_list(partner, psychology, full_url_path)
    else:
        questions = get_all_questions_to_shuffle(partner)
        if full_url_path:
            host_url = flask.request.host_url
            questions = rebuild_full_questions_url_path(questions, host_url)
        questions = exclude_image_attributes(questions,
                                             excluded_image_attrs)  # for now only score (it should be general)
        if free_text > 0 and questions.get('FreeText'):
            result.extend(random.sample(questions.get('FreeText'), min(len(questions.get('FreeText')), free_text)))
        if slider > 0 and questions.get('Slider'):
            result.extend(random.sample(questions.get('Slider'), min(len(questions.get('Slider')), slider)))
        if image > 0 and questions.get('Image'):
            result.extend(random.sample(questions.get('Image'), min(len(questions.get('Image')), image)))
        if drawing > 0 and questions.get('Drawing'):
            result.extend(random.sample(questions.get('Drawing'), min(len(questions.get('Drawing')), drawing)))
        if voice > 0 and questions.get('Voice'):
            result.extend(random.sample(questions.get('Voice'), min(len(questions.get('Voice')), voice)))
        if select > 0 and questions.get('Select'):
            result.extend(random.sample(questions.get('Select'), min(len(questions.get('Select')), select)))
    return result


def make_shuffled_psychological_list(partner, psychology, full_url_path):
    if psychology.get('All', None) is not None:
        if psychology['All']:  # In case all questions are required
            return get_all_partner_questions(partner, excluded_image_attrs=['score'], full_url_path=full_url_path)
        elif len(psychology) == 1:
            return list()
        else:
            del psychology['All']

    questions = get_all_questions_to_shuffle(partner)
    # we don't sinking boat questions to be retrieved now
    if questions.get('Decision') is not None:
        questions['Decision'] = [q for q in questions.get('Decision') if not q.get('QuestionCode') == "100"]
    questions_types = ['FreeText', 'Slider', 'Image', 'Drawing', 'Voice', 'Select', 'Cart', "Decision",
                       "CartOrDecision"]
    result = list()
    for psychology_type in psychology:
        for Q_T in questions_types:
            if psychology.get(psychology_type).get(Q_T) or 0 > 0 and questions.get(Q_T):
                if Q_T == "CartOrDecision":
                    Q_T = random.choice(['Cart', 'Decision'])
                    psychology_questions = psychology.get(psychology_type)
                    psychology_questions[Q_T] = psychology_questions.get('CartOrDecision')
                    del psychology_questions['CartOrDecision']
                filtered_questions = list(
                    filter(lambda x: psychology_type in x['QuestionPsychology'], questions.get(Q_T) or []))
                random_filtered_questions = random.sample(filtered_questions, min(len(filtered_questions),
                                                                                  psychology.get(psychology_type).get(
                                                                                      Q_T)))
                if len(random_filtered_questions) > 0:
                    # Remove Psychology from response
                    for q in random_filtered_questions:
                        del q['QuestionPsychology']
                    # Because we don't need duplicate/replicated questions
                    questions[Q_T] = [ele for ele in questions.get(Q_T) if ele not in random_filtered_questions]
                result.extend(random_filtered_questions)

    return result


def get_all_partner_questions(partner, excluded_image_attrs=[], full_url_path=True):
    questions = get_all_questions_to_shuffle(partner)
    # Partner has no questions
    if len(questions) == 0:
        return questions

    if full_url_path:
        host_url = flask.request.host_url
        questions = rebuild_full_questions_url_path(questions, host_url)
    result = list()
    questions = exclude_image_attributes(questions, excluded_image_attrs)
    for item in questions:
        result.extend(questions[item])
    return result


def get_default_partner_questions(partner, excluded_image_attrs=[], full_url_path=True):
    """
    The Default questions are 5 select images (3 of type cons. psychology and 2 other at random).
    and 2 other type of questions (dice, cart or island) all of them at random.
    """
    # in case partner is bridge2capital then default questions conf
    if partner in ["bridge2capital", "b2cap_hindi"]:
        if partner == "bridge2capital":
            questions_conf = PartnerQuestionsConf.bridge2capital_questions
        else:
            questions_conf = PartnerQuestionsConf.b2cap_hindi_questions
        return make_shuffled_list(partner, free_text=questions_conf.get("free_text"),
                                  slider=questions_conf.get("slider"),
                                  image=questions_conf.get("image"), drawing=questions_conf.get("drawing"),
                                  voice=questions_conf.get("voice"), select=questions_conf.get("select"),
                                  excluded_image_attrs=excluded_image_attrs, full_url_path=full_url_path)

    questions = get_all_questions_to_shuffle(partner)
    # Partner has no questions
    if len(questions) == 0:
        return questions
    result = list()
    questions = exclude_image_attributes(questions, excluded_image_attrs)
    # TODO : Remove this filtering in the future.
    if questions.get('Decision') is not None:
        questions['Decision'] = [q for q in questions.get('Decision') if not q.get('QuestionCode') == "100"]
    if full_url_path:
        host_url = flask.request.host_url
        questions = rebuild_full_questions_url_path(questions, host_url)
    # 5 images
    result = make_shuffled_list(partner, image=5, full_url_path=full_url_path)
    # 2 other questions (dice, cart or island)
    questions_options = ['Select', 'Cart', 'Decision']
    randomized_options = random.sample(questions_options, 2)
    for option in randomized_options:
        specific_questions = questions.get(option)
        if specific_questions is not None:
            one_sample = random.sample(specific_questions, 1)
            result.extend(one_sample)
    return result


def exclude_image_attributes(dic, exclude=[]):
    for exclude_item in exclude:  # Exclude given attributes from questions
        for image_question in dic.get('Image'):
            images = image_question.get('Images')
            for image in images:
                # del image[exclude_item]
                image[exclude_item] = 0
            image_question['Images'] = images
    return dic


def rebuild_full_questions_url_path(questions, host_url):
    images = questions.get('Image')
    carts = questions.get('Cart')
    host_url = to_secure_url(host_url)
    if images is not None:
        for img in images:
            imgs = img['Images']
            for im in imgs:
                im['url'] = urllib.parse.urljoin(host_url, im['url'])
    if carts is not None:
        for cart in carts:
            options = cart['Options']
            for opt in options:
                opt['url'] = urllib.parse.urljoin(host_url, opt['url'])
    return questions


def flatten_list(grouped_list):
    result = []
    for group in grouped_list:
        result.extend(grouped_list.get(group))
    return result


def rebuild_scenario_answers_types(scenario_answers):
    one_cart = []
    for ans in scenario_answers:
        if ans.get('answer_type') == 'Image':
            ans['answer_id'] = "Image" + ans.get('answer_id')
        elif ans.get('answer_type') == 'Dice':
            ans['answer_type'] = 'Select'
        elif ans.get('answer_type') == 'Cart':
            one_cart.append(ans)
    if len(one_cart) > 0:
        scenario_answers = list(filter(lambda ans: ans.get('answer_type') != 'Cart', scenario_answers))
        scenario_answers.append(one_cart[0])
    return scenario_answers


def get_scenario_questions(excluded_image_attrs=[], full_url_path=True):
    """
    returns questions associated with scenario scoring logic answers, based on random scenario_id.
    """
    result = list()
    # TODO : use senario_scores form constants.py file
    scenario_scores = {"scenarios":[{"scenario_id":1,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1","Dice2"],"answer":"50$","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Eggs","value":0.03},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Red pepers","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Cheese","value":0.025},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Nature Vally","value":0.02},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bread","value":0.01},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bananas","value":0.035},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Oak mill","value":0.005},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Mousse","value":-0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Coco pops","value":-0.042},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bagel","value":-0.066},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Muffin","value":-0.0725}]}],"scenario_answers":[{"answer_id":"18","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.624229979}]},{"answer_id":"21","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.611111111}]},{"answer_id":"35","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.605108055}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.629716981}]},{"answer_id":"Dice2","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.620952381}]},{"answer_id":"Cart1","answer_type":"Cart","answers_probabilities":[{"answer":"XXXX","prob":0}]}],"scenario_alternative":"35","scenario_others":0,"max_score":None},{"scenario_id":2,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1","Dice2"],"answer":"50$","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Eggs","value":0.03},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Red pepers","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Cheese","value":0.025},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Nature Vally","value":0.02},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bread","value":0.01},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bananas","value":0.035},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Oak mill","value":0.005},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Mousse","value":-0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Coco pops","value":-0.042},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bagel","value":-0.066},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Muffin","value":-0.0725}]}],"scenario_answers":[{"answer_id":"18","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.624229979}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.629716981}]},{"answer_id":"Dice2","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.620952381}]},{"answer_id":"Cart1","answer_type":"Cart","answers_probabilities":[{"answer":"XXXX","prob":0}]}],"scenario_alternative":"18","scenario_others":2,"max_score":None},{"scenario_id":3,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1"],"answer":"50$","value":0.05}]}],"scenario_answers":[{"answer_id":"21","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.611111111}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.629716981}]},{"answer_id":"Decision1","answer_type":"Island","answers_probabilities":[{"answer":"A 35 year old sailor","prob":0.08},{"answer":"A heroic fighter","prob":0.05},{"answer":"A 26 year old college","prob":0.05},{"answer":"A rich charity worker","prob":0.025},{"answer":"blind ten years old","prob":0.0225},{"answer":"blind ten years old","prob":0.02},{"answer":"pregnant women","prob":0.01},{"answer":"2 year old","prob":0.01}]}],"scenario_alternative":"21","scenario_others":2,"max_score":81.61070231},{"scenario_id":4,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1"],"answer":"50$","value":0.05},{"answer_type":"Select","answers_id":["Dice2"],"answer":"100$","value":0.025}]}],"scenario_answers":[{"answer_id":"35","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.605108055}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.629716981132075}]},{"answer_id":"Dice2","answer_type":"Dice","answers_probabilities":[{"answer":"100$","prob":0.62095238095238}]},{"answer_id":"Dice4","answer_type":"Dice","answers_probabilities":[{"answer":"100$","prob":0.6088235294117647}]},{"answer_id":"Decision1","answer_type":"Island","answers_probabilities":[{"answer":"A 35 year old sailor","prob":0.017},{"answer":"A heroic fighter","prob":0.0151},{"answer":"A 26 year old college","prob":0.0121},{"answer":"A rich charity worker","prob":0.0101},{"answer":"blind ten years old","prob":0.0042},{"answer":"blind ten years old","prob":0.0091},{"answer":"pregnant women","prob":0.0043},{"answer":"2 year old","prob":0.0021}]}],"scenario_alternative":"Dice4","scenario_others":2,"max_score":97.85502366},{"scenario_id":5,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1","Dice2","Dice3"],"answer":"50$","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Eggs","value":0.03},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Red pepers","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Cheese","value":0.025},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Nature Vally","value":0.02},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bread","value":0.01},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bananas","value":0.035},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Oak mill","value":0.005},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Mousse","value":-0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Coco pops","value":-0.042},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bagel","value":-0.066},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Muffin","value":-0.0725}]}],"scenario_answers":[{"answer_id":"18","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.6242299794661191}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.608156028368794}]},{"answer_id":"Dice2","answer_type":"Dice","answers_probabilities":[{"answer":"100$","prob":0.608823529411764}]},{"answer_id":"Dice3","answer_type":"Dice","answers_probabilities":[{"answer":"100$","prob":0.608156028368794}]},{"answer_id":"Cart1","answer_type":"Cart","answers_probabilities":[{"answer":"XXXX","prob":0}]}],"scenario_alternative":"18","scenario_others":1,"max_score":None},{"scenario_id":6,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1","Dice2","Dice3"],"answer":"50$","value":0.05},{"answer_type":"Select","answers_id":["Dice4"],"answer":"100$","value":0.025}]}],"scenario_answers":[{"answer_id":"18","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.624229979466119}]},{"answer_id":"35","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.605108055009823}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.629716981132075}]},{"answer_id":"Dice2","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.62095238095238}]},{"answer_id":"Dice3","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.608156028368794}]},{"answer_id":"Dice4","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.608823529411764}]},{"answer_id":"Decision1","answer_type":"Island","answers_probabilities":[{"answer":"A 35 year old sailor","prob":0.0475},{"answer":"A heroic fighter","prob":0.0225},{"answer":"A 26 year old college","prob":0.0111},{"answer":"A rich charity worker","prob":0.025},{"answer":"blind ten years old","prob":0.0225},{"answer":"blind ten years old","prob":0.02},{"answer":"pregnant women","prob":0.01},{"answer":"2 year old","prob":0.01}]}],"scenario_alternative":"35","scenario_others":0,"max_score":96.1702761},{"scenario_id":7,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."}],"scenario_answers":[{"answer_id":"35","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.605108055}]}],"scenario_alternative":"35","scenario_others":6,"max_score":99.99}],"any_other_pics_points":{"1":4.37,"2":7.11,"3":9.42,"4":1.21,"5":3.67,"6":4.41,"8":8.11,"9":2.87,"10":4.57,"11":8.87,"12":6.23,"13":5.09,"14":3.39,"15":0.77,"16":9.01,"17":9.21,"18":2.64,"19":1.17,"20":0.39,"21":8.01,"22":2.27,"23":6.51,"24":4.91,"25":5.97,"26":3.03,"27":8.04,"28":5.16,"29":3.77,"30":2.34,"31":3.55,"32":4.11,"33":4.86,"34":9.18,"35":6.34,"36":2.17,"37":3.38,"38":9.34}}
    any_other_pics = scenario_scores.get('any_other_pics_points')
    scenario_id = random.randint(1, len(scenario_scores.get('scenarios')))
    scenario = [sc for sc in scenario_scores.get('scenarios') if sc.get('scenario_id') == scenario_id]
    scenario = scenario[0] if len(scenario) > 0 else None
    if scenario is None:
        return result

    scenario_answers = scenario.get('scenario_answers')
    scenario_answers = rebuild_scenario_answers_types(scenario_answers)
    # TODO : remove partner dizzy and create a centralized shared repo for scenario questions.
    all_questions = get_all_questions_to_shuffle('dizzy')

    if full_url_path:
        host_url = flask.request.host_url
        all_questions = rebuild_full_questions_url_path(all_questions, host_url)

    all_questions = exclude_image_attributes(all_questions, excluded_image_attrs)

    # flattening questions
    all_questions = flatten_list(all_questions)
    for scenario_question in scenario_answers:
        scenario_answer_id = scenario_question.get('answer_id')
        scenario_answer_type = scenario_question.get('answer_type')
        matched_questions = list(filter(lambda q: q.get('QuestionId') == scenario_answer_id and
                                                  q.get('QuestionType') == scenario_answer_type, all_questions))
        result.extend(matched_questions)

    # if scenario has extra any other image questions
    if scenario.get('scenario_others') is not None and scenario.get('scenario_others') != 0:
        random_pics_ids = random.sample(list(any_other_pics.keys()), scenario.get('scenario_others'))
        # pic 18 21 35 are already in questions, so we don't to have duplicate questions
        random_pics_ids = [pic_id for pic_id in random_pics_ids if pic_id not in ['18', '21', '35']]
        matched_extra_questions = list(
            filter(lambda q: q.get('QuestionId').replace('Image', '') in random_pics_ids, all_questions))
        result.extend(matched_extra_questions)
    return result, scenario_id


def to_secure_url(url):
    if "http" in url:
        return str(url).replace("http", "https")
    return url


if __name__ == '__main__':
    a = make_shuffled_list('mobileapp')