from collections import defaultdict


def image_randomization_algorithm(questions_data):
    '''
    calculate the answers score in order to apply questions randomize algorithm
    :param questions_data: a dictionary that contains questions, expected answers, psychology
    :return: dictionary of answers scores for each psychology
    '''
    pic_results = {}
    groups = defaultdict(list)

    for obj in questions_data:
        groups[obj['QuestionPsychology']].append(obj)
    grouped_answers = groups.values()  # answers grouped by psychology

    for psych_group in grouped_answers:
        pct = 0
        is_first_question = True
        for Q_A in psych_group:
            if Q_A['QuestionAnswer'] in Q_A['ExpectedAnswers'] and is_first_question:
                pct = 1
                is_first_question = False
            elif Q_A['QuestionAnswer'] in Q_A['ExpectedAnswers']:
                pct = (pct + 1) / 2
                is_first_question = False
            else:
                pct = pct / 2
                is_first_question = False
        pic_results[Q_A['QuestionPsychology']] = pct
    return pic_results
