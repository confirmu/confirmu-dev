import os
from O365 import Account
from utils.mongo_operations import get_partner_email_notifications


def send_notification(subject, to_email, text, cc_email=None):

    if not (to_email and subject and text):
        return False

    credentials = (os.environ.get('EMAIL_APP_ID'), os.environ.get('EMAIL_APP_SECRET'))
    account = Account(credentials, auth_flow_type='credentials', tenant_id=os.environ.get('EMAIL_APP_TENANT_ID'))

    if account.authenticate():
        mailbox = account.mailbox('admin@confirmu.com')
        m = mailbox.new_message()

        m.to.add(to_email)
        if cc_email:
            m.cc.add(cc_email)
        m.subject = subject
        m.body = text
        is_success = m.send()
        return is_success
    else:
        return False


def send_partner_notification(partner, subject, text):
    email_settings = get_partner_email_notifications(partner)

    if email_settings.get('cc'):
        cc = email_settings.get('cc').split(',')
    else:
        cc = None
    if send_notification(subject, email_settings.get('to'), text, cc):
        return True
    else:
        return False


if __name__ == "__main__":
    send_notification(
        "Confirmu Scores: test@test.com",
        "antmanin@gmail.com",
        '<p>New client: test@test.com</p><p>Final scoer is: 85</p>'
    )
