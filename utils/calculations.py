import json
import logging
from os import environ
from statistics import mean
from flask import url_for
from scipy.stats import norm
import requests
from math import exp
import numpy as np

from conf import ConstValues, ImageScores
from conf import PartnerConfigurations as Config
from sentiment_analyzer import get_sentiment_score
from utils.frequency_analyzers import freq_analyzer
from utils.gibberishclassifier import classify
from utils.mongo_operations import save_to_confifrmu, get_all_questions_to_shuffle, get_score_factors
from constants import ScoringSchemas


def calculate_final_score(scores, patterns):
    if scores is None:
        return 0
    return int(mean([exp(float(scores[x])) for x in scores if x in patterns]) / exp(1) * 100)


def calculate_weighted_final_score(partner, ta_score, sent_score, images_score, min_val=0, max_val=100):
    TEXT_ANALYSER_SCORE, SENTIMENT_SCORE, IMAGE_SCORE = get_score_factors(partner)
    final_score = min(
        max(ta_score * TEXT_ANALYSER_SCORE + sent_score * SENTIMENT_SCORE + images_score * IMAGE_SCORE, min_val),
        max_val)
    return final_score


def get_patterns(partner=None):
    if Config.config.get(partner) is not None and Config.config.get(partner).get('patterns') is not None:
        patterns = Config.config.get(partner).get('patterns')
    else:
        patterns = ConstValues.patterns
    return patterns


def get_scores(chat_text, gibber_check=True):
    s = requests.session()
    result = dict(error_code=0, error_message='', final_score=0.)

    # gibber check
    if gibber_check:
        if classify(chat_text) > 50:
            result['error_code'] = 1
            result['error_message'] = 'unable to get scores for text: %s' % chat_text
            return result

    data = chat_text.encode("utf-8")
    headers = {
        "Content-Type": "text/plain",
        "Cache-Control": "no-cache",
        "applicationToken": "056f16c3-6f7f-4d1d-b604-e63a9456e1ed"
    }

    print()
    response = s.post(ConstValues.text_analyzer, data=data, headers=headers)
    if response.status_code != 200:
        result['error_code'] = 1
        result['error_message'] = 'unable to get scores for text: %s' % chat_text
        return result

    scores = json.loads(response.text).get('response')
    if type(scores) is str:
        result['error_code'] = 1
        result['error_message'] = scores
    else:
        # reduce scores if text is gibber
        if gibber_check is False:
            if classify(chat_text) > 50:
                scores = {k: round(v * 0.6, 4) for k, v in scores.items()}

        result['raw_scores'] = scores
        result['final_score'] = sum([float(scores[x]) for x in scores if x != 'top'])
        try:
            result['top_score_sum'] = sum([float(x[1]) for x in scores['top']])
        except:
            result['top_score_sum'] = -1
    return result


def define_message(score):
    if score >= 80:
        return dict(
            message="Accept",
            decision="approved",
        )
    elif score >= 55:
        return dict(
            message="Recommend to accept but with conditions",
            decision="approved",
        )
    else:
        return dict(
            message="Recommend to reject applicant",
            decision="rejected",
        )


def define_message_us(score):
    if score >= 800:
        return dict(
            message="Exceptional",
            decision="approved",
        )
    elif score >= 740:
        return dict(
            message="Very Good",
            decision="approved",
        )
    elif score >= 670:
        return dict(
            message="Good",
            decision="approved",
        )
    elif score >= 580:
        return dict(
            message="Fair",
            decision="approved",
        )
    else:
        return dict(
            message="Very poor",
            decision="rejected",
        )


def define_message_us_old(score):
    if score >= 800:
        return dict(
            message="Congrats you're really doing great your loan is fully approved",
            decision="approved",
        )
    elif score >= 740:
        return dict(
            message="Great news we approved your loan request you'll be contacted soon",
            decision="approved",
        )
    elif score >= 670:
        return dict(
            message="Our analysis shows your entitled for the loan but under some conditions",
            decision="approved",
        )
    elif score >= 580:
        return dict(
            message="We found that under certain condition your entitled to loan ",
            decision="approved",
        )
    else:
        return dict(
            message="Unfortunately we found that your financial health will not allow you receive a loan",
            decision="rejected",
        )


def get_letter_score(score):
    if score < 60:
        return 'C'
    elif score < 80:
        return 'B'
    else:
        return 'A'


def get_sms_income_ratio(sms_data):
    debit = sum([int(x['Amount']) for x in sms_data if x['TrxType'] == "+"])
    credit = sum([int(x['Amount']) for x in sms_data if x['TrxType'] == "-"])
    return debit / credit


def get_images_scores(questions, partner=None):
    if partner == 'mobileapp':
        try:
            select_answers = {
                x['General'].get('QuestionId'): x['SelectionQuestion'].get('FinalSelection')
                for x in questions if x['General'].get('QuestionType') == 'Selection'
            }
            images_score = 65
            for x in select_answers:
                if ImageScores.mobileapp.get(str(x)):
                    images_score += ImageScores.mobileapp.get(str(x)).get(select_answers[x])
        except Exception as e:
            logging.error('No images scores available: {}'.format(e))
            images_score = 0

    elif partner != 'mobbisurance':
        try:
            select_answers = {
                x['SelectionQuestion'].get('SelectionQuestionCode'): x['SelectionQuestion'].get('FinalSelection')
                for x in questions if x['General'].get('QuestionType') == 'Selection'
            }
            images_score = 65
            max_image_score = images_score
            scores = get_select_questions_scores(partner)
            for x in select_answers:
                if scores.get(str(x)):
                    images_score += int(scores.get(str(x)).get(select_answers[x]))
                    max_image_score += int(max(scores.get(str(x)).values()))
            images_score = int(images_score / (max_image_score / 100))

        except Exception as e:
            logging.error('No images scores available: {}'.format(e))
            images_score = 0
    else:
        try:
            select_answers = {
                x['SelectionQuestion'].get('SelectionQuestionCode'): x['SelectionQuestion'].get('FinalSelection')
                for x in questions if x['General'].get('QuestionType') == 'Selection'
            }
            images_score = 65
            for x in select_answers:
                if ImageScores.mobbisurance.get(str(x)):
                    images_score += ImageScores.mobbisurance.get(str(x)).get(select_answers[x])
        except Exception as e:
            logging.error('No images scores available: {}'.format(e))
            images_score = 0
    return images_score


def calculate_mobile_app_score(partner, req_data):
    patterns = get_patterns('partner')
    result = dict()

    customer_key = req_data.get('customer_key')
    result['partner'] = partner
    result['customer_key'] = str(customer_key)
    result['Questionaire'] = req_data

    # get scores from ML algorithm
    try:
        en_options = []
        questions = req_data.get('Questionaire').get('Questions')
        for x in questions:
            if x.get('General').get('QuestionType') in ['FreeText']:
                en_options.append(x.get('FreeTextQuestion').get('FreeTextAnswer'))
        text_to_analyze = ' '.join(en_options)
        images_score = get_images_scores(questions, partner)
        sent_score = get_sentiment_score(en_options)

        scores_result = get_scores(text_to_analyze, False)

        if scores_result.get('error_code') == 0:
            scores = scores_result.get('raw_scores')
            result['scores'] = scores
            response_code = 201
            final_score = 0

            if len(patterns) > 0:
                ta_score = calculate_final_score(scores, patterns)
            else:
                ta_score = calculate_final_score(scores, ConstValues.patterns)

            final_score = min(max(ta_score * 0.5 + sent_score * 0.2 + images_score * 0.3, 0), 100) * 9.5
            result['Score details'] = "{} >>> text analyzer {}, sentiment analysis: {}, images: {}.".format(
                final_score, ta_score, sent_score, images_score
            )
            try:
                result['freq_analyzer'] = freq_analyzer(text_to_analyze)
            except Exception as e:
                logging.error(e)
            msg = define_message_us_old(final_score)

            result['response'] = {
                "Code": 0,
                "IsSuccess": True,
                "Key": customer_key,  # customer number
                "Message": msg.get('message'),
                "Number": final_score / 9.5 if partner == "epaysa" else final_score
            }
            if partner == 'iloan':
                result['response']['LetterScore'] = get_letter_score(final_score / 9.5)
            if msg.get('decision'):
                result['response']['Decision'] = msg.get('decision')
                if partner == 'mobileapp' and msg.get('decision') == 'approved':
                    result['response']['Code'] = 1

        else:
            result['response'] = {
                "Code": scores_result.get('error_code'),
                "IsSuccess": False,
                "Key": customer_key,  # customer number
                "Message": scores_result.get('error_message'),
                "Number": -1
            }
            response_code = 400
    except Exception as e:
        result['response'] = {
            "Code": -1,
            "IsSuccess": False,
            "Key": customer_key,  # customer number
            "Message": "Score calculation failed: %s" % str(e),
            "Number": -1
        }
        response_code = 400
    return result, response_code


def cvt_score_to(final_score, grade=False, color=False, percentage=False, meaning=False):
    grades = ['A', 'B', 'C', 'D', 'E']
    colors = ['Green', 'Blue', 'Brown', 'Yellow', 'Red']
    percentages = [1.0, 0.90, 0.80, 0.60, 0]
    meanings = ['Exceptional', 'Very Good', 'Good', 'Fair', 'Very poor']

    # decide which metric to convert
    if grade:
        metric = grades
    elif color:
        metric = colors
    elif percentage:
        metric = percentages
    elif meaning:
        metric = meanings

    if 300 <= final_score < 580:
        return metric[len(metric) - 1]
    elif 580 <= final_score < 670:
        return metric[len(metric) - 2]
    elif 670 <= final_score < 739:
        return metric[len(metric) - 3]
    elif 740 <= final_score < 799:
        return metric[len(metric) - 4]
    elif 800 <= final_score < 850:
        return metric[len(metric) - 5]
    else:
        return 0


def calculate_form_scores(partner, request, email=None):
    r = request.json
    questions = r['Questionaire'].get('Questions')
    if email is None or email == 'None':
        email = r['Customer'].get('EmailAddress')
    text_answers = [
        x['FreeTextQuestion'].get('FreeTextAnswer') for x in questions if x['General'].get('QuestionType') == 'FreeText'
    ]
    voice_files = [
        x['VoiceQuestion'].get('VoiceQuestionFile') for x in questions if x['General'].get('QuestionType') == 'Voice'
    ]
    if len(voice_files) > 0:
        voice_scores = get_namesyco_scores(voice_files, request.url_root)
        logging.info('Voice scores:\n{}'.format(str(voice_scores)))
    else:
        voice_scores = None

    joined_text_answers = '. '.join(text_answers)
    images_score = get_images_scores(questions, partner)
    ta = get_scores(joined_text_answers)
    ta_score = min(calculate_final_score(ta.get('raw_scores'), ConstValues.patterns) * 1.25, 100)
    ta_error = ta.get('error_message')
    sent_score = get_sentiment_score(text_answers)
    final_score = int(min(max(ta_score * 0.5 + sent_score * 0.2 + images_score * 0.3, 0), 100) * 8.5)
    meaning = define_message_us(final_score)
    requested_amount = float(r['Loan'].get('TotalAmount'))
    score_percentage = cvt_score_to(final_score, percentage=True)
    qualified_amount = requested_amount * score_percentage
    result = {
        "email": email,
        "final_score": final_score,
        "message": meaning.get('message'),
        "decision": meaning.get('decision'),
        "ta_score": ta_score,
        "ta_error": ta_error,
        "sent_score": sent_score,
        "images_score": images_score,
        "requested_amount": requested_amount,
        "qualified_amount": qualified_amount,
        "request": r
    }
    if voice_scores:
        result['voice_scores'] = voice_scores

    mongo_id = save_to_confifrmu(partner, result)

    if partner == 'myfugo':
        try:
            result['myfugo_response'] = send_result_to_myfugo(
                dict(
                    requestCode=mongo_id,
                    requestedAmount=str(requested_amount),
                    qualifiedAmount=str(qualified_amount),
                    emailAdress=email,
                    creditScore=str(final_score),
                    recommendation=meaning.get('message')
                )
            )
        except Exception as e:
            logging.error('Error while sending results to myfugo: {}'.format(e))

    logging.debug(result)
    logging.debug("Email: {} >> Psycholinguistic: {}; Sentiment : {};  Images: {}; Final: {}".format(
        email, ta_score, sent_score, images_score, final_score
    ))
    return "We are going to contact you soon<br>Have a nice day!"


def get_select_questions_scores(partner):
    result = dict()
    qs = get_all_questions_to_shuffle(partner)
    try:
        for category in qs:
            if category == 'Image':
                for q in qs[category]:
                    scores = {}
                    for image in q['Images']:
                        scores[image.get('image_id')] = image.get('score')
                    result[q.get('QuestionCode')] = scores
            elif category == 'Select':
                for q in qs[category]:
                    scores = {}
                    for option in q['Options']:
                        scores[option.get('value')] = option.get('score')
                    result[q.get('QuestionCode')] = scores
    except Exception as e:
        logging.error('Can\'t calculate selection questions score: {}'.format(e))
    return result


def send_result_to_myfugo(data):
    url = '{}/confirmu/credit_score'.format(environ.get('MYFUGO_URL'))
    headers = {"Content-Type": "application/json"}
    r = requests.post(url, data=json.dumps(data), headers=headers)
    logging.debug('Result sent to myfugo. Response code: {}'.format(r.status_code))
    logging.debug('Response: {}'.format(r.text))
    return r.status_code


def get_namesyco_scores(records, url_root):
    result = []
    for x in records:
        audio_url = '{}/temp_audio?file={}'.format(url_root, x)
        headers = {
            "N-MS-AUTHCB": "0785236B-DF26-4D58-918E-EABB010AD287",
            "Content-Type": "application/json"
        }
        data = {
            "MarkFileName": "NEWTEST_WITH_WEB_EXPORTTEXT",
            "ServiceFlag": "SEG,EDP,RISKREPT",
            "TestData": [
                {
                    "Power": 30,
                    "Question": "This is question 1",
                    "Reference": "Test",
                    "Topic": "This is topic 1",
                    "URL": audio_url
                }

            ],
            "TestName": "ConfirmU",
            "Testlng": "en"
        }
        r = requests.post(
            'https://rservice.nemesysco.net/RadvAnalysis/',
            data=json.dumps(data),
            headers=headers
        )
        try:
            result.append(int(r.json().get('Data').get('RISKREPT')[0].split(';')[-2]))
        except Exception as e:
            logging.error('Bad response from Nemesysco: {}\n{}'.format(e, r.json()))
    if len(result) > 0:
        return int(mean(result))
    else:
        return 0


def get_result_from_answers(answers, scoring, result, partner, psyc_filter):
    all_questions = get_all_questions_to_shuffle(partner)
    for question_type in answers:

        questions_answers = answers[question_type]
        for ans in questions_answers:
            q_type = 'Select' if question_type.title() == 'Dice' else question_type.title()
            matched_answer = list(filter(lambda q: ans.get('QuestionId') == q.get('QuestionId'), all_questions[q_type]))
            # In case there is a matched answer otherwise go to next ans
            if len(matched_answer) > 0:
                matched_answer = matched_answer[0]
            else:
                continue
            ans['QuestionPsych'] = matched_answer.get('QuestionPsychology')
            if psyc_filter is None:
                matched_psychologies = list(filter(lambda a: a in ans['QuestionPsych'], scoring))
            else:
                matched_psychologies = list(filter(lambda a: a in ans['QuestionPsych'] and a in psyc_filter, scoring))
                matched_psychologies = list(filter(lambda a: a in psyc_filter, matched_psychologies))
            for psych in matched_psychologies:
                high = scoring[psych].get('high').get(question_type)
                low = scoring[psych].get('low').get(question_type)
                question_id = str(ans['QuestionId']).replace('Image', '') if question_type == 'image' else ans[
                    'QuestionId']
                if question_type == 'cart':
                    if question_id in high:
                        result[psych].update({'high': result.get(psych).get('high') + high[question_id]})
                    if question_id in low:
                        result[psych].update({'low': result.get(psych).get('low') + low[question_id]})
                else:  # Image or Select Question
                    if question_id in high and high[question_id] == ans['QuestionAnswer']:
                        result[psych].update({'high': result.get(psych).get('high') + 1})
                    if question_id in low and not low[question_id] == ans['QuestionAnswer']:
                        result[psych].update({' low': result.get(psych).get('low') + 1})
                result[psych].update({'n': result.get(psych).get('n') + 1})
    return result


def calculate_percentile(correct_answers, total_answers):
    """calculates the fraction score for a given trait values"""
    return correct_answers / total_answers


def calculate_trait_score(weight, percentile):
    """calculates the final score for a given trait using normal distribution and standard deviation"""
    if percentile == 0:
        return 0
    elif percentile >= 1:
        percentile = 0.99
    return (weight / 2) + norm.ppf(percentile) * (weight / 6)


def calculate_dice_points(answers):
    # In case no dice answers are present
    if answers.get('dice') is None:
        return 0
    total_pts = 0
    dice_answers = answers.get('dice')

    dice_pts = dict({
        "50$": 5,
        "100$": 3.75,
        "150$": 2.75,
        "200$": 1.75,
    })

    for answer in dice_answers:
        answer_value = answer.get('QuestionAnswer')
        total_pts = total_pts + dice_pts.get(answer_value) or 0
    # since dice pts represents 40% of the final score and the max value is 5 pts, then 8 dice questions are needed
    total_pts = total_pts * (8 / len(dice_answers)) if len(dice_answers) > 0 else 0
    return total_pts


def calculate_psychology_score(answers, partner):
    scoring = ScoringSchemas.psychology_scores
    result = dict()
    weights = dict()
    final_score = 0
    # create list of all psychologies for high and low
    for psych in scoring:
        # TODO : remove static weight values
        weights[psych] = 60 if psych == 'Concientiousness' else 20
        result[psych] = {'high': 0, 'low': 0, 'n': 0}

    result = get_result_from_answers(answers, scoring, result, partner, None)
    dice_pts = calculate_dice_points(answers)
    # scoring for each trait
    for psych in result:
        if float(result[psych].get('n')) > 0:
            percentile = calculate_percentile(result[psych].get('high') + result[psych].get('low'),
                                              result[psych].get('n'))
            score = calculate_trait_score(weights[psych], percentile)
            result[psych].update({'score': score})

    result['final_score'] = (result['Concientiousness'].get('score') or 0) * 0.60 + dice_pts
    recommendation_message = cvt_score_to_recommendation(result.get('final_score') / 100)
    result['recommendation'] = recommendation_message
    return result


def flatten_answers_list(answers):
    result = []
    for answer_type in answers:
        result.extend(answers.get(answer_type))
    return result


def cvt_score_to_recommendation(score):
    if score < 0.60:
        return "Recommend To Reject"
    elif 0.60 < score < 0.80:
        return "Accept But With Conditions"
    elif 0.80 < score < 1.00:
        return "Fully Accept"
    else:
        return "Score Out Of Range"


def calculate_scenario_score(answers, scenario_id):
    # TODO : use senario_scores form constants.py file
    scenario_scores = {"scenarios":[{"scenario_id":1,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1","Dice2"],"answer":"50$","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Eggs","value":0.03},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Red pepers","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Cheese","value":0.025},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Nature Vally","value":0.02},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bread","value":0.01},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bananas","value":0.035},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Oak mill","value":0.005},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Mousse","value":-0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Coco pops","value":-0.042},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bagel","value":-0.066},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Muffin","value":-0.0725}]}],"scenario_answers":[{"answer_id":"18","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.624229979}]},{"answer_id":"21","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.611111111}]},{"answer_id":"35","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.605108055}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.629716981}]},{"answer_id":"Dice2","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.620952381}]},{"answer_id":"Cart1","answer_type":"Cart","answers_probabilities":[{"answer":"XXXX","prob":0}]}],"scenario_alternative":"35","scenario_others":0,"max_score":None},{"scenario_id":2,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1","Dice2"],"answer":"50$","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Eggs","value":0.03},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Red pepers","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Cheese","value":0.025},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Nature Vally","value":0.02},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bread","value":0.01},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bananas","value":0.035},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Oak mill","value":0.005},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Mousse","value":-0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Coco pops","value":-0.042},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bagel","value":-0.066},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Muffin","value":-0.0725}]}],"scenario_answers":[{"answer_id":"18","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.624229979}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.629716981}]},{"answer_id":"Dice2","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.620952381}]},{"answer_id":"Cart1","answer_type":"Cart","answers_probabilities":[{"answer":"XXXX","prob":0}]}],"scenario_alternative":"18","scenario_others":2,"max_score":None},{"scenario_id":3,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1"],"answer":"50$","value":0.05}]}],"scenario_answers":[{"answer_id":"21","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.611111111}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.629716981}]},{"answer_id":"Decision1","answer_type":"Island","answers_probabilities":[{"answer":"A 35 year old sailor","prob":0.08},{"answer":"A heroic fighter","prob":0.05},{"answer":"A 26 year old college","prob":0.05},{"answer":"A rich charity worker","prob":0.025},{"answer":"blind ten years old","prob":0.0225},{"answer":"blind ten years old","prob":0.02},{"answer":"pregnant women","prob":0.01},{"answer":"2 year old","prob":0.01}]}],"scenario_alternative":"21","scenario_others":2,"max_score":81.61070231},{"scenario_id":4,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1"],"answer":"50$","value":0.05},{"answer_type":"Select","answers_id":["Dice2"],"answer":"100$","value":0.025}]}],"scenario_answers":[{"answer_id":"35","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.605108055}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.629716981132075}]},{"answer_id":"Dice2","answer_type":"Dice","answers_probabilities":[{"answer":"100$","prob":0.62095238095238}]},{"answer_id":"Dice4","answer_type":"Dice","answers_probabilities":[{"answer":"100$","prob":0.6088235294117647}]},{"answer_id":"Decision1","answer_type":"Island","answers_probabilities":[{"answer":"A 35 year old sailor","prob":0.017},{"answer":"A heroic fighter","prob":0.0151},{"answer":"A 26 year old college","prob":0.0121},{"answer":"A rich charity worker","prob":0.0101},{"answer":"blind ten years old","prob":0.0042},{"answer":"blind ten years old","prob":0.0091},{"answer":"pregnant women","prob":0.0043},{"answer":"2 year old","prob":0.0021}]}],"scenario_alternative":"Dice4","scenario_others":2,"max_score":97.85502366},{"scenario_id":5,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1","Dice2","Dice3"],"answer":"50$","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Eggs","value":0.03},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Red pepers","value":0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Cheese","value":0.025},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Nature Vally","value":0.02},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bread","value":0.01},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bananas","value":0.035},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Oak mill","value":0.005},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Mousse","value":-0.05},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Coco pops","value":-0.042},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Bagel","value":-0.066},{"answer_type":"Cart","answers_id":["Cart1"],"answer":"Muffin","value":-0.0725}]}],"scenario_answers":[{"answer_id":"18","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.6242299794661191}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.608156028368794}]},{"answer_id":"Dice2","answer_type":"Dice","answers_probabilities":[{"answer":"100$","prob":0.608823529411764}]},{"answer_id":"Dice3","answer_type":"Dice","answers_probabilities":[{"answer":"100$","prob":0.608156028368794}]},{"answer_id":"Cart1","answer_type":"Cart","answers_probabilities":[{"answer":"XXXX","prob":0}]}],"scenario_alternative":"18","scenario_others":1,"max_score":None},{"scenario_id":6,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."},{"logic":"add","value":[{"answer_type":"Select","answers_id":["Dice1","Dice2","Dice3"],"answer":"50$","value":0.05},{"answer_type":"Select","answers_id":["Dice4"],"answer":"100$","value":0.025}]}],"scenario_answers":[{"answer_id":"18","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.624229979466119}]},{"answer_id":"35","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.605108055009823}]},{"answer_id":"Dice1","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.629716981132075}]},{"answer_id":"Dice2","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.62095238095238}]},{"answer_id":"Dice3","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.608156028368794}]},{"answer_id":"Dice4","answer_type":"Dice","answers_probabilities":[{"answer":"50$","prob":0.608823529411764}]},{"answer_id":"Decision1","answer_type":"Island","answers_probabilities":[{"answer":"A 35 year old sailor","prob":0.0475},{"answer":"A heroic fighter","prob":0.0225},{"answer":"A 26 year old college","prob":0.0111},{"answer":"A rich charity worker","prob":0.025},{"answer":"blind ten years old","prob":0.0225},{"answer":"blind ten years old","prob":0.02},{"answer":"pregnant women","prob":0.01},{"answer":"2 year old","prob":0.01}]}],"scenario_alternative":"35","scenario_others":0,"max_score":96.1702761},{"scenario_id":7,"scenario_logic":[{"logic":"avg","value":True,"description":"The average of all answers probabilities."}],"scenario_answers":[{"answer_id":"35","answer_type":"Image","answers_probabilities":[{"answer":"1","prob":0.605108055}]}],"scenario_alternative":"35","scenario_others":6,"max_score":99.99}],"any_other_pics_points":{"1":4.37,"2":7.11,"3":9.42,"4":1.21,"5":3.67,"6":4.41,"8":8.11,"9":2.87,"10":4.57,"11":8.87,"12":6.23,"13":5.09,"14":3.39,"15":0.77,"16":9.01,"17":9.21,"18":2.64,"19":1.17,"20":0.39,"21":8.01,"22":2.27,"23":6.51,"24":4.91,"25":5.97,"26":3.03,"27":8.04,"28":5.16,"29":3.77,"30":2.34,"31":3.55,"32":4.11,"33":4.86,"34":9.18,"35":6.34,"36":2.17,"37":3.38,"38":9.34}}
    result = dict()
    scenario = None
    probs = []
    score = 0
    additive_score = 0
    # picking up the correct scenario
    for scenario_item in scenario_scores.get('scenarios'):
        if str(scenario_item.get("scenario_id")) == str(scenario_id):
            scenario = scenario_item
            break
    # in case no scenario matched then return empty result
    if scenario is None:
        return result

    answers = flatten_answers_list(answers)
    for answer in answers:
        for scenario_answer in scenario.get("scenario_answers"):
            if scenario_answer.get("answer_id") == str(answer.get("QuestionId")):
                expected_answers = scenario_answer.get("answers_probabilities")
                for expected_answer in expected_answers:
                    if expected_answer.get("answer") == str(answer.get("QuestionAnswer")):
                        if scenario_answer.get("answer_type") == 'Island':
                            additive_score += expected_answer.get("prob")
                        else:
                            probs.append(expected_answer.get("prob"))
    # in case no matched answers then use the alternative scenario
    if len(probs) == 0:
        alternative_id = scenario.get("scenario_alternative")
        for scenario_answer in scenario.get("scenario_answers"):
            if str(scenario_answer.get("answer_id")) == str(alternative_id):
                score = scenario_answer.get("answers_probabilities")[0].get("prob")
    else:
        scenario_logic = scenario.get("scenario_logic")
        for logic in scenario_logic:
            if logic.get("logic") == "avg":
                score = np.average(probs)
            elif logic.get("logic") == "add":
                scenario_logic_values = logic.get("value")
                for answer in answers:
                    for logic_value in scenario_logic_values:
                        if answer.get("QuestionType") == logic_value.get("answer_type") and answer.get(
                                "QuestionId") in logic_value.get("answers_id") and answer.get(
                                "QuestionAnswer") == logic_value.get("answer"):
                            score = score + logic_value.get("value")

    # Pics extra points
    extra_points = scenario_scores.get('any_other_pics_points')
    for answer in answers:
        if answer.get('QuestionType') == 'Image':
            ans_id = answer.get('QuestionId').replace('Image', '')
            ans_extra_points = extra_points.get(ans_id) / 100
            score = score + ans_extra_points if ans_extra_points is not None else score

    scenario_max_score = scenario.get("max_score") / 100 if scenario.get("max_score") else 0.9999
    score = min(score + additive_score, scenario_max_score)
    recommendation_message = cvt_score_to_recommendation(score)
    result['final_score'] = score * 100
    result['recommendation'] = recommendation_message
    return result


if __name__ == '__main__':
    records = ['d0307e59- ec67-4bf9-a270-af4e35620cd8-2019-07-14T16-06-21.482Z.wav']
    get_namesyco_scores(records, 'https://confirmu-test.us-east-2.elasticbeanstalk.com')
