import unittest
from application import application


class TestRoutes(unittest.TestCase):

    def setUp(self):
        # creates a test client
        self.app = application.test_client()
        # propagate the exceptions to the test client
        self.app.testing = True

    def tearDown(self):
        pass

    def test_get(self):
        # sends HTTP GET request to the application
        # on the specified path
        endpoints = [
            '/itraveleo/',
            '/mobbisurance'
        ]
        for x in endpoints:
            result = self.app.get(x)

            # assert the status code of the response
            self.assertEqual(result.status_code, 200, 'Current route is unavailable: %s' % str(x))

    def test_post_scores_no_auth(self):
        result = self.app.post('/get_scores')
        self.assertEqual(result.status_code, 404)


if __name__ == '__main__':
    unittest.main()
