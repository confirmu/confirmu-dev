import hashlib
import json
from base64 import b64encode

import requests

from conf import ConstValues as const
from utils.gibberishclassifier import classify

CORIUNDER_URL = const.coriunder_url
TEXT_ANALYZER = const.text_analyzer


def get_customer_info_from_coriunder(user, email, password="aabb1122"):
    result = dict(
        error_code=0,
        error_message=''
    )
    login = json.dumps({
        "email": email,
        "password": password,
        "options": {
            "userRole": "15",
            "applicationToken": const.coriunder_token
        }
    })

    headers = {
        "Content-Type": "application/json",
        "applicationToken": const.coriunder_token
    }

    s = requests.Session()

    r_login = s.post(CORIUNDER_URL + 'account.svc/Login', data=login, headers=headers)
    log_info = r_login.json()
    if r_login.status_code != 200 or log_info['d']['IsSuccess'] is False:
        result['error_code'] = 1
        result['error_message'] = 'Unable to login. Response: %s' % log_info.text
        s.close()
        return result

    request_body_hash = b64encode(hashlib.sha256(const.coriunder_salt.encode()).digest()).decode()
    signature = "bytes-SHA256, %s" % request_body_hash

    headers_get_customer = headers
    headers_get_customer[log_info['d']['CredentialsHeaderName']] = log_info['d']['CredentialsToken']
    headers_get_customer['Signature'] = signature
    headers_get_customer['Cookie'] = r_login.headers.get('Set-Cookie').split(';')[0]

    r_get_customer = s.post(CORIUNDER_URL + 'customer.svc/GetCustomer', headers=headers_get_customer)
    s.close()

    if r_get_customer.status_code != 200:
        result['error_code'] = 1
        result['error_message'] = 'Unable to get customer information. Response: %s' % r_get_customer.text
        return result

    result['profile'] = r_get_customer.json()['d']
    return result





if __name__ == "__main__":
    #print(get_scores('something great could be written here to get high score'))
    print(get_customer_info_from_coriunder('', 'test3@test.com'))

