import requests
import hashlib
import json

data = json.dumps({
    "data": {
        "ApplicationToken": "056f16c3-6f7f-4d1d-b604-e63a9456e1ed",
        "Password": "aabb1122",
        "PinCode": "1234",
        "info": {
            "AddressLine1": "NY",
            "AddressLine2": "Manhattan",
            "City": "NY",
            "AboutMe": "Long description of boring life",
            "CellNumber": "1234567890",
            "CustomerNumber": "12436356347674",
            "EmailAddress": "test3@test.com",
            "FirstName": "John",
            "LastName": "Blacksmith",
            "PersonalNumber": "234623666",
            "PhoneNumber": "122346662346",
        }
    }
})

login = {
  "email": "test@test.com",
  "password": "aabb1122",
  "options": {
     "userRole": "15"
  }
}

my_hash = hashlib.sha256(str(data).encode()).hexdigest()
signature = "bytes-SHA256, %s" % my_hash

headers = {
    "Content-Type": "application/json",
    "applicationToken": "056f16c3-6f7f-4d1d-b604-e63a9456e1ed",
    "Signature": signature
}

print("Request body:\n", data)
print("Headers:\n", headers)

response = requests.post('https://webservices.coriunder.cloud/v2/customer.svc/RegisterCustomer', data=data, headers=headers)

#r_login = requests.post('https://webservices.coriunder.cloud/v2/account.svc/Login', data=login, headers=headers)

print("Response body:\n", response.text)
