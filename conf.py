import os

from utils.mongo_operations import get_partner_questions_no_context, get_mobileapp_options_no_context, \
    get_nrc_em_lexicon


class ConstValues:
    coriunder_url = r'https://webservices.coriunder.cloud/v2/'
    coriunder_token = os.environ.get('CORIUNDER_TOKEN')
    coriunder_salt = os.environ.get('CORIUNDER_SALT')
    text_analyzer = r'http://18.224.87.99/analyze_text'  # Ohio
    # text_analyzer = r'http://13.235.185.122/analyze_text'   # India
    confirmu_log_level = os.environ.get('CONFIRMU_LOG_LEVEL')
    app_secret_key = os.environ.get('APP_SECRET_KEY')
    patterns = "".join(os.environ.get('DEFAULT_PATTERNS').split()).split(',') if os.environ.get(
        'DEFAULT_PATTERNS') is not None else []
    transcript_key = os.environ.get('TRANSCRIPT_KEY')
    translate_key = os.environ.get('TRANSLATE_KEY')


class MongoDB:
    uri = os.environ.get('MONGO_URI')
    db = uri.split('/')[-1] if uri is not None else ""
    uri_india = os.environ.get('MONGO_URI_INDIA')
    db_india = uri_india.split('/')[-1] if uri_india is not None else ""
    salaries = 'salaries'
    loan_score = 'loanscore'
    col = 'cost_of_living'
    bsd = 'behaviour_score_data'
    rules = 'rules'
    getscores = 'get_scores'


class Emoxicon:
    emoxicon = get_nrc_em_lexicon()


class ImageScores:
    mobbisurance = {
        'dayoff': {'shopping': -5,
                   'reading': 10,
                   'running': 15,
                   'walking': -10,
                   'lying': -15},
        'holiday': {'camels': -5,
                    'beach': -10,
                    'mountains': -5,
                    'city': 5,
                    'village': -5},
        'hangout': {'family': 10,
                    'girl': -5,
                    'twogirls': 5,
                    'champaign': -10,
                    'couple': 5},
        'spend': {'school': 5,
                  'bags': -10,
                  'cake': -5,
                  'smoke': -5}
    }
    mobileapp = {
        '6': {"day_off_1": -15,
              "day_off_2": -5,
              "day_off_3": 10,
              "day_off_4": 15,
              "day_off_5": -10},
        '7': {"material_goods_1": -5,
              "material_goods_3": -10,
              "material_goods_4": 15,
              "material_goods_2": -5,
              "material_goods_5": -15,
              "vacation_2": 5,
              "vacation_1": -5,
              "vacation_3": 5,
              "vacation_4": -10,
              "vacation_5": -15,
              "day_off_1": -15,
              "day_off_2": -5,
              "day_off_3": 10,
              "day_off_4": 15,
              "day_off_5": -10
              },
        '8': {"material_goods_1": -5,
              "material_goods_3": -10,
              "material_goods_4": 15,
              "material_goods_2": -5,
              "material_goods_5": -15,
              "vacation_2": 5,
              "vacation_1": -5,
              "vacation_3": 5,
              "vacation_4": -10,
              "vacation_5": -15},
        '9': {"material_goods_1": -5,
              "material_goods_3": -10,
              "material_goods_4": 15,
              "material_goods_2": -5,
              "material_goods_5": -15,
              "vacation_2": 5,
              "vacation_1": -5,
              "vacation_3": 5,
              "vacation_4": -10,
              "vacation_5": -15},
        '11': {
            "girls_outfit_1": 5,
            "girls_outfit_2": 10,
            "girls_outfit_3": -5,
            "girls_outfit_4": -10,
            "boys_outfit_1": 5,
            "boys_outfit_2": 10,
            "boys_outfit_3": -5,
            "boys_outfit_4": -10},
        '12': {
            "girls_outfit_1": 5,
            "girls_outfit_2": 10,
            "girls_outfit_3": -5,
            "girls_outfit_4": -10,
            "boys_outfit_1": 5,
            "boys_outfit_2": 10,
            "boys_outfit_3": -5,
            "boys_outfit_4": -10},
        '13': {
            "girls_outfit_1": 5,
            "girls_outfit_2": 10,
            "girls_outfit_3": -5,
            "girls_outfit_4": -10,
            "boys_outfit_1": 5,
            "boys_outfit_2": 10,
            "boys_outfit_3": -5,
            "boys_outfit_4": -10}
    }


class PartnerConfigurations:
    config = dict(
        creditAI=dict(
            access_token=os.environ.get('CREDITAI_TOKEN')
        ),
        hedgini=dict(
            access_token=os.environ.get('HEDGINI_TOKEN')
        ),
        LM_financials=dict(
            access_token=os.environ.get('LM_FINANCIALS_TOKEN')
        ),
        dizzy=dict(
            access_token=os.environ.get('DIZZY_TOKEN')
        ),
        epaysa=dict(
            access_token=os.environ.get('EPAYSA_TOKEN')
        ),
        bacreditosimple=dict(
            access_token=os.environ.get('BACREDITOSIMPLE_TOKEN')
        ),
        mobile=dict(
            access_token=os.environ.get('MOBILE_TOKEN')
        ),
        ubapesa=dict(
            access_token=os.environ.get('UBAPESA_TOKEN')
        ),
        bridge2capital=dict(
            access_token=os.environ.get('BRIDGE2CAPITAL_TOKEN')
        ),
        b2cap_hindi=dict(
            access_token=os.environ.get('BRIDGE2CAPITAL_TOKEN')
        ),
        VOua8PmXQC=dict(
            access_token=os.environ.get('VOUA8PMXQC_TOKEN'),
            patterns=['dutiful', 'money', 'cautious', 'genuine']
        ),
        inclusivity=dict(
            access_token=os.environ.get('INCLUSIVITY_TOKEN')
        ),
        enterid=dict(
            access_token=os.environ.get('ENTERID_TOKEN')
        ),
        leo=dict(
            access_token=os.environ.get('LEO_TOKEN')
        ),
        myfugo=dict(
            access_token=os.environ.get('MYFUGO_TOKEN')
        ),
        iloan=dict(
            access_token=os.environ.get('ILOAN_TOKEN')
        ),
        whatsloan=dict(
            access_token=os.environ.get('WHATSLOAN_TOKEN')
        ),
        mobileapp=dict(
            access_token=os.environ.get('MOBILE_APP_TOKEN'),
            questions=get_partner_questions_no_context('mobileapp', 'en'),
            options=get_mobileapp_options_no_context()

        ),
        ilend=dict(
            access_token=os.environ.get('ILEND_TOKEN')
        ),
        itraveleo=dict(
            access_token=os.environ.get('ITRAVELEO_TOKEN'),
            questions=dict(
                question_01='Who do you usually travel with?',
                question_02='How many times do you travel for pleasure in a year? How many times would you ideally like to take vacations in a year?',
                question_03='What kind of accommodation do you usually stay in – Hotels, Homestays, Hostels, Service Apartments, or Guesthouses? What’s the best accommodation you’ve stayed in so far?',
                question_04='When travelling what kind of food do you eat? Local cuisine, Indian cuisine or dine at fast food chains like McDonalds and Dominoes?',
                question_05='What kind of activities do you enjoy the most when traveling?',
                question_06='What kind of destinations do you like you visit?',
                question_07='Imagine you’ve lost your baggage on your way from the airport to hotel. What would you do?',
                question_08='You’re lost in an unknown city and your phone’s battery dies. How will you to try to find your way back to your hotel?',
                question_09='What do you think is the best thing about traveling?',
                question_10='What is the most annoying thing about traveling?',
                question_11='What is your travel goal?',
                question_12='What criteria do you consider before choosing a holiday destination?',
                question_13='What are the emotions and thoughts that run through your mind at the end of your trip?',
                question_14='Have you ever taken a loan or considering taking one? What kind of loan is it? Have you repaid any loan taken completely?',
                question_15='You’re in a bar and you want to hang out with a group of friendly-looking locals sitting on the next table. How would you approach them and start a conversation?',
                question_16='How do you normally approach arguments?',
                question_17='How would you best describe your work?',
                question_18='How do you think your closest friends would describe you?',
                question_19='What kind of a traveler would you say you are?',
                question_20='How important is cost to you when choosing a vacation destination?',
                question_21='How early do you normally plan your holiday?',
                question_22='How do you keep track of expenses when traveling?',
                question_23='Your boss asks you to stay up all night to respond to an email although you are not getting paid for it. What will you do?',
                question_24='You are promoted and will be your best friend’s senior at work. How will you interact with him/her now?',
                question_25='Imagine you have missed work deadlines due to negligence. What explanation will you give your boss?',
                question_26='Just before you are about to go up on stage for a presentation you find a stain on your shirt, with no spare shirt around. What will you do?',
                question_27='Your partner got promoted at work and is now required to work late. You’ll hardly see him / her at home. What will you do?',
                question_28='When the cashier at the supermarket hands you your bill, you realize that the prices are higher than you expected. How will you react?')
        ),
        tutu=dict(
            access_token=os.environ.get('TUTU_TOKEN'),
            questions=dict(
                question_1='Para quem é o empréstimo do carro?',
                question_2='Quanto tempo você possui uma carteira de motorista?',
                question_3='O que você gosta de dirigir?',
                question_4='Você pode descrever qual é o seu tipo favorito de carro?',
                question_5='O que fez você comprar um carro e por que emprestar dinheiro para isso?',
                question_6='Como você navega nas estradas para o seu destino?',
                question_7='Você vai à garagem às vezes ou você lida com isso sozinho?',
                question_8='Se você fosse escolher entre uma empresa de carro alugado e um carro de propriedade privada, o que você escolheria?',
                question_9='Qual é a sua reação típica quando outro motorista o passa na sua fila quando está prestes a dar uma volta?',
                question_10='você já teve um carro ao qual você estava realmente ligado? Por quê?',
                question_11='À medida que agendou um compromisso, você encontra o seu auto preso em um engarrafamento, mas sem possibilidade de estimar se você vai atrasar ou não, o que você vai fazer?',
                question_12='Agora, um sinal no seu carro mostra que dentro de 3000 quilômetros você precisaria ter o carro entrar no check-up regular na garagem e na próxima semana expira a licença anual e o carro deve ser testado o que você fará?',
                question_13='Os negócios automotivos têm um novo acordo sobre o comércio de carros no negócio onde se você vender seu carro para eles com uma taxa adicional, você pode obter um novo, mas descobre que se você vende seu carro e pegue um empréstimo por 3 anos, isso talvez seja mais barato agora, mas caro mais tarde, qual opção você escolherá?'
            )
        ),
        sage=dict(
            access_token=os.environ.get('SAGE_TOKEN')
        ),
        test=dict(
            access_token=os.environ.get('TEST_TOKEN'),
            questions=dict(
                question_01='How do you define a great day on the slopes?',
                question_02='When it’s snowing hard in resort do you prefer..',
                question_03='Faced with a last run down to resort that’s covered in bumpy moguls do you...',
                question_04='Will you take ski or snowboard lessons on your next holiday?',
                question_05='Once the slopes have closed for the day you prefer to spend your evenings...'
            )
        ),
        indepay=dict(
            access_token=os.environ.get('INDEPAY_TOKEN')
        )
    )


class PartnerQuestionsConf:
    bridge2capital_questions = dict({"free_text": 7, "slider": 2, "image": 3, "drawing": 1, "voice": 2, "select": 2})
    b2cap_hindi_questions =({"free_text": 5, "slider": 2, "image": 3, "drawing": 1, "voice": 2, "select": 2})

class GibberishContent:
    english_gibberish_reason = "Looks like you have gibberish code in your answers, Please check again and re-enter without non valid text!"
    hindi_gibberish_reason = "aisa lagata hai ki aapake uttar mein aapake paas aspasht kod hai, krpaya phir se jaanch karen aur bina vaidh paath ke phir se darj karen!"
